package mainpackage;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class TaskPanel extends JComponent {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Graphics2D g2;
	
	public int unitWidth = 50;
	public int unitHeight = 50;
	public int pw, ph;

	int xToGuess, yToGuess;
	boolean guessed = false;
	boolean missed = false;
	
	JPanel messagePanel = new JPanel();
	
	
	boolean showMessagePanel = false;
	
	JButton messageOK = new JButton("ok");
	JTextField nameField = new JTextField(5);
	JButton submitBut = new JButton("spremi rezultat");
	JLabel scoreLabel = new JLabel();
	
	String scoreFilePath = "scores.txt";
	
	
	
	
	public TaskPanel() {
		super();
		addMouseListener(new Clicker());
		addMouseMotionListener(new Dragger());
		//renderTask();
		messagePanel.setBounds(50,100, 120, 100);
		messagePanel.setBackground(new Color(200, 230, 230));
		//messagePanel.setPreferredSize(new Dimension(200,100));
		messagePanel.add(scoreLabel);

		//messagePanel.add(messageOK);
		nameField.setMinimumSize(new Dimension(60, 20));
		messagePanel.add(nameField);
		messagePanel.add(submitBut);
		//System.out.println(messagePanel.getComponentCount());
		//messagePanel.setBackground(Color.blue);
		messageOK.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						remove(messagePanel);
						//repaint();
						//System.out.println("ok");
						
					}
				});
			}
		});
		submitBut.addActionListener(new ActionListener() {
			
			void checkName(String name) {
				for (int i = 0; i<name.length(); i++) {
					
				}
			}
			
			public void actionPerformed(ActionEvent e) {
				String myName = nameField.getText();
				if (myName.trim().equals("")) {
					System.out.println("Enter name");
					return;
				}
				File f = new File(scoreFilePath);
				Scanner scanner;
				FileWriter fw;
				
				try {
					fw = new FileWriter(f, true);
					
					fw.write("\n***\n" + myName + "\n" + gameDurationSec + "\n" +  numCorrect + " " + numWrong + "\n---\n");
					fw.close();
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					System.out.println(e1.getMessage());
				}
				remove(messagePanel);
				revalidate();
				repaint();
			}
		});
		
		
		//add(messagePanel);
		//add(messagePanel);
		
	}
	


	
	boolean firstrender = true;
	
	public void renderTask() {
		do {
			xToGuess = (int)(0.9*(Math.random()-0.5) * pw/unitWidth);
			yToGuess = (int)(0.9*(Math.random()-0.5) * ph/unitHeight);
			//System.out.println(xToGuess);
		} while (Math.abs(xToGuess) + Math.abs(yToGuess) < 3);
		

	}
	
	

	
	
	
	
	boolean drawing = true;
	boolean moving = true;
	LocVektor vek = new LocVektor(0,0,0,0);
	
	
	int xor, yor;
	
	public void paintLocLine(LocVektor lv) {
		g2.drawLine((int)lv.x0, (int)lv.y0, (int)(lv.x0+lv.x), (int)(lv.y0+lv.y));
	}
	
	boolean help = false;
	
	boolean readyToPlay = false;
	boolean playing = false;
	
	LocVektor locArrow;
	double arrowLength = 15;
	double arrowHalfAngle = 0.3;
	public void paintLocVektor(LocVektor lv) {
		paintLocLine(lv);
		lv.fix();
		if (lv.r < 2)
			return;
		
		double dxa = -lv.x/arrowLength, dya = -lv.y/arrowLength;
		double r = Math.sqrt(dxa*dxa + dya*dya);
		double an = Vektor.angle(dxa, dya);
		if (r<8) {
			r = 8;
			dxa = r * Math.cos(an);
			dya = r * Math.sin(an);
		}
		an += arrowHalfAngle;
		locArrow = new LocVektor(lv.x1, lv.y1, r*Math.cos(an), r*Math.sin(an));
		paintLocLine(locArrow);
		an -= 2*arrowHalfAngle;
		locArrow.x = r*Math.cos(an);
		locArrow.y = r*Math.sin(an);
		paintLocLine(locArrow);
	}
	
	String task;
	
	LocVektor helpV;
	
	
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		pw = getWidth(); ph = getHeight();
		g.setColor(new Color(220, 175, 175));
		g.fillRect(0,0,pw,ph);
		g2 = (Graphics2D) g;
		
		if (firstrender) {
			renderTask();
			firstrender = false;
		}
		
		
		
		
		
		xor = pw/2; yor = ph/2;
		
		g2.setColor(Color.black);
		g2.setStroke(new BasicStroke(4));
		g2.drawLine(0, yor, pw, yor);
		g2.drawLine(xor, 0, xor, ph);
		
		g2.setStroke(new BasicStroke(1));

		for (int i = xor+unitWidth; i<=pw; i+= unitWidth) {
			if ((i-xor)/unitWidth % 5 == 0) {
				g2.setStroke(new BasicStroke(2));
				g2.drawLine(i, 0, i, ph);
				g2.drawLine(2*xor-i, 0, 2*xor-i, ph);
				g2.setStroke(new BasicStroke(1));
			}
			else {
				g2.drawLine(i, 0, i, ph);
				g2.drawLine(2*xor-i, 0, 2*xor-i, ph);

			}
			
			g2.drawString(""+(i-xor)/unitWidth, i+5, yor+15);
			g2.drawString(""+(xor-i)/unitWidth, 2*xor-i, yor+15);

		}
		
		for (int i = yor+unitHeight; i<=ph; i+= unitHeight) {
			if ((i-yor)/unitHeight % 5 == 0) {
				g2.setStroke(new BasicStroke(2));
				g2.drawLine(0, i, pw, i);
				g2.drawLine(0, 2*yor-i, pw, 2*yor-i);
				g2.setStroke(new BasicStroke(1));
			}
			else {
				g2.drawLine(0, i, pw, i);
				g2.drawLine(0, 2*yor-i, pw, 2*yor-i);
			}
			
			g2.drawString(""+(i-yor)/unitHeight, xor+2, i+5);
			g2.drawString("-"+(i-yor)/unitHeight, xor+2, 2*yor-i-5);

		}
		
		
		/*
		for (int i = unitWidth/2; i<pw; i += unitWidth) {
			if (pw/2 - i <= unitWidth && i < pw/2) {
				g2.setStroke(new BasicStroke(3));
				g2.drawLine(i,0, i,ph);
				g2.setStroke(new BasicStroke(1));
				g.setFont(new Font("Arial", 0, 30));
				g.drawString("y", i+5, 15);
				xor = i;
				continue;
			}
			g2.drawLine(i,0, i,ph);
			
		}
		
		for (int i = unitHeight/2; i<ph; i += unitHeight) {
			if (ph/2 - i <= unitHeight && i < ph/2) {
				g2.setStroke(new BasicStroke(3));
				g2.drawLine(0,i, pw,i);
				g2.setStroke(new BasicStroke(1));
				g.drawString("x", pw-20, i-10);

				yor = i;
				continue;
			}
			g2.drawLine(0,i, pw,i);
		}
		*/
		vek.x0 = xor;
		vek.y0 = yor;
		
		
		
		
		
		
		g.setColor(new Color(0, 70, 70));
		g.setFont(new Font("Arial", 0, 50));
		
		
		
		if (readyToPlay && !playing) {
			task = "Kliknite na ravninu za pocetak";
		}
		else {
			if (playing) {
				if (guessed) {
					//renderTask();
					//guessed = false;
				}
			}
			task = "Nacrtaj vektor " + xToGuess + "i " + (yToGuess < 0 ? "- " : "+ ") + Math.abs(yToGuess) + "j";
		}
		
		//g.drawString(task, 50, 50);
		g.drawString(task, 50, 50);

		if (guessed) {
			//renderTask();
			g.drawString("Tako je!", 50, 100);
		}
		if (missed) {
			g.drawString("Krivo", 50, 100);
		}
		
		if (drawing) {
			//System.out.println("yes");
			g2.setStroke(new BasicStroke(4));
			g2.setColor(Color.blue);
			paintLocVektor(vek);
			if (help) {
				g.setFont(new Font("Arial", 0, 15));

				g2.setStroke(new BasicStroke(2));
				g2.setColor(Color.red);
				vek.fix();
				//helpV = new LocVektor(xor, yor+vek.y, vek.x1-xor, -vek.y1+yor);
				helpV = new LocVektor(xor, vek.y1, vek.x1, vek.y1, true);
				paintLocVektor(helpV);
				g.drawString(vek.x/unitWidth+"", (int)(xor + vek.x/2), (int)vek.y1+15);
				//helpV = new LocVektor(yor, vek.x1, vek.x1, vek.y1, true);
				helpV = new LocVektor(vek.x1, yor, vek.x1, vek.y1, true);

				paintLocVektor(helpV);
				//g.drawString(vek.y/unitHeight+"", (int)(yor + vek.y/2), (int)vek.y1+5);
				g.drawString(-vek.y/unitHeight+"", (int)(vek.x1+5), (int)(yor + vek.y/2));

			}
		}
		
		
	}
	
	
	Timer timer = new Timer(1500, new MyActionListener());
	
	boolean drawSuccess = false;
	
	class MyActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			timer.stop();
			if (playing) {
				drawSuccess = false;
			}

			guessed = false;
			moving = true;
//			if (!missed)
//				renderTask();
			missed = false;
			repaint();
		}
		
	}
	
	int gameDurationSec = 20;
	
	Timer gtimer = new Timer(gameDurationSec*1000, new GameDurationListener());
	
	int numCorrect = 0, numWrong = 0;
	
	
	
	class GameDurationListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			
			
			gtimer.stop();
			timer.setDelay(1500);
			timer.setInitialDelay(1500);
			readyToPlay = playing = false;
			//System.out.println("correct: " + numCorrect + ", wrong: " + numWrong);
			scoreLabel.setText("<html>Tocno: " + numCorrect + "<br/>pogreske: " + numWrong + "</html>");
			//numCorrect = numWrong = 0;

			add(messagePanel);
			revalidate();
			repaint();
			
		}
	}
	
	class Clicker implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			int x, y;
			
			if (readyToPlay && !playing) {
				playing = true;
				numCorrect = numWrong = 0;
				timer.setDelay(200);
				timer.setInitialDelay(200);
				
				repaint();
				gtimer.restart();
				return;
			}
			
			if (moving) {
				x = e.getX();
				y = e.getY();
				double xgrid = (x-xor)*1.0 / unitWidth, ygrid = -(y-yor)*1.0 / unitHeight;
				if (Math.pow(xToGuess-xgrid, 2) + Math.pow(yToGuess-ygrid, 2) < 0.1) {
					guessed = true;
					missed = false;
					numCorrect++;
					renderTask();

					repaint();
					timer.restart();
					drawSuccess = true;
					
					/*
					System.out.println("about to sleep");
					try {
						System.out.println("sleeping");
						Thread.sleep(10);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						System.out.println("woke up");
						guessed = false;
						moving = true;
						renderTask();
						repaint();
					}
					*/
					
				}
				else {
					//moving = false;
					missed = true;
					guessed = false;
					numWrong++;
					repaint();
					timer.restart();
				}
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
			int x, y;
			
			
			
			
			
			
			//System.out.println(e.getX() + id);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	class Dragger implements MouseMotionListener {

		int x, y;
		
		@Override
		public void mouseDragged(MouseEvent e) {
			
		}
		
		
		@Override
		public void mouseMoved(MouseEvent e) {
			if (!moving)
				return;
			x = e.getX(); y = e.getY();
			//System.out.println(x + " " + y);
			vek.x = x-xor; vek.y = y-yor;
			repaint();
			//System.out.println(vek);
		}

	}
	
	
	
	
}
