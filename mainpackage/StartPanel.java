package mainpackage;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import exercises.ExerciseSelection;
import lessons.LessonPanel;

public class StartPanel extends JPanel {

	
	ActionListener l;
	
	public StartPanel() {
		super();
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS) );
		JLabel title = new JLabel("Lagano s vektorima");
		title.setFont(new Font("Comic Sans", 0, 50));
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		JPanel buttons = new JPanel();
		buttons.setLayout(new GridLayout(0,2));
		/*
		JPanel jp = new JPanel();
		jp.setLayout(new BorderLayout());
		jp.add(title, BorderLayout.LINE_END);
		jp.validate();
		*/
		add(title);
		
		JButton jb = new JButton("Istraži");
		jb.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				l.actionPerformed(e);
			}
			
		});
		
		buttons.add (jb);
		
		JButton probButton = new JButton("Izvježbaj");
		buttons.add(probButton);
		probButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						Container par = getParent();
						par.removeAll();
						par.add(new ExerciseSelection());
						par.revalidate();
						par.repaint();
					}
				});
				
			}
		});
		
		JButton lectButton = new JButton("Nauči");
		buttons.add(lectButton);
		lectButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						Container par = getParent();
						par.removeAll();
						par.add(new LessonPanel());
						par.revalidate();
						par.repaint();
					}
				});
			}
		});
		
		JButton progrButton = new JButton("Napredak");
		buttons.add(progrButton);
		
		progrButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				SwingUtilities.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						Container par = getParent();
						par.removeAll();
						par.add(new ScoresPanel());
						par.revalidate();
						par.repaint();
					}
				});
			}
		});
		
		add(buttons);
		
		//add(new Calculator());
		
		revalidate();
		
	}
	
	
	
}


//jb.setAlignmentX(Component.RIGHT_ALIGNMENT);
		/*
		JPanel jp1 = new JPanel();
		jp1.setLayout(new BorderLayout());
		jp1.setPreferredSize(new Dimension(jb.getWidth(), jb.getHeight()));
		*/

