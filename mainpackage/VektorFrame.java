package mainpackage;
import java.awt.BasicStroke;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.text.StyledDocument;


public class VektorFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	int wDef = 500, hDef = 500;
	int w, h;

	public VektorFrame() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		w = wDef;
		h = hDef;
		setSize(w, h);
		setVisible(true);
		getContentPane().setLayout(new GridLayout());
	}

	public VektorFrame(boolean dontshow) {
		
	}
	
	LessonPanel lessonP = new LessonPanel();

	public VektorFrame(int wi, int hi) {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		w = wi;
		h = hi;
		setSize(w, h);
		setVisible(true);
	}

	String scoreFilePath = "scores.txt";

	public ScorePanel giveScorePanel() {
		return new ScorePanel();
	}

	class ScorePanel extends JPanel {

		private static final long serialVersionUID = 1L;
		JScrollPane jsp = new JScrollPane();
		JTextArea scoresTA = new JTextArea();

		public ScorePanel() {
			fetch();
			// JTextArea scoresTA = new JTextArea();
			/*
			 * scoresTA.append("Rezultati\n-------------------------------\n");
			 * System.out.println(scores); for (List<String> score : scores) { int correct =
			 * Integer.parseInt(score.get(2)); int incorrect =
			 * Integer.parseInt(score.get(3));
			 * 
			 * scoresTA.append("Ime: " + score.get(0) + "\n"); scoresTA.append("Trajanje: "
			 * + score.get(1) + "s\n"); scoresTA.append("Pogodaka: " + score.get(2) + "\n");
			 * scoresTA.append("Brzina: " + Integer.parseInt(score.get(1)) * 1.0 / correct +
			 * "s\n"); scoresTA.append("Tocnost: " + (int)(100 * correct * 1.0 / (correct +
			 * incorrect)) + "%\n\n\n"); }
			 */
			jsp = new JScrollPane(scoresTA);
			jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			System.out.println(getHeight());
			jsp.setPreferredSize(new Dimension(500, 500));
			add(jsp);
		}

		String scoresString = "";

		List<List<String>> scores = new ArrayList<List<String>>();

		public void fetch() {
			try {
				File f = new File(scoreFilePath);
				Scanner sc = new Scanner(f);
				List<String> score = new ArrayList<String>();
				int i = 0;
				while (sc.hasNextLine()) {
					// System.out.println(i++);
					String line = sc.nextLine();
					if (line.equals("***")) {
						/*
						 * score.clear(); score.add(sc.nextLine()); score.add(sc.nextLine()); line =
						 * sc.nextLine(); score.add(line.split(" ")[0]); score.add(line.split(" ")[1]);
						 * scores.add(score); System.out.print(score); System.out.println(scores);
						 */

						scoresTA.append("Ime: " + sc.nextLine() + "\n");
						int time = Integer.parseInt(sc.nextLine());
						scoresTA.append("Trajanje: " + time + "s\n");
						String[] hits = sc.nextLine().split(" ");

						int correct = Integer.parseInt(hits[0]);
						int incorrect = Integer.parseInt(hits[1]);

						scoresTA.append("pogodaka: " + correct + "\n");
						scoresTA.append("Brzina: " + time * 1.0 / correct + "s\n");
						scoresTA.append("Tocnost: " + (int) (100 * correct * 1.0 / (correct + incorrect)) + "%\n\n\n");
					}
				}
				sc.close();
			} catch (IOException e1) {

			}
		}

	}

	class LessonPanel extends JComponent {

		
		
		private static final long serialVersionUID = 1L;

		JPanel bufferP = new JPanel();

		
		
		public void initiate() {

			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			// setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();

			JTextArea jta1 = new JTextArea();
			jta1.setEditable(false);
			jta1.append("Vektori su usmjerene dužine. To znači da imaju duljinu, orijentaciju i smjer.\n");
			jta1.append("Usredotočimo se na jednu dimenziju. Imamo samo dvije orijentacije: lijevo i desno.\n");
			jta1.append("Stoga, vektor u jednoj dimenziji daje položaj točke na brojevnom pravcu.\n");
			jta1.append("Često prikazujemo vektor strelicom koja ima orijentaciju i duljinu iste kao taj vektor.\n\n\n");
			jta1.append("Pomičite kraj vektora (vrh strelice) da bi vidjeli kako se mijenjaju duljina i orijentacija vektora");

			add(jta1);
			/*
			 * JPanel inner = new JPanel(); inner.setPreferredSize(new Dimension(200, 100));
			 * inner.setBackground(Color.green); add(inner);
			 */

			VectorPanel1D p1 = new VectorPanel1D();
			add(p1);
			
			JTextArea jta2 = new JTextArea();
			jta2.setEditable(false);
			jta2.append("Iskoristimo cijeli potencijal brojevnog pravca i opišimo vektor samo jednim brojem.\n");
			jta2.append("Umjesto riječi lijevo ili desno ćemo koristiti znak (pozitivno ili negativno)\n");
			jta2.append("Kako smo brojevni pravac odlučili orijentirati s pozitivnim vrijednostima prema desno,\n");
			jta2.append("onda će svi vektori koji imaju orijentaciju prema desno biti \"pozitivni\", a oni prema lijevo \"negativni\".\n");
			jta2.append("Ovu kombinaciju duljine i smjera nazivamo iznos, veličina, ili jednostavno vrijednost vektora.");
			

			
			add(jta2);
			System.out.println("y: " + jta2.getY());

			VectorPanel1D p2 = new VectorPanel1D();
			p2.envelopInfo = true;
			add(p2);
			
			JTextArea jta3 = new JTextArea();
			jta3.setEditable(false);
			jta3.append("Rekli smo da vektor opisuju duljina, smjer i orijentacija, no nismo ništa rekli o tome gdje počinje.\n");
			jta3.append("I zaista (iako smo ga dosad radi jasnoće crtali s početkom u 0)\n,\n");
			jta3.append("vektor smatramo neovisnim o mjestu gdje se nalazi. Možemo ga pomicati bez da ga promijenimo.\n");

			add(jta3);
			
			add(new Question("Raketa i kamen se gibaju vertikalno - raketa gore, a kamen dolje.\n"
					+ "Vektori njihovih brzina imaju jednake:", new String[] {"Orijentacije", "Smjerove", "Module"}, 1));
			
			add(new Question("Vektori istih orijentacija imaju iste smjerove", new String[] {"DA", "NE"}, 0));
			
			SeveralVectorPanel1D p3 = new SeveralVectorPanel1D();
			p3.envelopInfo = true;
			add(p3);

			Button nextBut = new Button("Dalje");
			nextBut.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						/*
						vf.remove(jsp);
						vf.add(new JScrollPane(vf.lessonP));
						*/
						removeAll();
						try {
							putAddition();
						} catch (InvocationTargetException | InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						revalidate();
						LessonPanel.this.revalidate();

					}
				});
			add(nextBut);
			add(new JLabel("Vektori"));
			add(bufferP);
			revalidate();
			repaint();

			//revalidate();
			
			
		}
		
		public LessonPanel() {
			super();
			bufferP.setPreferredSize(new Dimension(200, 100));
			SwingUtilities.invokeLater(new Runnable(){

				@Override
				public void run() {
					initiate();
					//LessonPanel.this.revalidate();
				}
			});
		}
		
		public void putAddition() throws InvocationTargetException, InterruptedException {
			SwingUtilities.invokeLater(new Runnable(){

				@Override
				public void run() {
					// TODO Auto-generated method stub
					JTextArea jta1 = new JTextArea();
					jta1.setEditable(false);
					jta1.append("Vektore možemo zbrajati. Rezultat zbrajanja je novi vektor.\n");
					jta1.append("Zbrajanje vektora radimo tako da početak jednog vektora stavimo na kraj drugog.\n");
					jta1.append("Rezultat je vektor koji je usmjeren od početka prvog vektora do kraja drugog vektora.\n");


					

					add(jta1);
					
					

					
					add(new AdditionPanel1D());
					
					Button returnB = new Button("Natrag");
					returnB.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								/*
								vf.remove(jsp);
								vf.add(new JScrollPane(vf.lessonP));
								*/
								SwingUtilities.invokeLater(new Runnable(){

									@Override
									public void run() {
										removeAll();
										initiate();
										revalidate();
									}
								});
								
							}

						});
					
					add(returnB);
					
					Button nextBut = new Button("Dalje");
					nextBut.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								/*
								vf.remove(jsp);
								vf.add(new JScrollPane(vf.lessonP));
								*/
								
									SwingUtilities.invokeLater(new Runnable(){

										@Override
										public void run() {
											
											putDotProduct();
											
										}
									});
								
							}

						});
					
					add(nextBut);
					add(bufferP);
					add(new Question("Ako zbrojimo dva vektore suprotnih orijentacija, rezultat može biti nul-vektor.", new String[] {"DA", "NE"}, 0));

				}
				
			});
			
			
		}

		public void putDotProduct() {
			removeAll();
			JTextArea jta1 = new JTextArea();
			jta1.setEditable(false);
			jta1.append("Pogledajmo što znači ortogonalna projekcija\n");
			jta1.append("Projiciranje vektora v na vektor u smjeru x-osi rezultira x-komponentom od v\n");

			

			
			

			add(jta1);
			
			add(new DotProductPanel(true));
			
			
			
			JTextArea jta2 = new JTextArea();
			jta2.setEditable(false);
			jta2.append("Proizvoljno odaberimo vektor na koji projiciramo\n");
			jta2.append("\n");
			add(jta2);
			
			
			add(new DotProductPanel());
			
			JTextArea jta3 = new JTextArea();
			jta3.setEditable(false);
			jta3.append("Primijetimo da je duljina projekcije manja ili jednaka duljini vektora koji projiciramo.\n");
			jta3.append("Njena duljina ovisi o kutu između vektora. P = v cosθ\n");
			jta3.append("Ako θ = 90° onda projekcija nestaje (pretvara se u nul-vektor), što lagano vidimo iz crteža.\n");
			jta3.append("Umnožak |v| |u| cosθ nazivamo skalarni produkt vektora v i u.·\n");
			jta3.append("Označavamo ga obično v·u ili u·v \n");
			
			JTextPane jtp = new JTextPane();
	        StyledDocument doc = jtp.getStyledDocument();

			
			jta3.append("\n");
			
			
			jta3.append("\n");
			add(jta3);
			String q = "Za vektore neke duljine, kada je njihov skalarni umnožak najveći?";
			
			add(new Question(q, new String[] {
				"Kad je kut između njih 0",
				"Kad je kut između njih 180°",
				"Kad je kut između njih 90°"
			}, 0));
			
			q = "A kada je najmanji?";
			add(new Question(q, new String[] {
					"Kad je kut između njih 0",
					"Kad je kut između njih 180°",
					"Kad je kut između njih 90°"
				}, 1));
			
			q = "Kada projekcija u na v ima najmanju duljinu=";
			add(new Question(q, new String[] {
					"Kad je kut između u i v 0",
					"Kad je kut između u i v 180°",
					"Kad je kut između u i v 90°"
				}, 2));
			
			
			JTextArea jta4 = new JTextArea();
			jta4.setEditable(false);
			jta4.append("Primijetimo da je duljina projekcije manja ili jednaka duljini vektora koji projiciramo.\n");
			jta4.append("Njena duljina ovisi o kutu između vektora. P = v cosθ\n");
			jta4.append("Ako θ = 90° onda projekcija nestaje (pretvara se u nul-vektor), što lagano vidimo iz crteža.\n");
			jta4.append("Umnožak |v| |u| cosθ nazivamo skalarni produkt vektora v i u.·\n");
			jta4.append("Označavamo ga obično v·u ili u·v \n");
			
			add(jta4);
			
			DotProductPanel dpp = new DotProductPanel();
			dpp.formulas = true;
			
			add(dpp);
			
			Button returnB = new Button("Natrag");
			returnB.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						/*
						vf.remove(jsp);
						vf.add(new JScrollPane(vf.lessonP));
						*/
						SwingUtilities.invokeLater(new Runnable(){

							@Override
							public void run() {
								removeAll();
								initiate();
								//revalidate();
								
							}
						});
						
					}

				});
			
			add(returnB);
			
			Button nextBut = new Button("Dalje");
			nextBut.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						SwingUtilities.invokeLater(new Runnable(){

							@Override
							public void run() {
								putCrossProduct();
								//revalidate();
								
							}
						});
					}

			});
			
			add(nextBut);
			add(new JLabel("Vektori"));
			add(bufferP);
			revalidate();
			repaint();
		}
		
		
		
		public void putCrossProduct() {
			removeAll();
			
			
			add(new CrossProductPanel());
			add(bufferP);

			revalidate();
			repaint();
		}
		
	}

	class VectorPanel1D extends JPanel {

		private static final long serialVersionUID = 1L;

		public VectorPanel1D() {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 200));
			repaint();
			addMouseMotionListener(new Clicker());
		}
		
		int w, h;

		int lineUnits = 10;
		int unitWidth;
		double xV = 2.5;
		int x0;
		
		double x01 = 1;
		double x02 = -4;
		
		
		boolean showInfo = true;
		
		boolean envelopInfo = false;
		
		@Override
		public void paintComponent(Graphics g) {
			Font f = g.getFont();
			super.paintComponent(g);
			w = getWidth();
			h = getHeight();
			x0 = w / 2;
			unitWidth = w/lineUnits;
			Graphics2D g2d = (Graphics2D) g;
			int y = h / 2;
			g2d.setColor(Color.black);
			
			g2d.drawLine(0, y, w, y);
			int arrowLen = 30;
			if (envelopInfo) {
				g2d.drawLine(w, y, w-arrowLen, y-arrowLen/2);
				g2d.drawLine(w, y, w-arrowLen, y+arrowLen/2);
			}
			
			g2d.drawLine(x0, y-5, x0, y+5);
			g2d.drawString("0", x0, y+15);
			
			for (int i = 1; i<=lineUnits/2; i++) {
				int xcoor = i*unitWidth + x0;
				g2d.drawLine(xcoor, y-5, xcoor, y+5);
				g2d.drawString(""+i, xcoor+5, y+15);
				g2d.drawLine(2*x0 - xcoor, y-5, 2*x0 - xcoor, y+5);
				g2d.drawString("-"+i, 2*x0 - xcoor+5, y+15);
				
			}
			g2d.setColor(Color.blue);
			g2d.setStroke(new BasicStroke(5));
			int xVs = x0 + (int)(xV*unitWidth);
			int x01s = x0 + (int)(xV*unitWidth);
			int xV1s = x01s + (int)(xV*unitWidth);
			g2d.drawLine(x0, y, xVs, y);
			arrowLen = (int)(xV*unitWidth * 0.1);
			int arrowLen1 = Math.min(Math.abs(arrowLen), w/50);
			arrowLen1 = Math.max(Math.abs(arrowLen),  8);
			arrowLen = arrowLen1 * (int)Math.signum(arrowLen);
			//g2d.setStroke(new BasicStroke(3));
			g2d.drawLine(xVs, y, xVs-arrowLen, y-arrowLen/3);
			g2d.drawLine(xVs, y, xVs-arrowLen, y+arrowLen/3);
			if (showInfo) {
				//f.deriveFont(10);
				//System.out.println(f);
				//g.setFont(new Font("Dialog", Font.BOLD, 20));
				g.setFont(f.deriveFont(Font.BOLD, 15f));
				if (envelopInfo) {
					g.drawString("Iznos vektora: " + (int)(100*xV)/100.0, w/3, y+40);
				}
				else {
					g.drawString("Duljina: " + (int)(100*Math.abs(xV))/100.0 + ", smjer: " + (xV<0 ? "lijevo" : "desno"), w/3, y+40);
				}
			}
		}
		
		class Clicker implements MouseMotionListener {

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				xV = e.getX();
				xV = (xV-x0)/unitWidth;
				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}}
		
	}
	
	class SeveralVectorPanel1D extends JPanel {

		private static final long serialVersionUID = 1L;

		public SeveralVectorPanel1D() {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 300));
			repaint();
			addMouseMotionListener(new Clicker());
			addMouseListener(new ML());
			w = getWidth();
			h = getHeight();
			//System.out.println(w);
			//introduce();
		}

		int w, h;

		int lineUnits = 10;
		int unitWidth;
		double xV = 2.5;
		int x0;
		int vecAmt = 2;
		boolean showInfo = true;
		double[] vecStarts, vecEnds;
		double ys[];
		public void introduce() {
			vecStarts = new double[vecAmt];
			vecEnds = new double[vecAmt];
			ys = new double[vecAmt];
			for (int i = 0; i < vecAmt; i++ ) {
				ys[i] = 0.2*h + 0.6*h/vecAmt*i;
				//System.out.println(ys[i]);
				vecStarts[i] = 0;
				vecEnds[i] = vecStarts[i] + xV;
			}
		}
		
		boolean drawAxes = true;
		boolean envelopInfo = false;
		
		boolean firstRender = true;
		
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			w = getWidth();
			h = getHeight();
			//System.out.println(h);
			if (firstRender) {
				introduce();
				//System.out.println(h);
				//System.out.println(ys[0]);
				//System.out.println(ys[1]);


				firstRender = false;
			}
			Font f = g.getFont();
			
			x0 = w / 2;
			unitWidth = w/lineUnits;
			Graphics2D g2d = (Graphics2D) g;
			int y = h / 2;
			g2d.setColor(Color.black);
			int arrowLen = 30;
			//System.out.println(ys[0]);
			if (drawAxes) {
				for (int i = 0; i<vecAmt; i++) {
					//int yVs = 
					g2d.drawLine(0, (int)ys[i], w, (int)ys[i]);
					if (envelopInfo) {
						g2d.drawLine(w, (int)ys[i], w-arrowLen, (int)ys[i]-arrowLen/2);
						g2d.drawLine(w, (int)ys[i], w-arrowLen, (int)ys[i]+arrowLen/2);
					}
					g2d.drawLine(x0, (int)ys[i]-5, x0, (int)ys[i]+5);
					g2d.drawString("0", x0, (int)ys[i]+15);
					
					
					for (int j = 1; j<=lineUnits/2; j++) {
						int xcoor = j*unitWidth + x0;
						g2d.drawLine(xcoor, (int)ys[i]-5, xcoor, (int)ys[i]+5);
						g2d.drawString(""+j, xcoor+5, (int)ys[i]+15);
						g2d.drawLine(2*x0 - xcoor, (int)ys[i]-5, 2*x0 - xcoor, (int)ys[i]+5);
						g2d.drawString("-"+j, 2*x0 - xcoor+5, (int)ys[i]+15);
						
					}
				}
			}
			g.setFont(f.deriveFont(Font.BOLD, 15f));
			g2d.setColor(Color.blue);
			g2d.setStroke(new BasicStroke(5));
			for (int i = 0; i<vecAmt; i++) {
				int xVs = x0 + (int)((vecStarts[i] + xV)*unitWidth);
				g2d.drawLine(x0+(int)(vecStarts[i]*unitWidth), (int)ys[i], xVs, (int)ys[i]);
				arrowLen = (int)(xV*unitWidth * 0.1);
				int arrowLen1 = Math.min(Math.abs(arrowLen), w/50);
				arrowLen1 = Math.max(Math.abs(arrowLen),  8);
				arrowLen = arrowLen1 * (int)Math.signum(arrowLen);
				//g2d.setStroke(new BasicStroke(3));
				g2d.drawLine(xVs, (int)ys[i], xVs-arrowLen, (int)ys[i]-arrowLen/3);
				g2d.drawLine(xVs, (int)ys[i], xVs-arrowLen, (int)ys[i]+arrowLen/3);
				if (showInfo) {
					//f.deriveFont(10);
					//System.out.println(f);
					//g.setFont(new Font("Dialog", Font.BOLD, 20));
					g.setFont(f.deriveFont(Font.BOLD, 15f));
					if (envelopInfo) {
						g.drawString("Iznos vektora: " + (int)(100*xV)/100.0, w/3, (int)ys[i]+40);
					}
					else {
						g.drawString("Duljina: " + (int)(100*Math.abs(xV))/100.0 + ", smjer: " + (xV<0 ? "lijevo" : "desno"), w/3, (int)ys[i]+40);
					}
				}
			}
			
		}
		
		int held = -1;

		class ML implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				held = -1;	
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
		class Clicker implements MouseMotionListener {

			
			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				
				
				int xe = e.getX(), ye = e.getY();
				
				//System.out.println("dragged " + xe + " " + ye);
				
				if (held >= 0) {
					vecStarts[held] = (xe-x0)*1.0/unitWidth;
				}
				else 
				for (int i = 0; i<vecAmt; i++) {
					int x0i = (int)(vecStarts[i]*unitWidth + x0);
					int dx = xe - x0i;
					int dy = ye - (int)ys[i];
					if (dx*dx + dy*dy < 9) {
						
					}
				}
				
				xV = xe;
				xV = (xV-x0)/unitWidth;
				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}}
		}

	
	class AdditionPanel1D extends JPanel {


		private static final long serialVersionUID = 1L;

		
		
		public AdditionPanel1D() {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 300));
			repaint();
			addMouseMotionListener(new Clicker());
			addMouseListener(new ML());
			w = getWidth();
			h = getHeight();
			//System.out.println(w);
			//introduce();
		}

		int w, h;

		int lineUnits = 10;
		int unitWidth;
		double xV1 = 2.5, xV2 = 2.5;
		int x0;
		int vecAmt = 2;
		boolean showInfo = true;
		double[] vecStarts, vecEnds;
		double ys[];
		public void introduce() {
			ys = new double[2];
			ys[0] = 0.1; ys[1] = 0.2;
			vecStarts = new double[2];
			vecEnds = new double[2];
			vecStarts[0] = 0; vecStarts[1] = 0;
			vecEnds[0] = xV1; vecEnds[1] = xV2;


		}
		
		boolean drawAxes = true;
		boolean envelopInfo = false;
		
		boolean firstRender = true;
		
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			w = getWidth();
			h = getHeight();
			//System.out.println(h);
			if (firstRender) {
				introduce();
				/*
				System.out.println(h);
				System.out.println(ys[0]);
				System.out.println(ys[1]);
*/
				
				firstRender = false;
				repaint();
			}
			Font f = g.getFont();
			
			x0 = w / 2;
			unitWidth = w/lineUnits;
			Graphics2D g2d = (Graphics2D) g;
			int y = h / 2;
			g2d.setColor(Color.black);
			int arrowLen = 30;
			//System.out.println(ys[0]);
			if (drawAxes) {
				for (int i = 0; i<2; i++) {
					g.drawLine(0, (int)(ys[i]*h), w, (int)(ys[i]*h));
					int arrowLength = 15;
					g.drawLine(w, (int)(ys[i]*h), w-arrowLength, (int)(ys[i]*h) + arrowLength/2);
					g.drawLine(w, (int)(ys[i]*h), w-arrowLength, (int)(ys[i]*h) - arrowLength/2);
					for (int j = -lineUnits/2; j<lineUnits/2; j++) {
						g.drawLine(x0+j*unitWidth, (int)(ys[i]*h)+5, x0+j*unitWidth, (int)(ys[i]*h)-5);
					}
				}
			}
			g.setFont(f.deriveFont(Font.BOLD, 15f));
			g2d.setColor(Color.blue);
			g2d.setStroke(new BasicStroke(5));
			for (int i = 0; i<2; i++) {
				g2d.drawLine(x0+(int)(vecStarts[i]*unitWidth), (int)(ys[i]*h), x0+(int)(vecEnds[i]*unitWidth), (int)(ys[i]*h));
			}
			
		}
		
		int held = -1;

		class ML implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				held = -1;	
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
		class Clicker implements MouseMotionListener {

			
			@Override
			public void mouseDragged(MouseEvent e) {}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}}
		
	}
	
	class DotProductPanel extends JPanel {



		private static final long serialVersionUID = 1L;

		public DotProductPanel(boolean horiz) {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 300));
			repaint();
			addMouseMotionListener(new Clicker());
			addMouseListener(new ML());
			w = getWidth();
			h = getHeight();
			fixed = true;
			this.horiz = horiz;
			//System.out.println(w);
			//introduce();
			
		}
		
		public DotProductPanel() {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 500));
			repaint();
			addMouseMotionListener(new Clicker());
			addMouseListener(new ML());
			w = getWidth();
			h = getHeight();
			//System.out.println(w);
			//introduce();
			
		}

		boolean fixed = false;
		boolean horiz;
		
		boolean formulas = false;

		
		int w, h;

		int lineUnits = 10;
		int unitWidth;
		double xV1 = 2.5, xV2 = 2.5;
		int x0;
		int vecAmt = 2;
		boolean showInfo = true;
		double[] vecStarts, vecEnds;
		double ys[] = new double[2];
		
		
		LocVektor vproj;
		LocVektor guideLine;
		
		LocVektor v1, v2;
		
		Graphics2D g2;
		
		public void paintLocLine(LocVektor lv) {
			g2.drawLine((int)lv.x0, (int)lv.y0, (int)(lv.x0+lv.x), (int)(lv.y0+lv.y));
		}
		double arrowLength = 10;
		LocVektor locArrow;
		double arrowHalfAngle = Math.PI/6;
		public void paintLocVektor(LocVektor lv) {
			//System.out.println(lv);
			paintLocLine(lv);
			lv.fix();
			if (lv.r < 2)
				return;
			
			double dxa = -lv.x/arrowLength, dya = -lv.y/arrowLength;
			double r = Math.sqrt(dxa*dxa + dya*dya);
			double an = Vektor.angle(dxa, dya);
			if (r<8) {
				r = 8;
				dxa = r * Math.cos(an);
				dya = r * Math.sin(an);
			}
			an += arrowHalfAngle;
			locArrow = new LocVektor(lv.x1, lv.y1, r*Math.cos(an), r*Math.sin(an));
			paintLocLine(locArrow);
			an -= 2*arrowHalfAngle;
			locArrow.x = r*Math.cos(an);
			locArrow.y = r*Math.sin(an);
			paintLocLine(locArrow);
			
			/*
			locArrow = lv;
			locArrow.fix();
			//g2.setColor(Color.BLACK);
			//System.out.println(locArrow);
			locArrow.x0 += lv.x;
			locArrow.y0 += lv.y;
			locArrow.fix();
			//System.out.println(locArrow);
			//lv.rotate(Math.PI/2);
			//paintLocLine(locArrow);
			//g2.setColor(newCol);

			
			locArrow.scaleToR(-arrowLength);
			//System.out.println(locArrow.r);
			//System.out.println(lv);
			locArrow.fix();
			//System.out.println(lv);
			locArrow.rotate(arrowHalfAngle);
			//System.out.println(locArrow.r);
			paintLocLine(locArrow);
			locArrow.rotate(-2 * arrowHalfAngle);
			//g2.setColor(Color.ORANGE);
			paintLocLine(locArrow);
			locArrow = null;
			*/
		}
		
		public void introduce() {
			ys[0] = 0.5;
			if (!fixed) {
				v1 = new LocVektor(w/2, h/2, w*0.2, h*0.4);
				v2 = new LocVektor(w/2, h/2, w*0.3, -h*0.2);
			}
			else {
				v1 = new LocVektor(w/2, h/2, w*0.2, h*0.4);
				v2 = new LocVektor(w/2, h/2, w*0.3, 0);
			}
			vproj = v1.projOn(v2);
			vproj.x0=v1.x0; vproj.y0=v1.y0;
			
			guideLine = new LocVektor(v1.x1, v1.y1, vproj.x1, vproj.y1, true);
		}
		
		boolean drawAxes = true;
		boolean envelopInfo = false;
		
		boolean firstRender = true;
		
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			w = getWidth();
			h = getHeight();
			g2 = (Graphics2D)g;
			//System.out.println(h);
			if (firstRender) {
				firstRender =false;
				repaint();
				introduce();
				/*
				System.out.println(h);
				System.out.println(ys[0]);
				System.out.println(ys[1]);
*/
				
				
			}
			double minLength = Math.sqrt(w*w+h*h);
			v1.fix(); v2.fix();
			LocVektor extendedv2 = v2.copy();
			extendedv2.scaleToR(minLength);
			paintLocLine(extendedv2);
			extendedv2.scale(-1);
			paintLocLine(extendedv2);
			Font f = g.getFont();
			g2.setColor(Color.blue);
			g2.setStroke(new BasicStroke(3));
			
			paintLocVektor(v1);
			g2.setColor(Color.green);
			paintLocVektor(v2);
			
			g2.setStroke(new BasicStroke(4));
			g2.setColor(Color.red);
			paintLocVektor(vproj);
			
			g2.setStroke(new BasicStroke(1));
			g2.setColor(Color.black);
			paintLocLine(guideLine);
			
			if (formulas) {
				
				g.drawString("v", (int)v1.x1, (int)v1.y1);
				g.drawString("u", (int)v2.x1, (int)v2.y1);

				int x1 = (int)(v1.x)/10, x2 = (int)(v2.x)/10, y1 = (int)(v1.y)/10, y2 = (int)(v2.y)/10;
				int prod = x1*x2 + y1*y2;
				String prodCalc = "v·u = (" + x1 + ", " + y1 + ") · (" + x2 + ", " + y2 + ") = " +
						x1 + "·" + x2 + " + " + y1 + "·" + y2 + " = " + prod;
				g.drawString(prodCalc, 200, 450);
				
				int r = 30;
				double a1 = LocVektor.angle(v1.x, -v1.y);
				double a2 = LocVektor.angle(v2.x, -v2.y);
				
				double delta = LocVektor.closestAngleFromTo(a1, a2);
				g.drawArc((int)(v1.x0-r), (int)(v1.y0-r), 2*r, 2*r, (int)(180/Math.PI*a1), (int)(180/Math.PI*delta));

				
				/*
				int first = LocVektor.whichFirstCCW(a1, a2);
				System.out.println(a1);
				double firstAngle;
				double da;
				if (first==1) {
					da = a2-a1;
					firstAngle = a1;
				}
				else {
					da = a1-a2;
					firstAngle = a2;
				}
				//System.out.println(a1);
				//System.out.println(a1 + " " + da);
				g.drawArc((int)(v1.x0-r), (int)(v1.y0-r), 2*r, 2*r, (int)(180/Math.PI*firstAngle), (int)(180/Math.PI*da));
				*/
			}
			
		}
		
		int held = -1;

		class ML implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				int x = e.getX(), y = e.getY();
				//System.out.println(x);
				double xv = v1.x1, yv = v1.y1;
				if (Math.abs(x-xv) + Math.abs(y-yv) < 10) {
					held = 1;
				}
				else {
					xv = v2.x1; yv = v2.y1;
					if (Math.abs(x-xv) + Math.abs(y-yv) < 5) {
						held = 2;
					}
					else {
						held = -1;
					}
				}
				//System.out.println(held);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				held = -1;	
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
		class Clicker implements MouseMotionListener {

			
			@Override
			public void mouseDragged(MouseEvent e) {
				//System.out.println(e.getX());
				//System.out.println("dragging: " + held);
				if (held == 1) {
					v1.x1 = e.getX();
					v1.y1 = e.getY();
					v1.fixLength();
					//System.out.println(v1);
				}
				else if (held == 2) {
					//System.out.println(v2);
					v2.x1 = e.getX();
					if (!fixed)
						v2.y1 = e.getY();
					v2.fixLength();
					//System.out.println(v2);
				}
				vproj = v1.projOn(v2);
				vproj.fixLength();
				vproj.x0=v1.x0; vproj.y0=v1.y0;
				guideLine = new LocVektor(v1.x1, v1.y1, vproj.x1, vproj.y1, true);

				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}}
		
	
	}
	
	
	
	class CrossProductPanel extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public CrossProductPanel() {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 300));
			repaint();
			addMouseMotionListener(new Clicker());
			w = getWidth();
			h = getHeight();
			v1=new LocVektor();v2=new LocVektor();
			v1.x0=x0; v2.x0=x0;
			v1.y0=y0; v2.y0=y0;
			v1.x=100;v1.y=-100;
			v2.x=-100;v2.y=-100;
			an=Math.sin(v2.angleFrom(v1));
			
			
		}
		
		double an = 0;
		
		
		int held = -1;
		
		int w, h;
		
		int x0=200, y0=200;
		LocVektor v1, v2;
		
		
		public void paintLocLine(LocVektor lv) {
			g2.drawLine((int)lv.x0, (int)lv.y0, (int)(lv.x0+lv.x), (int)(lv.y0+lv.y));
		}
		double arrowLength = 10;
		LocVektor locArrow;
		double arrowHalfAngle = Math.PI/6;
		public void paintLocVektor(LocVektor lv) {
			//System.out.println(lv);
			paintLocLine(lv);
			lv.fix();
			if (lv.r < 2)
				return;
			
			double dxa = -lv.x/arrowLength, dya = -lv.y/arrowLength;
			double r = Math.sqrt(dxa*dxa + dya*dya);
			double an = Vektor.angle(dxa, dya);
			if (r<8) {
				r = 8;
				dxa = r * Math.cos(an);
				dya = r * Math.sin(an);
			}
			an += arrowHalfAngle;
			locArrow = new LocVektor(lv.x1, lv.y1, r*Math.cos(an), r*Math.sin(an));
			paintLocLine(locArrow);
			an -= 2*arrowHalfAngle;
			locArrow.x = r*Math.cos(an);
			locArrow.y = r*Math.sin(an);
			paintLocLine(locArrow);
			
			/*
			locArrow = lv;
			locArrow.fix();
			//g2.setColor(Color.BLACK);
			//System.out.println(locArrow);
			locArrow.x0 += lv.x;
			locArrow.y0 += lv.y;
			locArrow.fix();
			//System.out.println(locArrow);
			//lv.rotate(Math.PI/2);
			//paintLocLine(locArrow);
			//g2.setColor(newCol);

			
			locArrow.scaleToR(-arrowLength);
			//System.out.println(locArrow.r);
			//System.out.println(lv);
			locArrow.fix();
			//System.out.println(lv);
			locArrow.rotate(arrowHalfAngle);
			//System.out.println(locArrow.r);
			paintLocLine(locArrow);
			locArrow.rotate(-2 * arrowHalfAngle);
			//g2.setColor(Color.ORANGE);
			paintLocLine(locArrow);
			locArrow = null;
			*/
		}
		
		Graphics2D g2;
		
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g2=(Graphics2D)g;
			g2.setStroke(new BasicStroke(3));
			paintLocVektor(v1);paintLocVektor(v2);
			double r = Math.abs(an*20);
			g2.fillOval((int)(v1.x0-r),(int)(v1.y0-r), (int)(2*r),(int)(2*r));
		}
		
		
		class Clicker implements MouseMotionListener {

			
			@Override
			public void mouseDragged(MouseEvent e) {
				int x = e.getX(), y = e.getY();
				
				if (held == -1) {
					if (Math.abs(v1.x1-x)+Math.abs(v1.y1-y)<10) {
						held = 1;
					}
					else if (Math.abs(v2.x1-x)+Math.abs(v2.y1-y)<10) {
						held = 2;
					}
					
				}
				
				if (Math.abs(v1.x1-x)+Math.abs(v1.y1-y)<10) {
					held = 1;
					System.out.println(1);
					v1.x1=x;v1.y1=y;
					v1.fixLength();
				}
				else if (Math.abs(v2.x1-x)+Math.abs(v2.y1-y)<10) {
					held = 2;
					System.out.println(1);

					v2.x1=x;v2.y1=y;
					v2.fixLength();
				}
				else {
					held = -1;
					return;
				}
				an = v2.angleFrom(v1);
				an = Math.sin(an);
				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		}
		
		class Mouse implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				held = -1;
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
	}
		
	}
	
	

