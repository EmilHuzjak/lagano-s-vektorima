package mainpackage;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class ScoresPanel extends JPanel {

	
	
	
	
	List<String> filepaths = new ArrayList<>();
	
	JScrollPane scroll = new JScrollPane();
	JPanel controls = new JPanel();
	JButton returnB = new JButton("Natrag na naslovnu");
	
	JMenuBar bar = new JMenuBar();
	
	JMenu menu1 = new JMenu("Pregled zadatka");
	
	JMenuItem i1 = new JMenuItem("Crtanje zadanog vektora");
	
	JTextArea scoresTA = new JTextArea();
	
	public ScoresPanel() {
		returnB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
					// TODO Auto-generated method stub
					Container par = getParent();
					par.removeAll();
					par.add(new StartPanel());
					par.revalidate();
					par.repaint();
				
			}
		});
		
		filepaths.add("scores.txt");
		
		
		
		
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				setLayout(new BoxLayout(ScoresPanel.this, BoxLayout.PAGE_AXIS));
				initializeMenu();
				initGUI();
				revalidate();
				repaint();
				fixScroll();
			}
			
		});
		
	}
	
	public void initializeMenu() {
		menu1.add(i1);
		bar.add(menu1);
		controls.add(bar);
	}
	
	public void initGUI() {
		
		controls.add(returnB);
		add(controls);
		
		if (!filepaths.isEmpty()) {
			fetch("" + filepaths.get(0));
		}
		//System.out.println(scoresTA.getText());
		scroll = new JScrollPane(scoresTA);
		add(scroll);
		
	}
	
	public void fixScroll() {
		scroll.setPreferredSize(new Dimension(getWidth(), getHeight()-controls.getHeight()));
	}
	
	
	
	public void fetch(String filepath) {

		try {
			File f = new File(filepath);
			Scanner sc = new Scanner(f);
			List<String> score = new ArrayList<String>();
			int i = 0;
			while (sc.hasNextLine()) {
				// System.out.println(i++);
				String line = sc.nextLine();
				if (line.equals("***")) {
					/*
					 * score.clear(); score.add(sc.nextLine()); score.add(sc.nextLine()); line =
					 * sc.nextLine(); score.add(line.split(" ")[0]); score.add(line.split(" ")[1]);
					 * scores.add(score); System.out.print(score); System.out.println(scores);
					 */

					scoresTA.append("Ime: " + sc.nextLine() + "\n");
					int time = Integer.parseInt(sc.nextLine());
					scoresTA.append("Trajanje: " + time + "s\n");
					String[] hits = sc.nextLine().split(" ");

					int correct = Integer.parseInt(hits[0]);
					int incorrect = Integer.parseInt(hits[1]);

					scoresTA.append("pogodaka: " + correct + "\n");
					scoresTA.append("Brzina: " + (int)(time * 1.0 / correct*100)/100.0 + " s\n");
					scoresTA.append("Tocnost: " + (int) (100 * correct * 1.0 / (correct + incorrect)) + "%\n\n\n");
				}
			}
			sc.close();
		} catch (IOException e1) {
			System.out.println(e1.getMessage());
		}
	
	}
	
	
}
