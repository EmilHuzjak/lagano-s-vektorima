package mainpackage;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;

public class TriangleAdditionPanel extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Vektor[] vektori;
	public Vektor[] startPoz;
	public Vektor start = new Vektor(200, 200);

	public Vektor[] vektori1;
	public Vektor[] startPoz1;

	public Vektor result = new Vektor(0,0);

	int id;
	Color newCol = new Color(150,150,50);
	Color[] boje;
	int numVekt = 0;
	int numMax = 10;
	
	LocVektor newVek = new LocVektor();
	boolean isNewVek = false;
	
	int heldVektor = -1;
	int heldPos = -1;
	
	Vektor addedNow = new Vektor(0,0);
	Vektor addedStart = new Vektor(0,0);

	TriangleAdditionPanel t = this;

	public TriangleAdditionPanel() {
		vektori = new Vektor[numMax];
		boje = new Color[numMax];
		for (int i = 0; i < numMax; i++) {
			boje[i] = Color.blue;
		}

		Dragger d = new Dragger();
		Clicker c = new Clicker();
		addMouseMotionListener(d);
		addMouseListener(c);
		
		addVektor(100,100);
		
		// getRootPane().setBackground(Color.green);
	}

	public void addVektor(double x, double y) {
		vektori[numVekt] = new Vektor(x, y);
		numVekt++;

		result.x += x;
		result.y += y;

		/*
		 * for (int i = 0; i<numVekt; i++) { System.out.print(vektori[i] + ",   "); }
		 */
	}

	int pointR = 5;

	Graphics2D g2;
	boolean init = true;

	public void paintLocLine(LocVektor lv) {
		g2.drawLine((int)lv.x0, (int)lv.y0, (int)(lv.x0+lv.x), (int)(lv.y0+lv.y));
	}
	
	public void paintLocVektor(LocVektor lv) {
		paintLocLine(lv);
		/*
		locArrow = lv;
		locArrow.fix();
		//g2.setColor(Color.BLACK);
		//System.out.println(locArrow);
		locArrow.translate(lv);
		//System.out.println(locArrow);
		//lv.rotate(Math.PI/2);
		//paintLocLine(locArrow);
		//g2.setColor(newCol);

		
		locArrow.scaleToR(-arrowLength);
		//System.out.println(locArrow.r);
		//System.out.println(lv);
		locArrow.fix();
		//System.out.println(lv);
		locArrow.rotate(arrowHalfAngle);
		//System.out.println(locArrow.r);
		paintLocLine(locArrow);
		locArrow.rotate(-2 * arrowHalfAngle);
		//g2.setColor(Color.ORANGE);
		paintLocLine(locArrow);
		*/
	}
	
	
	
	Vektor arrowSide;
	LocVektor locArrow;
	double arrowLength = 20;
	double arrowHalfAngle = 0.7* Math.PI / 6;
	
	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Color c = new Color(150, 255, 255);
		// System.out.println(c);
		g.setColor(c);
		g.fillRect(0, 0, 1500, 1000);

		g.setColor(new Color(0, 100, 100));
		g.setFont(new Font("Arial", 0, 30));
		g.drawString("Nacrtaj vektore koje zelis zbrojiti", 50, 50);

		g2 = (Graphics2D) g;

		g2.setStroke(new BasicStroke(3));
		double x0 = start.x, y0 = start.y;
		double x = x0, y = y0;
		for (int i = 0; i < numVekt; i++) {
			x += vektori[i].x;
			y += vektori[i].y;
			// System.out.println(i);
			g2.setPaint(boje[i]);
			g2.drawLine((int) x0, (int) y0, (int) x, (int) y);
			arrowSide = new Vektor(x-x0, y-y0);

			arrowSide.scaleToR(arrowLength);
			arrowSide.rotate(arrowHalfAngle);
			//System.out.println(arrowSide.x);
			g2.drawLine((int) x, (int) y, (int) (x-arrowSide.x), (int) (y-arrowSide.y));
			arrowSide.rotate(-2*arrowHalfAngle);
			g2.drawLine((int) x, (int) y, (int) (x-arrowSide.x), (int) (y-arrowSide.y));
			
			g2.fillOval((int) (x0 - pointR), (int) (y0 - pointR), pointR * 2, pointR * 2);
			x0 += vektori[i].x;
			y0 += vektori[i].y;
			/*
			 * 
			 * g2.fillOval((int) (startPoz[i].x + vektori[i].x) - pointR, (int)
			 * (startPoz[i].y + vektori[i].y) - pointR, pointR * 2, pointR * 2);
			 */

		}
		
		if (isNewVek) {
			g2.setColor(newCol);
			paintLocVektor(newVek);
			g2.setColor(Color.BLUE);
		}
		
		if (numVekt < 2)
			return;
		g2.setColor(Color.RED);
		g2.setStroke(new BasicStroke(3));
		g2.drawLine((int) start.x, (int) start.y, (int) x, (int) y);
		
		//g2.setStroke(new BasicStroke(2));
		//result.fix();
		//System.out.println("ra: " + result.angle);
		arrowSide = new Vektor(x-start.x, y-start.y);
		arrowSide.fix();
		//System.out.println(result.angle);
		//System.out.println(arrowSide.angle);
		
		arrowSide.scaleToR(arrowLength);
		arrowSide.rotate(arrowHalfAngle);
		//System.out.println(arrowSide.x);
		g2.drawLine((int) x, (int) y, (int) (x-arrowSide.x), (int) (y-arrowSide.y));
		arrowSide.rotate(-2*arrowHalfAngle);
		g2.drawLine((int) x, (int) y, (int) (x-arrowSide.x), (int) (y-arrowSide.y));

		

		
	}

	boolean adding = false;
	boolean addFirst = false;

	void addVektor() {
		if (numVekt >= numMax) {
			return;
		}
		adding = true;
		addFirst = true;
	}

	void clear() {
		numVekt = 0;
		repaint();
	}
	
	
	private class MyActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	double xg, yg;
	class Clicker implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

			int held = -1;

			if (adding) {
				
				if (addFirst) {
					addedStart = new Vektor(e.getX(), e.getY());
					newVek = new LocVektor(e.getX(), e.getY(), 0, 0);
					addFirst = false;
					isNewVek = true;
				} else {
					addedNow = new Vektor(e.getX()-addedStart.x, e.getY()-addedStart.y);
					adding = false;
					newVek.x = addedNow.x;
					newVek.y = addedNow.y;
					newVek.fix();
					//isNewVek = true;
					vektori[numVekt] = addedNow;
					numVekt++;

				}
				repaint();

				return;
			}
			xg = start.x; t.yg = start.y;
			for (int i = 0; i < numVekt; i++) {
				t.xg += vektori[i].x;
				t.yg += vektori[i].y;

				if (Math.abs(xg - e.getX()) + Math.abs(yg - e.getY()) < pointR + 2) {
					held = i;
					t.xg -= vektori[i].x; t.yg -= vektori[i].y;
					break;
				}
			}
			if (held >= 0) {
				heldVektor = held;
				return;
			} else
				heldVektor = -1;
			
			xg = start.x; yg = start.y;
			if (Math.abs(xg - e.getX()) + Math.abs(yg - e.getY()) < pointR + 2) {
				heldPos = 0;
				//System.out.println("pos");
				//t.xg -= vektori[i].x; t.yg -= vektori[i].y;
				//break;
			}
			else {
				if (heldPos == 0) {
					heldPos = -1;
				}
			}
			
/*
			int heldP = -1;
			for (int i = 0; i < numVekt; i++) {
				double xg = startPoz[i].x;
				double yg = startPoz[i].y;

				if (Math.abs(xg - e.getX()) + Math.abs(yg - e.getY()) < pointR) {
					heldP = i;
					break;
				}
			}
			if (heldP >= 0) {
				heldPos = heldP;
			} else
				heldPos = -1;
*/
			// System.out.println(e.getX() + id);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			heldPos = -1;
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	class Dragger implements MouseMotionListener {

		@Override
		public void mouseDragged(MouseEvent e) {
			// TODO Auto-generated method stub
			
			if (heldPos == 0) {
				start = new Vektor(e.getX(), e.getY());
				t.repaint();
				return;
			}
			if (heldVektor == 0) {
				vektori[0].x = e.getX() - start.x;
				vektori[0].y = e.getY() - start.y;
				t.repaint();
			}
			else if (heldVektor > 0) {
				vektori[heldVektor].x = e.getX() - xg;
				vektori[heldVektor].y = e.getY() - yg;
				t.repaint();
			}


		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			if (adding && !addFirst) {
				newVek.x = e.getX() - newVek.x0;
				newVek.y = e.getY() - newVek.y0;
				repaint();
			}
		}

	}

}
