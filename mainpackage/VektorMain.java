package mainpackage;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;

public class VektorMain {

	static boolean triangles = false;
	static int id = 0;
	
	static  boolean isTp = false;

	static VektorFrame vf;
	static ActionListener listener;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int w = 900, h = 500;
		
		
		listener = new ActionListener() {


			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//System.out.println(e);
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						

						
						

						
						JSplitPane jsp = new JSplitPane();
						jsp.setOrientation(JSplitPane.VERTICAL_SPLIT);

						// int nums = 10;

						VektorPanel vp = new VektorPanel();
						TriangleAdditionPanel tap = new TriangleAdditionPanel();

						JComponent[] comps = { vp, tap };
						TriangleSubtractionPanel sub = new TriangleSubtractionPanel();
						
						TaskPanel tp = new TaskPanel();
						
						vp.repaint();
						vp.id = 1;
						jsp.setDividerLocation(100);
						JPanel butPanel = new JPanel();
						JPanel taskButPanel = new JPanel();
						JButton clearB = new JButton("ocisti sve");
						clearB.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (id == 0) {
									vp.clear();
								} else if (id == 1) {
									tap.clear();
									tap.isNewVek = false;
								} else if (id == 2) {
									sub.clear();
								}
							}
						});
						butPanel.add(clearB);

						JButton clearOne = new JButton("ocisti zadnji vektor");
						clearOne.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if (id == 0) {
									if (vp.numVekt > 0) {
										vp.numVekt--;
										vp.repaint();
									}
								} else if (id == 1) {
									if (tap.numVekt > 0) {
										tap.isNewVek = false;
										tap.numVekt--;
										tap.repaint();
									}
								}

							}
						});
						butPanel.add(clearOne);

						JButton addB = new JButton("dodaj");
						addB.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								switch (id) {
								case 0:
									vp.addVektor();
									break;
								case 1:
									tap.addVektor();
									break;
								case 2:
									sub.addVektor();
								}
								/*
								 * if (id == 0) vp.addVektor(); else tap.addVektor();
								 */
							}
						});

						JButton changeBut = new JButton("Idi na pravilo trokuta");
						changeBut.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								jsp.remove(2);
								if (id == 1) {
									id = 0;
									changeBut.setText("Idi na pravilo trokuta");
								} else {
									id = 1;
									changeBut.setText("Idi na pravilo paralelograma");
								}
								jsp.setBottomComponent(comps[id]);

								triangles = !triangles;
							}
						});
						JButton subBut = new JButton("Idi na oduzimanje vektora");
						subBut.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								if (id != 2) {
									id = 2;
									jsp.setBottomComponent(sub);
								}
							}

						});
						
						JButton fastBut = new JButton("Brze");
						fastBut.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								if (id == 2) {
									
									int cap = 100;
									
									if (sub.milisanim <= cap) {
										return;
									}
									sub.milisanim /= 2;
									if (sub.milisanim < cap) {
										sub.milisanim = cap;
									}
									
									sub.changedAnim = true;
									System.out.println(sub.milisanim);

								}
							}

						});
						JButton slowBut = new JButton("Sporije");
						slowBut.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								if (id == 2) {
									
									if (sub.milisanim >= 5000) {
										return;
									}
									
									
									sub.milisanim *= 1.5;
									if (sub.milisanim > 5000) {
										sub.milisanim = 5000;
									}
									sub.changedAnim = true;
									System.out.println(sub.milisanim);
									
								}
							}

						});
						

						JButton taskBut = new JButton("Vjezbe");
						JButton lecBut = new JButton("Demonstracija");
						JButton helpBut = new JButton("Pomoc");

						
						JButton playBut = new JButton("Zapocni igru");
						playBut.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								if (!tp.readyToPlay) {
									tp.readyToPlay = true;
									playBut.setText("Prekini igru");
									
								}
								else {
									tp.readyToPlay = false;
									tp.playing = false;
									playBut.setText("Zapocni igru");
								}
							}
						});
						
						//taskButPanel.add(lecBut);
						//taskButPanel.add(helpBut);
						
						JPanel playSettings = new JPanel();
						
						
						
						
						taskBut.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								if (isTp) return;
								//jsp.remove(1);
								//jsp.setDividerLocation(50);
								//System.out.println(taskButPanel);
								//jsp.setTopComponent(taskButPanel);
								jsp.remove(2);
								jsp.setBottomComponent(tp);
								isTp = true;
								//System.out.println(jsp);
								jsp.setDividerLocation(80);
								
								butPanel.remove(taskBut);
								butPanel.add(playBut);
								butPanel.add(lecBut);
								
							}

						});
						lecBut.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								if (!isTp) return;
								
								//jsp.remove(1);
								//jsp.setTopComponent(butPanel);
								
								if (id==2) {
									jsp.setBottomComponent(sub);
								}
								else {
									jsp.setBottomComponent(comps[id]);
								}
								jsp.setDividerLocation(80);
								isTp = false;
								
								butPanel.add(taskBut);
								butPanel.remove(playBut);
								butPanel.remove(lecBut);
								
							}

						});

						
						helpBut.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								boolean help = tp.help;
								if (help) {
									helpBut.setText("Pomoc");
									tp.help = false;
								}
								else {
									helpBut.setText("Ne treba pomoc");
									tp.help = true;
								}
								
								
								
							}

						});
						
						JButton scoresBut = new JButton("Rezultati");
						boolean showingScores = false;
						scoresBut.addActionListener(new ActionListener() {

							@Override
							public void actionPerformed(ActionEvent e) {
								jsp.remove(2);
								jsp.setBottomComponent(vf.giveScorePanel());
								
							}

						});
						
						JButton lessonBut = new JButton("Gradivo");
						lessonBut.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								/*
								vf.remove(jsp);
								vf.add(new JScrollPane(vf.lessonP));
								*/
								SwingUtilities.invokeLater(new Runnable() {

									@Override
									public void run() {
										// TODO Auto-generated method stub
										jsp.remove(2);
										JScrollPane jscrollp = new JScrollPane(vf.lessonP);
										jscrollp.getVerticalScrollBar().setUnitIncrement(16);
										
										jsp.setBottomComponent(jscrollp);
										jsp.revalidate();
										jscrollp.revalidate();
									}
									
									
								});
								
							}

						});
						
						
						butPanel.add(addB);
						butPanel.add(changeBut);
						butPanel.add(subBut);
						butPanel.add(fastBut);
						butPanel.add(slowBut);
						butPanel.add(taskBut);
						butPanel.add(lecBut);
						butPanel.add(helpBut);
						butPanel.add(scoresBut);
						butPanel.add(lessonBut);
						
						
						JButton returnB = new JButton("Natrag na naslovnu");
						
						returnB.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
									// TODO Auto-generated method stub
									Container par = vf.getContentPane();
									par.removeAll();
									par.add(new StartPanel());
									par.revalidate();
									par.repaint();
								
							}
						});
						
						butPanel.add(returnB);
						
						jsp.setTopComponent(taskButPanel);
						jsp.setTopComponent(butPanel);
						jsp.setBottomComponent(vp);
						vf.getContentPane().removeAll();
						//vf.setContentPane(new ContentPane());
						vf.getContentPane().add(jsp);
						vf.getContentPane().setBackground(Color.CYAN);
						//vp.repaint();
						//jsp.repaint();
						vf.revalidate();
						vf.repaint();
					}
				});
				
			}
			
		};
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				
				StartPanel sp = new StartPanel();
				sp.l = listener;
				vf = new VektorFrame(w,h);
				vf.add(sp);
				//vf.pack();
				//vf.add(new Calculator());
				vf.revalidate();
				
				
			}
		});
		
		
		if (false) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				


				//vf = new VektorFrame(w, h);
				vf.removeAll();
				JSplitPane jsp = new JSplitPane();
				jsp.setOrientation(JSplitPane.VERTICAL_SPLIT);

				// int nums = 10;

				VektorPanel vp = new VektorPanel();
				TriangleAdditionPanel tap = new TriangleAdditionPanel();

				JComponent[] comps = { vp, tap };
				TriangleSubtractionPanel sub = new TriangleSubtractionPanel();
				
				TaskPanel tp = new TaskPanel();
				
				vp.repaint();
				vp.id = 1;
				jsp.setDividerLocation(100);
				JPanel butPanel = new JPanel();
				JPanel taskButPanel = new JPanel();
				JButton clearB = new JButton("ocisti sve");
				clearB.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (id == 0) {
							vp.clear();
						} else if (id == 1) {
							tap.clear();
							tap.isNewVek = false;
						} else if (id == 2) {
							sub.clear();
						}
					}
				});
				butPanel.add(clearB);

				JButton clearOne = new JButton("ocisti zadnji vektor");
				clearOne.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (id == 0) {
							if (vp.numVekt > 0) {
								vp.numVekt--;
								vp.repaint();
							}
						} else if (id == 1) {
							if (tap.numVekt > 0) {
								tap.isNewVek = false;
								tap.numVekt--;
								tap.repaint();
							}
						}

					}
				});
				butPanel.add(clearOne);

				JButton addB = new JButton("dodaj");
				addB.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						switch (id) {
						case 0:
							vp.addVektor();
							break;
						case 1:
							tap.addVektor();
							break;
						case 2:
							sub.addVektor();
						}
						/*
						 * if (id == 0) vp.addVektor(); else tap.addVektor();
						 */
					}
				});

				JButton changeBut = new JButton("Idi na pravilo trokuta");
				changeBut.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						jsp.remove(2);
						if (id == 1) {
							id = 0;
							changeBut.setText("Idi na pravilo trokuta");
						} else {
							id = 1;
							changeBut.setText("Idi na pravilo paralelograma");
						}
						jsp.setBottomComponent(comps[id]);

						triangles = !triangles;
					}
				});
				JButton subBut = new JButton("Idi na oduzimanje vektora");
				subBut.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (id != 2) {
							id = 2;
							jsp.setBottomComponent(sub);
						}
					}

				});
				
				JButton fastBut = new JButton("Brze");
				fastBut.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (id == 2) {
							
							int cap = 100;
							
							if (sub.milisanim <= cap) {
								return;
							}
							sub.milisanim /= 2;
							if (sub.milisanim < cap) {
								sub.milisanim = cap;
							}
							
							sub.changedAnim = true;
							System.out.println(sub.milisanim);

						}
					}

				});
				JButton slowBut = new JButton("Sporije");
				slowBut.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (id == 2) {
							
							if (sub.milisanim >= 5000) {
								return;
							}
							
							
							sub.milisanim *= 1.5;
							if (sub.milisanim > 5000) {
								sub.milisanim = 5000;
							}
							sub.changedAnim = true;
							System.out.println(sub.milisanim);
							
						}
					}

				});
				

				JButton taskBut = new JButton("Vjezbe");
				JButton lecBut = new JButton("Demonstracija");
				JButton helpBut = new JButton("Pomoc");

				
				JButton playBut = new JButton("Zapocni igru");
				playBut.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (!tp.readyToPlay) {
							tp.readyToPlay = true;
							playBut.setText("Prekini igru");
							
						}
						else {
							tp.readyToPlay = false;
							tp.playing = false;
							playBut.setText("Zapocni igru");
						}
					}
				});
				
				//taskButPanel.add(lecBut);
				//taskButPanel.add(helpBut);
				
				JPanel playSettings = new JPanel();
				
				
				
				
				taskBut.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (isTp) return;
						//jsp.remove(1);
						//jsp.setDividerLocation(50);
						//System.out.println(taskButPanel);
						//jsp.setTopComponent(taskButPanel);
						jsp.remove(2);
						jsp.setBottomComponent(tp);
						isTp = true;
						//System.out.println(jsp);
						jsp.setDividerLocation(80);
						
						butPanel.remove(taskBut);
						butPanel.add(playBut);
						butPanel.add(lecBut);
						
					}

				});
				lecBut.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						if (!isTp) return;
						
						//jsp.remove(1);
						//jsp.setTopComponent(butPanel);
						
						if (id==2) {
							jsp.setBottomComponent(sub);
						}
						else {
							jsp.setBottomComponent(comps[id]);
						}
						jsp.setDividerLocation(80);
						isTp = false;
						
						butPanel.add(taskBut);
						butPanel.remove(playBut);
						butPanel.remove(lecBut);
						
					}

				});

				
				helpBut.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						boolean help = tp.help;
						if (help) {
							helpBut.setText("Pomoc");
							tp.help = false;
						}
						else {
							helpBut.setText("Ne treba pomoc");
							tp.help = true;
						}
						
						
						
					}

				});
				
				JButton scoresBut = new JButton("Rezultati");
				boolean showingScores = false;
				scoresBut.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						jsp.remove(2);
						jsp.setBottomComponent(vf.giveScorePanel());
						
					}

				});
				
				JButton lessonBut = new JButton("Gradivo");
				lessonBut.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						/*
						vf.remove(jsp);
						vf.add(new JScrollPane(vf.lessonP));
						*/
						SwingUtilities.invokeLater(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								jsp.remove(2);
								JScrollPane jscrollp = new JScrollPane(vf.lessonP);
								jscrollp.getVerticalScrollBar().setUnitIncrement(16);
								
								jsp.setBottomComponent(jscrollp);
								jsp.revalidate();
								jscrollp.revalidate();
							}
							
							
						});
						
					}

				});
				
				
				butPanel.add(addB);
				butPanel.add(changeBut);
				butPanel.add(subBut);
				butPanel.add(fastBut);
				butPanel.add(slowBut);
				butPanel.add(taskBut);
				butPanel.add(lecBut);
				butPanel.add(helpBut);
				butPanel.add(scoresBut);
				butPanel.add(lessonBut);
				jsp.setTopComponent(taskButPanel);
				jsp.setTopComponent(butPanel);
				jsp.setBottomComponent(vp);
				vf.getContentPane().add(jsp);
				vf.getContentPane().setBackground(Color.CYAN);
				//vp.repaint();
				//jsp.repaint();
				vf.repaint();
			}
		});
		}

	}

}
