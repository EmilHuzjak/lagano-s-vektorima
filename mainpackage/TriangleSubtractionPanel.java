package mainpackage;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;
import javax.swing.Timer;

public class TriangleSubtractionPanel extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public LocVektor v1, v2;
	public LocVektor v2r;
	
	public LocVektor res;
	boolean drawRes = false;
	
	public LocVektor vdr, vdr1;
	
	
	public Vektor[] vektori;
	public Vektor[] startPoz;
	
	public Vektor[] vektori1;
	public Vektor[] startPoz1;
	
	int numsub = 0;
	
	int id;
	Color[] boje;
	int numVekt = 0;
	int numMax = 10;

	int heldVektor = -1;
	int heldPos = -1;

	
	boolean guideActive = false;
	LocVektor guide;
	
	
	TriangleSubtractionPanel t = this;

	public TriangleSubtractionPanel() {
		vektori = new Vektor[numMax];
		startPoz = new Vektor[numMax];
		boje = new Color[numMax];
		for (int i = 0; i < numMax; i++) {
			boje[i] = Color.blue;
		}
		boje[0] = Color.green;
		
		Dragger d = new Dragger();
		Clicker c = new Clicker();
		addMouseMotionListener(d);
		addMouseListener(c);
		//getRootPane().setBackground(Color.green);
	}

	public void addVektor(double x0, double y0, double x1, double y1) {
		startPoz[numVekt] = new Vektor(x0, y0);
		vektori[numVekt] = new Vektor(x1 - x0, y1 - y0);
		numVekt++;
		
		if (v1 == null) {
			v1 = new LocVektor(x0, y0, x1, y1, true);
		}
		else {
			v2 = new LocVektor(x0, y0, x1, y1, true);
		}

		

		
		/*
		for (int i = 0; i<numVekt; i++) {
			System.out.print(vektori[i] + ",   ");
		}
		*/
	}

	int pointR = 5;

	Graphics2D g2;
	boolean init = true;

	Color newCol = new Color(100, 150, 50);
	
	
	public void paintLocLine(LocVektor lv) {
		g2.drawLine((int)lv.x0, (int)lv.y0, (int)(lv.x0+lv.x), (int)(lv.y0+lv.y));
	}
	
	LocVektor locArrow;
	double arrowLength = 15;
	double arrowHalfAngle = 0.3;
	public void paintLocVektor(LocVektor lv) {
		paintLocLine(lv);
		lv.fix();
		if (lv.r < 2)
			return;
		
		double dxa = -lv.x/arrowLength, dya = -lv.y/arrowLength;
		double r = Math.sqrt(dxa*dxa + dya*dya);
		double an = Vektor.angle(dxa, dya);
		if (r<8) {
			r = 8;
			dxa = r * Math.cos(an);
			dya = r * Math.sin(an);
		}
		an += arrowHalfAngle;
		locArrow = new LocVektor(lv.x1, lv.y1, r*Math.cos(an), r*Math.sin(an));
		paintLocLine(locArrow);
		an -= 2*arrowHalfAngle;
		locArrow.x = r*Math.cos(an);
		locArrow.y = r*Math.sin(an);
		paintLocLine(locArrow);
		
		/*
		locArrow = lv;
		locArrow.fix();
		//g2.setColor(Color.BLACK);
		//System.out.println(locArrow);
		locArrow.x0 += lv.x;
		locArrow.y0 += lv.y;
		locArrow.fix();
		//System.out.println(locArrow);
		//lv.rotate(Math.PI/2);
		//paintLocLine(locArrow);
		//g2.setColor(newCol);

		
		locArrow.scaleToR(-arrowLength);
		//System.out.println(locArrow.r);
		//System.out.println(lv);
		locArrow.fix();
		//System.out.println(lv);
		locArrow.rotate(arrowHalfAngle);
		//System.out.println(locArrow.r);
		paintLocLine(locArrow);
		locArrow.rotate(-2 * arrowHalfAngle);
		//g2.setColor(Color.ORANGE);
		paintLocLine(locArrow);
		locArrow = null;
		*/
	}
	
	int animsteps = 20;
	int milisanim = 2000;
	boolean changedAnim = false;
	
	Timer timer = new Timer(milisanim/animsteps, new MyActionListener());
	
	boolean isanimating = false;
	LocVektor translatedVektor;
	
	private class MyActionListener implements ActionListener {
		int step = 0;
		boolean started = true, rotating = true;
		double dx=0, dy=0;
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			step++;
			
			if (changedAnim) {
				if (milisanim < 500) {
					animsteps = 5;
				}
				else {
					animsteps = 20;
				}
				timer.setDelay(milisanim/animsteps);
			}
			
			if (rotating) {
				
				translatedVektor.rotate(Math.PI/animsteps);
				repaint();
				if (step >= animsteps) {
					System.out.println(milisanim);
					rotating = false;
					step = 0;
					message = "Zbroji";
					return;
				}
				return;
			}
			
			if (started) {
				dx = v2.x0 - vdr1.x0; dy = v2.y0 - vdr1.y0;
				started = false;
			}
			translatedVektor.x0 += 1.0/animsteps * dx;
			translatedVektor.y0 += 1.0/animsteps * dy;
			translatedVektor.fix();
			if (step == animsteps) {
				step = 0;
				isanimating = false;
				timer.stop();
				started = true;
				rotating = true;
				drawRes = true;
			}
			repaint();
		}
		
	}
	
	String message = "";
	
	@Override
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		Color c = new Color(150,255,255);
		//System.out.println(c);
		g.setColor(c);
		g.fillRect(0,0,1500,1000);
		
		g.setColor(new Color(0,100,100));
		g.setFont(new Font("Arial", 0, 30));
		g.drawString("Nacrtaj dva vektora koja zelis oduzeti", 50,50);
		g.drawString("Rezultat je oznacen crvenom bojom", 50,85);
		g.drawString(message, 50, 120);
		
		
		
		
		g2 = (Graphics2D) g;
		
		g2.setStroke(new BasicStroke(3));
		/*
		for (int i = 0; i < numVekt; i++) {
			//System.out.println(i);
			g2.setPaint(boje[i]);
			g2.drawLine((int) (startPoz[i].x), (int) (startPoz[i].y), (int) (vektori[i].x + startPoz[i].x),
					(int) (vektori[i].y + startPoz[i].y));
			
			g2.fillOval((int) (startPoz[i].x - pointR),
					(int) (startPoz[i].y - pointR),
					pointR * 2, pointR * 2);
			g2.fillOval((int) (startPoz[i].x + vektori[i].x) - pointR,
					(int) (startPoz[i].y + vektori[i].y) - pointR,
					pointR * 2, pointR * 2);
			
			
			
			
			
		}
		*/
		/*
		if (numVekt <2) return;
		g2.setColor(Color.RED);
		g2.drawLine((int) (startPoz[0].x), (int) (startPoz[0].y) ,
				(int) (vektori[0].x+vektori[1].x+startPoz[0].x) ,
				(int) (vektori[0].y+vektori[1].y+startPoz[0].y));
		
		g2.setStroke(new BasicStroke(1));
		g2.drawLine((int) (startPoz[0].x+vektori[0].x), (int) (startPoz[0].y+vektori[0].y) ,
				(int) (vektori[0].x+vektori[1].x+startPoz[0].x) ,
				(int) (vektori[0].y+vektori[1].y+startPoz[0].y));
		g2.drawLine((int) (startPoz[0].x+vektori[1].x), (int) (startPoz[0].y+vektori[1].y) ,
				(int) (vektori[0].x+vektori[1].x+startPoz[0].x) ,
				(int) (vektori[0].y+vektori[1].y+startPoz[0].y));
		g2.setColor(newCol);
		*/
		
		if (guideActive) {
			Stroke s = g2.getStroke();
			g2.setStroke(new BasicStroke(1));
			paintLocLine(guide);
			g2.setStroke(s);
		}
		
		if (vdr != null) {
			paintLocVektor(vdr);
		}
		if (vdr1 != null) {
			paintLocVektor(vdr1);
		}
		
		if (v1 != null) {
			paintLocVektor(v1);
		}
		if (isanimating && translatedVektor != null) {

			g2.setColor(newCol);
			//paintLocVektor(translatedVektor);
		}
		if (v2 != null) {
			paintLocVektor(v2);
			if (v2r != null) {
				g2.setColor(Color.orange);
				paintLocVektor(v2r);
				g2.setColor(newCol);
			}
			
		}
		if (drawRes) {
			g2.setStroke(new BasicStroke(5));
			g2.setColor(Color.red);
			paintLocVektor(res);
		}
		
		
		
		
		
	}
	
	boolean adding = false;
	boolean addFirst = false;
	
	
	void addVektor() {
		if (numVekt >= numMax) {
			return;
		}
		adding = true;
		addFirst = true;
	}
	
	void clear() {
		v1 = v2 = v2r = vdr = vdr1 = res = translatedVektor = null;
		numVekt = 0;
		isanimating = guideActive = drawRes = false;
		message = "";
		numsub = 0;


		repaint();

	}
	
	boolean handleClickDone = false;
	
	int xv20, yv20;
	class Clicker implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
			int held = -1;
			
			if (adding) {
				if (addFirst) {
					message = "Nacrtaj prvi vektor 'a'";
					startPoz[numVekt] = new Vektor(e.getX(), e.getY());
					addFirst = false;
					if (v1 == null) {
						v1 = new LocVektor(e.getX(), e.getY(), 0, 0);
						//System.out.println("init v1");
						vdr = v1;
					}
					else if (v2 == null) {
						message = "Nacrtaj drugi vektor 'b'";
						v2 = new LocVektor(e.getX(), e.getY(), 0, 0);
						xv20 = e.getX();
						yv20 = e.getY();
						v2.fix();
						//System.out.println("init v2");
						vdr = v2;
						
					}
				}
				else {
					vektori[numVekt] = new Vektor(e.getX()-startPoz[numVekt].x, e.getY()-startPoz[numVekt].y);
					adding = false;
					numVekt++;
					
					if (numsub == 0) {
						v1.x = e.getX()-v1.x0;
						v1.y = e.getY()-v1.y0;
						v1.fix();
						numsub = 1;
						//System.out.println("update v1");
						//vdr = v1;

					}
					else if (numsub == 1) {
						vdr = v2;
						translatedVektor = v2;
						//vdr1 = v2;
						//v2.x = e.getX()-v2.x0;
						//v2.y = e.getY()-v2.y0;
						v2.fix();
						
						System.out.println(v2);
						
						v2r = v2;
						v2r.scale(-1);
						v2r.x0 = v1.x1;
						v2r.y0 = v1.y1;
						v2r.fix();
						numsub = 2;
						//System.out.println("update v2");
						res = new LocVektor(v1.x0,v1.y0,v2.x1,v2.y1, true);
						translatedVektor = vdr1;
						isanimating = true;
						message = "Pretvori b u -b";
						//System.out.println(v2);
						//System.out.println(vdr);
						
						guide = new LocVektor(translatedVektor.x0, translatedVektor.y0, translatedVektor.x, translatedVektor.y);
						guide.scaleToR(2000);
						double xg0 = guide.x1, yg0 = guide.y1;
						guide.scale(-1);
						double xg1 = guide.x1, yg1 = guide.y1;
						guide = new LocVektor(xg0, yg0, xg1, yg1, true);
						guideActive = true;
						handleClickDone = true;
						timer.start();

					}
					
				}
				repaint();
				
				return;
			}
			
			
			for (int i = 0; i < numVekt; i++) {
				double xg = vektori[i].x + startPoz[i].x;
				double yg = vektori[i].y + startPoz[i].y;

				if (Math.abs(xg - e.getX()) + Math.abs(yg - e.getY()) < pointR+2) {
					held = i;
					break;
				}
			}
			if (held >= 0) {
				heldVektor = held;
			} else
				heldVektor = -1;
			
			int heldP = -1;
			for (int i = 0; i < numVekt; i++) {
				double xg = startPoz[i].x;
				double yg = startPoz[i].y;

				if (Math.abs(xg - e.getX()) + Math.abs(yg - e.getY()) < pointR) {
					heldP = i;
					break;
				}
			}
			if (heldP >= 0) {
				heldPos = heldP;
			} else
				heldPos = -1;
			
			//System.out.println(e.getX() + id);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	class Dragger implements MouseMotionListener {

		@Override
		public void mouseDragged(MouseEvent e) {
			// TODO Auto-generated method stub
			if (heldVektor >= 0) {
				vektori[heldVektor].x = e.getX() - startPoz[heldVektor].x;
				vektori[heldVektor].y = e.getY() - startPoz[heldVektor].y;
				t.repaint();

			}
			if (heldPos >= 0) {
				startPoz[heldPos].x = e.getX();
				startPoz[heldPos].y = e.getY();
				t.repaint();

			}
			

		}
		
		boolean addingL = adding, addFirstL = addFirst;
		
		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			if (adding) {
				
				if (addFirst) {
					/*
					startPoz[numVekt] = new Vektor(e.getX(), e.getY());
					if (v1 == null) {
						v1.x1 = e.getX();
						v1.y1 = e.getY();
						v1.fix();
						//System.out.println("init v1");
						vdr = v1;
					}
					else if (v2 == null) {
						v2.x1 = e.getX();
						v2.y1 = e.getY();
						v2.fix();
						//System.out.println("init v2");
						vdr = v2;
						
					}
					*/
				}
				else {
					
					
					if (numsub == 0) {
						v1.x = e.getX()-v1.x0;
						v1.y = e.getY()-v1.y0;
						v1.fix();
						//System.out.println("update v1");
						vdr = v1;
						vdr1 = vdr;
					}
					else if (numsub == 1) {
						v2.x = e.getX()-v2.x0;
						v2.y = e.getY()-v2.y0;
						v2.fix();
						vdr = v2;
						//System.out.println(vdr);
						//System.out.println(v2);
						/*
						vdr1 = vdr;
						vdr1.x = xv20;
						vdr1.y = yv20;
						*/
						vdr1 = new LocVektor(xv20, yv20, e.getX(), e.getY(), true);
						vdr1.fix();
						/*
						v2r = v2;
						v2r.scale(-1);
						v2r.x0 = v1.x1;
						v2r.y0 = v1.y1;
						v2r.fix();
						//System.out.println("update v2");
						res = new LocVektor(v1.x0,v1.y0,v2.x1,v2.y1, true);
						*/
					}
					
				}
				repaint();
				
				return;
			}
		}

	}

	

	
}
