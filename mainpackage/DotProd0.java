package mainpackage;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class DotProd0 extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1511148134525926253L;

	CoordSystemVectors csv = new CoordSystemVectors();
	Calculator calc = new Calculator();
	JButton checkButton = new JButton("Provjeri");
	
	
	double dot = csv.giveDot();
	JLabel message = new JLabel(" ");
	public void init() {
		csv.draggy = true;
		csv.setup();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		//setLayout(new GridLayout(1,0));
		///setLayout(new CardLayout());
		add(csv);
		JPanel bottom = new JPanel();
		bottom.add(new JLabel("Namjestite da skalarni produkt bude 0"));
		bottom.add(calc);
		checkButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dot = csv.giveDot();
				if (Math.abs(dot/(csv.vectors[0].mag() * csv.vectors[1].mag())) <= 0.1) {
					csv.generateRandomVectors(2);
					message.setText("Tocno!");
					calc.display.setText("");
					csv.repaint();
					dot = csv.giveDot();
					//System.out.println(dot);
				}
				else {
					message.setText("Krivo");
				}
				System.out.println(dot);
				
				
			}
		});
		bottom.add(checkButton);
		bottom.add(message);
		add(bottom);
		revalidate();
		repaint();
	}
	
	
	public DotProd0() {
		super();
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				init();
			}
		});
		
	}
	
	
	
	
}
