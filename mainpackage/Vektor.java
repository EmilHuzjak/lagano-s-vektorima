package mainpackage;

public class Vektor {

	
	public double x, y;
	public double r;
	public double angle;
	
	public Vektor(double ri, double a, double polarInputYes) {
		r = ri;
		angle = a;
		x = r * Math.cos(a);
		y = r * Math.sin(a);

	}
	
	public void rotate(double rAngle) {
		//System.out.println("rotating: " + rAngle);
		angle += rAngle;
		x = r * Math.cos(angle);
		y = r * Math.sin(angle);
	}
	
	public void fix() {
		//System.out.println(y/x);
		angle = angle(x,y);
		//System.out.println(angle);

		r = Math.sqrt(x*x + y*y);
	}
	
	public void scaleToR(double newR) {
		x *= newR / r;
		y *= newR / r;
		r = newR;
	}
	
	public Vektor(double xi, double yi) {
		x = xi; y = yi;
		r = Math.sqrt(x*x + y*y);
		angle = angle(x,y);
	}
	
	public Vektor() {
		
	}
	
	public static double angle(double x, double y) {
		double a = 0;
		if (x==0) {
			if (y==0) {
				a = -1;
			}
			else a = (y > 0 ? Math.PI/2 : 3*Math.PI/2);
		}
		else {
			a = Math.atan(y/x);
			if (x<0) a += Math.PI;
		}
		
		return a;
	}
	
	public String toString() {
		return "( " + x + ", " + y + " )";
	}
	
	
}
