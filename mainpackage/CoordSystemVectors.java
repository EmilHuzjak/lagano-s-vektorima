package mainpackage;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import vectUtilities.Mapper;
import vectUtilities.Utility;

public class CoordSystemVectors extends JPanel {

	
	private static final long serialVersionUID = 6343472925141133827L;

	public CoordSystemVectors() {
		super();
		setBackground(new Color(200, 150, 200));
		setPreferredSize(new Dimension(500,500));
		initialize();
	}
	
	public void initialize() {
		w = getWidth();
		h = getHeight();
		numVectors = vectors.length;
	}
	
	public void setup() {
		if (draggy) {
			addMouseListener(new Clicker());
			addMouseMotionListener(new Dragger());
		}
	}
	
	Mapper m;
	
	boolean firstRender = true;
	
	public void adjustAspect() {
		double xunit1 = w/xCoordSpan;
		double yunit1 = h/yCoordSpan;
		if (xunit1 > yunit1) {
			System.out.println("adjust y");
			yCoordSpan = h/xunit1;
			yunit = xunit1;
			xunit = xunit1;
		}
		else if (yunit1 > xunit1) {
			System.out.println("adjust x");

			xCoordSpan = w/yunit1;
			yunit = yunit1;
			xunit = yunit1;
		}
		x0 = -xCoordSpan/2;
		y0 = -yCoordSpan/2;
		mapper = new Mapper(x0, y0+yCoordSpan, xunit, yunit);
		
	}
	
	public boolean draggy = false;
	
	boolean customColors = false;
	public Color[] colors;
	
	public LocVektor[] vectors = {new LocVektor(0,0,1,1), new LocVektor(0,0,-2,1)};
	public int numVectors = 0;
	double xCoordSpan = 20;
	double x0 = -8;
	double yCoordSpan = 20;
	double y0 = -8;
	double scaling;
	
	Mapper mapper;
	
	double xunit, yunit;

	int w, h;
	
	public void generateRandomVectors(int num) {
		numVectors = num;
		vectors = new LocVektor[num];
		for (int i = 0; i<num; i++) {
			int xr, yr;
			int attempt = 0;
			do {
				xr = (int)((Math.random()-0.5)*xCoordSpan); yr = (int)((Math.random()-0.5)*yCoordSpan);
				attempt++;
			} while(xr == 0 || yr == 0 || attempt > 100);
			//System.out.println(xr);
			vectors[i] = new LocVektor(xr, yr);
			vectors[i].fix();
		}
	}
	
	int xs, ys;
	public void map(double x, double y) {
		xs = (int)((x-x0) * xunit);
		//System.out.println(y + " " + y0 + " " + yunit + " " + h);
		ys = h-(int)((y-y0) * yunit);
		//System.out.println(ys);
	}
	
	Graphics2D g2D;
	
	public void drawLocLine(LocVektor lv1) {
		LocVektor lv = lv1.copy();
		map(lv.x0, lv.y0);
		//System.out.println(lv.y0);
		//System.out.println(ys);
		double x0v = xs, y0v = ys;
		map(lv.x0+lv.x, lv.y0+lv.y);
		drawLine(x0v, y0v, xs, ys);
	}
	
	double arrowLengthFactor = 1.0/10;
	double arrowHalfAngle = Math.PI/6;
	LocVektor locArrow;
	private void drawLocVector(LocVektor lv1) {
		// TODO Auto-generated method stub
		LocVektor lv = lv1.copy();
		drawLocLine(lv);
		lv.fix();
		
		map(lv.r, 0);
		
		if (xs/yunit < 2)
			return;
		
		locArrow = lv;
		locArrow.translate(lv);
		locArrow.scale(-arrowLengthFactor);
		locArrow.rotate(-arrowHalfAngle);
		drawLocLine(locArrow);
		locArrow.rotate(2*arrowHalfAngle);
		drawLocLine(locArrow);
	}
	
	public void drawLine(double xl0, double yl0, double xl1, double yl1) {
		g2D.drawLine((int)xl0, (int)yl0, (int)xl1, (int)yl1);
	}
	
	public double giveDot() {
		if (vectors.length > 1) {
			double dot = vectors[0].dot(vectors[1]);
			//System.out.println("dot: " + dot);
			return dot;
		}
		return 0;
	}
	
	public double giveAngle() {
		double dot = giveDot();
		return Math.acos(dot / (vectors[0].mag() * vectors[1].mag())) * 180 / Math.PI;
	}
	
	public int nearestInt(double d) {
		double absd = Math.abs(d);
		double space = absd - (int)absd;
		int res = (int)d;
		if (space > 0.5) {
			res++;
		}
		return (int) Math.round(d);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int wold = w, hold = h;
		w = getWidth();
		h = getHeight();
		if (wold != w || hold != h || firstRender) {
			adjustAspect();
			firstRender = false;
		}
		g2D = (Graphics2D)g;
		for (double i = x0+0.1; i<x0 + xCoordSpan; i++) {
			//System.out.println(i);
			int ii = (int)Math.round(i);
			map(ii,0);
			//System.out.println(xs);
			drawLine(xs,0,xs,h);
			g.drawString((int)ii+"", xs, ys-3);
		}
		for (double i = y0+0.1; i<y0 + yCoordSpan; i++) {
			int ii = (int)Math.round(i);
			map(0,ii);
			
			drawLine(0,ys,w,ys);
			g.drawString((int)ii+"", xs+1, ys-3);
		}
		g2D.setStroke(new BasicStroke(3));
		map(0,0);
		drawLine(0,ys,w,ys);
		drawLine(xs,0,xs,h);
		g2D.setColor(Color.blue);
		for (int i = 0; i<numVectors; i++) {
			LocVektor l = vectors[i];
			//System.out.println(l);
			//drawLocVector(l);
			Utility.drawLocVector(mapper.mapToScreen(l), g2D);
		}
		
	}

	double xco, yco;
	
	public void mapreverse(double x, double y) {
		xco = (x/xunit) + x0;
		yco = (h-y)/yunit + y0;
	}
	
	int held = -1;
	
	class Dragger implements MouseMotionListener {

		
		@Override
		public void mouseDragged(MouseEvent e) {
			// TODO Auto-generated method stub
			if (held >= 0) {
				int x = e.getX(), y = e.getY();
				mapreverse(x,y);
				vectors[held].x1 = xco; vectors[held].y1 = yco;
				//System.out.println(xco);
				vectors[held].fixLength();
				repaint();
			}
			
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	class Clicker implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			int x = e.getX(), y = e.getY();
			for (int i = 0 ; i<numVectors; i++) {
				vectors[i].fix();
				double xv = vectors[i].x1, yv = vectors[i].y1;
				map(xv, yv);
				//System.out.println(x + " " + xs);
				if (Math.abs(x-xs) + Math.abs(y-ys)<10) {
					held = i;
				}
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			held = -1;
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	
	
}
