package mainpackage;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;

public class VektorPanel extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public Vektor[] vektori;
	public Vektor[] startPoz;
	
	public Vektor[] vektori1;
	public Vektor[] startPoz1;
	
	
	int id;
	Color[] boje;
	int numVekt = 0;
	int numMax = 10;

	int heldVektor = -1;
	int heldPos = -1;

	VektorPanel t = this;

	public VektorPanel() {
		vektori = new Vektor[numMax];
		startPoz = new Vektor[numMax];
		boje = new Color[numMax];
		for (int i = 0; i < numMax; i++) {
			boje[i] = Color.blue;
		}
		boje[0] = Color.green;
		
		Dragger d = new Dragger();
		Clicker c = new Clicker();
		addMouseMotionListener(d);
		addMouseListener(c);
		//getRootPane().setBackground(Color.green);
	}

	public void addVektor(double x0, double y0, double x1, double y1) {
		startPoz[numVekt] = new Vektor(x0, y0);
		vektori[numVekt] = new Vektor(x1 - x0, y1 - y0);
		numVekt++;

		

		
		/*
		for (int i = 0; i<numVekt; i++) {
			System.out.print(vektori[i] + ",   ");
		}
		*/
	}

	int pointR = 5;

	Graphics2D g2;
	boolean init = true;

	
	
	@Override
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		Color c = new Color(150,255,255);
		//System.out.println(c);
		g.setColor(c);
		g.fillRect(0,0,1500,1000);
		
		g.setColor(new Color(0,100,100));
		g.setFont(new Font("Arial", 0, 30));
		g.drawString("Nacrtaj dva vektora koja zelis zbrojiti", 50,50);
		
		
		
		
		
		g2 = (Graphics2D) g;
		
		g2.setStroke(new BasicStroke(3));
		
		for (int i = 0; i < numVekt; i++) {
			//System.out.println(i);
			g2.setPaint(boje[i]);
			g2.drawLine((int) (startPoz[i].x), (int) (startPoz[i].y), (int) (vektori[i].x + startPoz[i].x),
					(int) (vektori[i].y + startPoz[i].y));
			
			g2.fillOval((int) (startPoz[i].x - pointR),
					(int) (startPoz[i].y - pointR),
					pointR * 2, pointR * 2);
			g2.fillOval((int) (startPoz[i].x + vektori[i].x) - pointR,
					(int) (startPoz[i].y + vektori[i].y) - pointR,
					pointR * 2, pointR * 2);
			
			
			
			
			
		}
		if (numVekt <2) return;
		g2.setColor(Color.RED);
		g2.drawLine((int) (startPoz[0].x), (int) (startPoz[0].y) ,
				(int) (vektori[0].x+vektori[1].x+startPoz[0].x) ,
				(int) (vektori[0].y+vektori[1].y+startPoz[0].y));
		
		g2.setStroke(new BasicStroke(1));
		g2.drawLine((int) (startPoz[0].x+vektori[0].x), (int) (startPoz[0].y+vektori[0].y) ,
				(int) (vektori[0].x+vektori[1].x+startPoz[0].x) ,
				(int) (vektori[0].y+vektori[1].y+startPoz[0].y));
		g2.drawLine((int) (startPoz[0].x+vektori[1].x), (int) (startPoz[0].y+vektori[1].y) ,
				(int) (vektori[0].x+vektori[1].x+startPoz[0].x) ,
				(int) (vektori[0].y+vektori[1].y+startPoz[0].y));
		

	}
	
	boolean adding = false;
	boolean addFirst = false;
	
	
	void addVektor() {
		if (numVekt >= numMax) {
			return;
		}
		adding = true;
		addFirst = true;
	}
	
	void clear() {
		numVekt = 0;
		repaint();
	}
	

	class Clicker implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {

		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
			int held = -1;
			
			if (adding) {
				if (addFirst) {
					startPoz[numVekt] = new Vektor(e.getX(), e.getY());
					addFirst = false;
				}
				else {
					vektori[numVekt] = new Vektor(e.getX()-startPoz[numVekt].x, e.getY()-startPoz[numVekt].y);
					adding = false;
					numVekt++;
				}
				repaint();
				
				return;
			}
			
			
			for (int i = 0; i < numVekt; i++) {
				double xg = vektori[i].x + startPoz[i].x;
				double yg = vektori[i].y + startPoz[i].y;

				if (Math.abs(xg - e.getX()) + Math.abs(yg - e.getY()) < pointR+2) {
					held = i;
					break;
				}
			}
			if (held >= 0) {
				heldVektor = held;
			} else
				heldVektor = -1;
			
			int heldP = -1;
			for (int i = 0; i < numVekt; i++) {
				double xg = startPoz[i].x;
				double yg = startPoz[i].y;

				if (Math.abs(xg - e.getX()) + Math.abs(yg - e.getY()) < pointR) {
					heldP = i;
					break;
				}
			}
			if (heldP >= 0) {
				heldPos = heldP;
			} else
				heldPos = -1;
			
			//System.out.println(e.getX() + id);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	class Dragger implements MouseMotionListener {

		@Override
		public void mouseDragged(MouseEvent e) {
			// TODO Auto-generated method stub
			if (heldVektor >= 0) {
				vektori[heldVektor].x = e.getX() - startPoz[heldVektor].x;
				vektori[heldVektor].y = e.getY() - startPoz[heldVektor].y;
				t.repaint();

			}
			if (heldPos >= 0) {
				startPoz[heldPos].x = e.getX();
				startPoz[heldPos].y = e.getY();
				t.repaint();

			}
			

		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

}
