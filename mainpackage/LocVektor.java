package mainpackage;

public class LocVektor {

	
	public double x0, y0;
	public double x1, y1;
	
	public double x, y;
	public double r;
	public double angle;
	
	public LocVektor(double ri, double a, double polarInputYes) {
		r = ri;
		angle = a;
		x = r * Math.cos(a);
		y = r * Math.sin(a);

	}
	
	public LocVektor copy() {
		fix();
		return new LocVektor(x0, y0, x, y);
	}
	
	public void matchComponents(LocVektor lvcomp) {
		x = lvcomp.x;
		y = lvcomp.y;
		fix();
	}
	
	public void add(LocVektor lv) {
		x += lv.x; y += lv.y;
		fix();
	}
	
	public void rotate(double rAngle) {
		//System.out.println("rotating: " + rAngle);
		angle += rAngle;
		x = r * Math.cos(angle);
		y = r * Math.sin(angle);
		x1 = x0 + x;
		y1 = y0 + y;
	}
	
	public double dot(LocVektor vd) {
		return x*vd.x + y*vd.y;
	}
	
	public static LocVektor add(LocVektor v1, LocVektor v2) {
		return new LocVektor(v1.x0, v1.y0, v1.x+v2.x, v1.y+v2.y);
	}
	
	public LocVektor projOn(LocVektor vpr) {
		
		double dot = dot(vpr);
		LocVektor vpr2 = new LocVektor(vpr.x0, vpr.y0, vpr.x, vpr.y);
		vpr2.scale(dot / (Math.pow(vpr2.mag(),2)));
		
		return vpr2;
	}
	
	public LocVektor xComp() {
		return new LocVektor(x0, y0, x, 0);
	}
	
	public LocVektor yComp() {
		return new LocVektor(x0, y0, 0, y);
	}
	
	public static LocVektor unitX() {
		return new LocVektor(0, 0, 1, 0);
	}
	
	public static LocVektor unitY() {
		return new LocVektor(0, 0, 0, 1);
	}
	
	public double mag() {
		return Math.sqrt(x*x+y*y);
	}
	
	public LocVektor unit() {
		if (mag() == 0) {
			return new LocVektor(0,0,0,0);
		}
		LocVektor u = this.copy();
		u.scaleToR(1);
		return u;
	}
	
	public void fix() {
		//System.out.println(y/x);
		x1 = x0 + x;
		y1 = y0 + y;
		angle = angle(x,y);
		//System.out.println(angle);

		r = Math.sqrt(x*x + y*y);
	}
	
	public void fixLength() {
		x = x1-x0; y=y1-y0;
		angle = angle(x,y);
		r = Math.sqrt(x*x + y*y);
	}
	
	public void scale(double f) {
		x *= f;
		y *= f;
		r *= f;
		fix();
	}
	
	public void scaleToR(double newR) {
		x *= newR / r;
		y *= newR / r;
		r = Math.abs(newR);
		fix();
	}
	
	public void translate(LocVektor vTran) {
		x0 += vTran.x;
		y0 += vTran.y;
		x1 += vTran.x;
		y1 += vTran.y;
	}
	
	public LocVektor(double x0, double y0, double x, double y) {
		this.x0 = x0;
		this.y0 = y0;
		this.x = x; this.y = y;
		this.x1 = x0 + x;
		this.y1 = y0 + y;
		fix();
	}
	
	public LocVektor(double x0, double y0, double x1, double y1, boolean endpoints) {
		this.x0 = x0;
		this.y0 = y0;
		this.x = x1-x0; this.y = y1-y0;
		this.x1 = x1;
		this.y1 = y1;
		fix();
	}
	
	public LocVektor(double xi, double yi) {
		x = xi; y = yi;
		r = Math.sqrt(x*x + y*y);
		angle = angle(x,y);
	}
	
	public LocVektor() {
		
	}
	
	public void setStartAt(double xstart, double ystart) {
		x0 = xstart;
		y0 = ystart;
		fix();
	}
	
	public void matchStart(LocVektor msv) {
		setStartAt(msv.x0, msv.y0);
	}
	
	public void startEnd(LocVektor lv) {
		setStartAt(lv.x1, lv.y1);
	}
	
	public double midX() {
		return x0 + x/2;
	}
	
	public double midY() {
		return y0 + y/2;
	}
	
	public static double angle(double x, double y) {
		double a = 0;
		if (x==0) {
			if (y==0) {
				a = -1;
			}
			else a = (y > 0 ? Math.PI/2 : 3*Math.PI/2);
		}
		else {
			a = Math.atan(y/x);
			if (x<0) a += Math.PI;
			else if (y<0) a += Math.PI * 2;
		}
		
		return a;
	}
	
	public static int whichFirstCCW(double a, double b) {
		if (b-a > 0 && b-a<Math.PI) {
			return 1;
		}
		return 2;
	}
	
	public static double closestAngleFromTo(double a, double b) {
		double delta = b-a;
		if (Math.abs(delta) > Math.PI) {
			return Math.PI*2 - Math.abs(delta);
		}
		
		return delta;
	}
	
	public double angleFrom(LocVektor lv) {
		return angle(x,y)-angle(lv.x,lv.y);
	}
	
	public String toString() {
		return "( " + x0 + "+" + x + ", " + y0 + "+" + y + " )";
	}
	
	
	
}
