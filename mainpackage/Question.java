package mainpackage;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class Question extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2917139965458928214L;

	int w=300, h=300;
	
	String q = "Pitanje";
	String[] options = {"Odgovor 1", "Odgovor 2", "Odgovor 3"};
	int correct = 0;
	int numopt = 3;
	
	int clicked = -1;
	boolean checked = false;
	
	String expl = "";
	
	public Question() {
		super();
		//setBackground(Color.white);
		setPreferredSize(new Dimension(w, h));
		repaint();
	}
	
	int buttonThickness = 30;
	int buttonTop;
	public Question(String ques, String[] opts, int correctone) {
		this();
		q = ques;
		options = opts;
		numopt = opts.length;
		buttonTop = qy + sizeFont*(1+numopt) + optSpacing*(numopt+1);
		JButton check = new JButton("Provjeri");
		check.setBounds(150, buttonTop,100,buttonThickness);

		setPreferredSize(new Dimension(300, buttonTop+buttonThickness+optSpacing));
		correct = correctone;
		addMouseListener(new Clicker());
		check.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (clicked >= 0) {
					checked = true;
					repaint();
				}
			}
			
		});
		setLayout(null);
		add(check);

	}
	
	int sizeFont = 20;
	int qy = 40;
	int optSpacing = sizeFont/2;
	int r = sizeFont/2;
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setFont(new Font("Arial", 0, sizeFont));
		g.drawString(q, 50, qy+sizeFont);
		for (int i = 0; i<numopt; i++) {
			g.setColor(Color.black);
			int optTop = qy+sizeFont + optSpacing*(i+1)+sizeFont*i;
			g.drawString(options[i], 90, optTop+sizeFont);
			if (clicked == i) {
				g.setColor(Color.blue);
				g.fillOval(50, optTop+(sizeFont/2-r), 2*r, 2*r);
			}
			((Graphics2D) g).setStroke(new BasicStroke(2));
			g.setColor(new Color(200,200,250));
			g.drawOval(48, optTop+(sizeFont/2-r)-2, 2*r+4, 2*r+4);
			g.setColor(new Color(150,150,200));
			g.drawOval(49, optTop+(sizeFont/2-r)-1, 2*r+2, 2*r+2);
			g.setColor(Color.blue);
			g.drawOval(50, optTop+(sizeFont/2-r), 2*r, 2*r);
		}
		if (checked && clicked==correct) {
			g.setColor(new Color(50,150,100));
			g.drawString("Točno", 300,buttonTop+sizeFont);
		}
		else if (checked && clicked != correct) {
			g.setColor(Color.red);
			g.drawString("Nije točno", 300,buttonTop+sizeFont);
		}

		
		
	}
	
	
	class Clicker implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

			// TODO Auto-generated method stub
			int x = e.getX(), y = e.getY();
			for (int i = 0; i<numopt; i++) {
				int xp= 50+r, yp = qy+sizeFont+(sizeFont/2-r) + optSpacing*(i+1)+sizeFont*i+r;
				if (Math.sqrt((xp-x)*(xp-x)+(yp-y)*(yp-y)) < r) {
					//System.out.println(i);
					clicked = i;
					checked=false;
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							repaint();
						}
						
					});
					return;
				}
			}
		
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	
	
}
