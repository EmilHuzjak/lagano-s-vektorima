package mainpackage;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;



public class Calculator extends JPanel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JPanel digitsPanel;
	JButton[] digitButtons = new JButton[10];
	public JTextArea display = new JTextArea();
	
	public Calculator() {
		super();
		setPreferredSize(new Dimension(400,150));
		JPanel inner = new JPanel();
		inner.setPreferredSize(new Dimension(400,150));
		//inner.setPreferredSize(new Dimension(150,200));
		inner.setLayout(new BoxLayout(inner, BoxLayout.Y_AXIS));
		digitsPanel = new JPanel();
		digitsPanel.setLayout(new GridLayout(4,3));
		for (int i = 0; i<10; i++) {
			final int value;
//			if (i<9) {
//				value = Integer.valueOf(i);
//			}
//			else {
//				value = Integer.valueOf(0);
//			}
			value = Integer.valueOf((i+1)%10);
			digitButtons[i] = new JButton(value+"");
			//digitButtons[i].setText(""+i);
			digitsPanel.add(digitButtons[i]);
			//final int ii = Integer.valueOf(i);
			digitButtons[i].addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					display.append(value+"");
				}
				
				// 1-(-5)+(-8*10)+5-4-6+7*18/3 - 351*(-1) + 200/2 + 10*20
			});

		}
		String[] parens = {"(", ")"};
		for (int i = 0; i<2; i++) {
			String ps = new String(parens[i]);
			
			JButton parenB = new JButton(ps);
			parenB.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					display.append(ps);
				}
				
				// 12*60 - 9*18/3 + 6/2 + 14/2*5 - 10
			});
			digitsPanel.add(parenB);
		}
		
		
		display.append("0");
		inner.add(display);
		inner.add(digitsPanel);
		
		JPanel others = new JPanel();
		others.setLayout(new FlowLayout());
		JButton clearAllB = new JButton("CE");
		clearAllB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				display.setText("");
			}
			
		});
		others.add(clearAllB);
		
		JButton clearB = new JButton("<-");
		clearB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (display.getText().length() > 0)
					display.setText(display.getText().substring(0, display.getText().length()-1));
			}
			
		});
		others.add(clearB);
		
		
		
		String[] operations = {"+", "-", "*", "/"};
		
		for (int i = 0; i<4; i++) {
			final int ii = Integer.valueOf(i);
			JButton opB = new JButton(operations[i]);
			opB.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					display.append(operations[ii]);
				}
			});
			others.add(opB);
		}
		
		JButton equalsB = new JButton("=");
		equalsB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				try {
					Node n = giveNode(display.getText().trim());
					n.evaluate();
					display.setText(""+n.value);
					System.out.println("\n----"+n.value);
				} catch(Exception ex) {
					//ex.printStackTrace();
				}
			}
			
		});
		others.add(equalsB);
		inner.add(others);
		add(inner);
		revalidate();
		repaint();
	}
	
	String instructions = "";
	
	public static int parDepth(String expr, int index) {
		int depth = 0;
		if (index >=expr.length())
			return -1;
		
		for (int i = 0; i<index; i++) {
			if (expr.charAt(i) == '(') {
				depth++;
			}
			if (expr.charAt(i) == ')') {
				depth--;
			}
		}
		return depth;
	}

	
	public static boolean enveloped(String s) {
		int len = s.length();
		if (s.charAt(0)!='(' || s.charAt(len-1)!=')') {
			return false;
		}
		for (int i = 1; i<len-1; i++) {
			if (parDepth(s, i) == 0)
				return false;
		}
		return true;
	}

	public static boolean containsOp(String s) {
		char c = s.charAt(0);
		char cp;
		int len = s.length();
		for (int i = 1; i<len; i++) {
			cp = c;
			c = s.charAt(i);
			//System.out.print(cp); System.out.print(" "); System.out.println(c);
			if (c=='*' || c=='/' 
					|| c=='+' && cp!='(' || c=='-' && cp!='(') {
				return true;
			}
		}
		return false;
	}

	
	public Node giveNode(String expr) {
		
		
		int len = expr.length();
		if (len==0) {
			return new Node(false, "$");
		}
		while(enveloped(expr)) {
			//System.out.println(expr + enveloped(expr));
			expr = expr.substring(1, len-1).trim();
			len = expr.length();
			
		}
		//System.out.println(expr);
		// a*b+c
		if (!containsOp(expr)) {
			
			return new Node(false, expr);
		}
		
		if (len==0) {
			return new Node(false, "$");
		}
		
		//System.out.println(expr);
		
		//System.out.println(len);
		//System.out.println(expr);
		char c = expr.charAt(len-1);
		char cp;
		Node n = new Node();
		for (int i = len-2; i>=1; i--) {
			n.operator=false;
			cp = expr.charAt(i-1);
			c = expr.charAt(i);
			//System.out.println(c);
			if ((c=='+' || c=='-') && parDepth(expr, i) == 0 && cp != '(') {
				n = new Node(true, c+"");
				String le = expr.substring(0,i).trim();
				String ri = expr.substring(i+1, len).trim();
				//System.out.println(le + " lijevi");
				//System.out.println(ri + " desni");
				n.left = giveNode(le);
				n.right = giveNode(ri);
				return n;
			}
		}
		c = expr.charAt(len-1);
		for (int i = len-2; i>=0; i--) {
			n.operator=false;
			cp = c;
			c = expr.charAt(i);
			//System.out.println(c);
			if ((c=='*' || c=='/') && parDepth(expr, i) == 0) {
				n = new Node(true, c+"");
				n.left = giveNode(expr.substring(0,i).trim());
				n.right = giveNode(expr.substring(i+1, len).trim());
				return n;
			}
		}
		
		return new Node();
	}
	
	class Node {
		boolean operator;
		String name;
		Node left, right;
		int value;
		
		public Node(boolean op, String n) {
			operator = op;
			name = n;
			left = new Node();
		}
		
		public Node() {
			
		}
		
		public boolean isNumeric(String str) {
			try {
			    Double.parseDouble(str);
			    return true;
			} catch(NumberFormatException e){
			    return false;
			}
		}
		
		public void evaluate() {

			// this function is called for the root node of an expression and for any nodes which are operators
			//System.out.println(toString());
			// left subtree
			System.out.println(name + " " + operator);
			if (left != null) {
				System.out.println(left.name + " " + left.operator);
			}
			if (right != null) {
				System.out.println(right.name + " " + right.operator);
			}
			if (!operator) {
				String v = name;
				if (isNumeric(v)) {
					instructions += "	MOVE %D " + v + ", R0\n";
					value = Integer.parseInt(v);
					//System.out.println(value);
				}
				else {
					instructions += "	LOAD R0, (" + v + ")\n";
				}
				instructions += "	PUSH R0\n";
				return;
			}
			//System.out.println("number: " + name);
			if (!left.operator) {
				//System.out.println("left idn");
				// then left node is an identifier
				String v = left.name;
				if (isNumeric(v)) {
					instructions += "	MOVE %D " + v + ", R0\n";
					left.value = Integer.parseInt(v);
					System.out.println(left.value);


				}
				else {
					instructions += "	LOAD R0, (" + v + ")\n";
				}
				instructions += "	PUSH R0\n";
			}
			else {
				left.evaluate();
			}
			
			// right subtree
			if (!right.operator) {
				// then right node is an identifier
				String v = right.name;
				if (isNumeric(v)) {
					instructions += "	MOVE %D " + v + ", R0\n";
					right.value = Integer.parseInt(v);
					//System.out.println(right.value);
				}
				else {
					instructions += "	LOAD R0, (" + v + ")\n";
				}
				instructions += "	PUSH R0\n";
			}
			else {
				right.evaluate();
			}
			
			
			
			
			
			instructions += "\n";
			// apply operator
			if (name.equals("+")) {
				instructions += "	POP R1\n	POP R0\n";
				instructions += "	ADD R0, R1, R2\n	PUSH R2\n";
				//System.out.println(instructions);
				value = left.value + right.value;
				return;

			}
			if (name.equals( "-")) {
				instructions += "	POP R1\n	POP R0\n";
				instructions += "	SUB R0, R1, R2\n	PUSH R2\n";
				value = left.value - right.value;
				return;
			}
			if (name.equals("*")) {
				instructions  += "	CALL MUL\n";
				value = left.value * right.value;
			}
			if (name.equals("/")) {
				instructions  += "	CALL DIV\n";
				value = left.value / right.value;
			}
		
		}
		
		public void generateInstructions() {
			// this function is called for the root node of an expression and for any nodes which are operators
			//System.out.println(toString());
			// left subtree
			
			if (!operator) {
				String v = name;
				if (isNumeric(v)) {
					instructions += "	MOVE %D " + v + ", R0\n";
				}
				else {
					instructions += "	LOAD R0, (" + v + ")\n";
				}
				instructions += "	PUSH R0\n";
				return;
			}
			
			if (!left.operator) {
				//System.out.println("left idn");
				// then left node is an identifier
				String v = left.name;
				if (isNumeric(v)) {
					instructions += "	MOVE %D " + v + ", R0\n";
				}
				else {
					instructions += "	LOAD R0, (" + v + ")\n";
				}
				instructions += "	PUSH R0\n";
			}
			else {
				left.generateInstructions();
			}
			
			// right subtree
			if (!right.operator) {
				// then right node is an identifier
				String v = right.name;
				if (isNumeric(v)) {
					instructions += "	MOVE %D " + v + ", R0\n";
				}
				else {
					instructions += "	LOAD R0, (" + v + ")\n";
				}
				instructions += "	PUSH R0\n";
			}
			else {
				right.generateInstructions();
			}
			instructions += "\n";
			// apply operator
			if (name.equals("+")) {
				instructions += "	POP R1\n	POP R0\n";
				instructions += "	ADD R0, R1, R2\n	PUSH R2\n";
				//System.out.println(instructions);

				return;

			}
			if (name.equals( "-")) {
				instructions += "	POP R1\n	POP R0\n";
				instructions += "	SUB R0, R1, R2\n	PUSH R2\n";
				return;
			}
			if (name.equals("*")) {
				instructions  += "	CALL MUL\n";
			}
			if (name.equals("/")) {
				instructions  += "	CALL DIV\n";
			}
		}
		
		public String toString() {
			if (left==null && right==null) {
				return name + " ";
			}
			else {
				return name + "\n| |\n" + ((left!=null) ? left : "") + ((right!=null) ? right : "");
				/*
				String res = "";
				
				int spaceNum = left==null ? 0 : left.name.length();
				spaceNum += right==null ? 0 : right.name.length();
				
				return name + "\n|" + giveSpaces(spaceNum) + "|";
				*/
			}
		}
		
		public String giveSpaces(int numSpaces) {
			String spaces = "";
			for (int i = 0; i<numSpaces; i++) {
				spaces += " ";
			}
			return spaces;
		}
		
	}
	
	
}
