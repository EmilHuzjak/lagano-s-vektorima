package vectUtilities;

import java.util.Hashtable;

import javax.swing.JLabel;
import javax.swing.JSlider;

public class DoubleSlider extends JSlider {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7485059492129306576L;
	
	
	public DoubleSlider(int orien, double min, double max, double init, int divNums1) {
		
		super(orien, (int)(divNums1*min), (int)(divNums1*max), (int)(divNums1*init));
		divNums = divNums1;
		int minint = (int)(divNums*min), maxint = (int)(divNums*max), initint = (int)(divNums*init);
		Hashtable<Integer, JLabel> labelTable = new Hashtable<>();
		for (int i = 0; i<=maxint; i+=10) {
			labelTable.put( Integer.valueOf(i), new JLabel(""+i/divNums*1.0) );
		}
		for (int i = -10; i>=minint; i-=10) {
			labelTable.put( Integer.valueOf(i), new JLabel(""+i/divNums*1.0) );
		}
		setLabelTable( labelTable );
		
		setMajorTickSpacing(1);
		setMinorTickSpacing(divNums/5);
		setPaintTicks(true);
		setPaintLabels(true);
		
	}
	
	int divNums = 1;
	
	public double getDoubleValue() {
		//System.out.println(getValue());
		return 1.0 * getValue() / divNums;
	}
	
	
}
