package vectUtilities;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

import mainpackage.LocVektor;

public class Utility {

	
	public static ImageIcon imageText(String text, int w, int h) {

		BufferedImage im = new BufferedImage(w,h,2);
		Graphics g = im.createGraphics();
		g.setColor(Color.black);
		g.setFont(new Font("Arial", 1, h-4));
		int width = g.getFontMetrics().stringWidth(text);
		
		
		im = new BufferedImage(width+6,h,2);
		g = im.createGraphics();
		g.setColor(Color.black);
		g.setFont(new Font("Arial", 1, h-4));
		
		g.drawLine(1, 3, width+3, 3);
		
		((Graphics2D) g).setStroke(new BasicStroke(1));
		g.fillOval(width-2,1,4,4);
		g.drawLine(width+3, 3, width-2, 1);
		g.drawLine(width+3, 3, width-2, 5);
		g.drawString(text, 3, h-2);
		
		
		return new ImageIcon(im);
		
	}
	
	public static ImageIcon drawModulus(ImageIcon ic) {
		
		BufferedImage im = (BufferedImage)ic.getImage();
		BufferedImage b = new BufferedImage(im.getWidth(), im.getHeight(), im.getType());
	    Graphics gic = b.getGraphics();
	    gic.drawImage(im, 0, 0, null);
	    //
		
		
		((Graphics2D) gic).setStroke(new BasicStroke(2));
		gic.setColor(Color.black);
		gic.drawLine(1, 2, 1, im.getHeight()-3);
		gic.drawLine(im.getWidth()-1, 2, im.getWidth()-1, im.getHeight()-3);
		gic.dispose();
		ImageIcon ic2 = new ImageIcon(b);
		return ic2;
		
	}
	
	
	
	
	
	public static void drawLocLine(LocVektor lv1, Graphics2D g2) {
		LocVektor lv = lv1.copy();
		lv.fix();
		//System.out.println(lv.y0);
		//System.out.println(ys);
		drawLine(lv.x0, lv.y0, lv.x1, lv.y1, g2);
	}
	
	
	public static void drawLine(double x0, double y0, double x1, double y1, Graphics2D g) {
		g.drawLine((int)x0, (int)y0, (int)x1, (int)y1);
	}
	
	
	
	public static double arrowLengthFactor = 1.0/10;
	public static double arrowLengthMax = 10, arrowLengthMin = 4;
	public static double arrowHalfAngle = Math.PI/6;
	public static LocVektor locArrow;
	public static void drawLocVector(LocVektor lv1, Graphics2D g) {
		// TODO Auto-generated method stub
		LocVektor lv = lv1.copy();
		lv.fix();
		drawLocLine(lv, g);
		
		if (lv.mag()<1) {
			return;
		}
		
		
		double arrowL = Math.min(arrowLengthMax, lv.mag()*arrowLengthFactor);
		arrowL = Math.max(arrowLengthMin, arrowL);
		locArrow = lv.copy();
		locArrow.translate(lv);
		locArrow.scaleToR(-arrowL);
		locArrow.rotate(-arrowHalfAngle);
		drawLocLine(locArrow, g);
		locArrow.rotate(2*arrowHalfAngle);
		drawLocLine(locArrow, g);
	}
	
	public static void drawLabelVector(LocVektor lv1, Graphics2D g) {
		// TODO Auto-generated method stub
		double oalf = arrowLengthFactor;
		arrowLengthFactor = 4.0/lv1.mag();
		LocVektor lv = lv1.copy();
		drawLocLine(lv, g);
		lv.fix();
		
		locArrow = lv.copy();
		locArrow.translate(lv);
		locArrow.scale(-arrowLengthFactor);
		locArrow.rotate(-arrowHalfAngle);
		drawLocLine(locArrow, g);
		locArrow.rotate(2*arrowHalfAngle);
		drawLocLine(locArrow, g);
		arrowLengthFactor = oalf;
	}
	
	static int xPos = 0;
	static int yPosTop;
	static int strW;
	static int fontsize = 0;
	public static void drawFancyString(String[] strings, int[] arrowed, double x, double y, Graphics2D g) {
		fontsize = g.getFont().getSize();
		xPos = (int)x;
		yPosTop = (int)y - fontsize;
		Stroke stroke = g.getStroke();
		for (int i = 0; i<strings.length; i++) {
			String s = strings[i];
			g.drawString(s, xPos, (int)y);
			strW = g.getFontMetrics().stringWidth(s);
			if (arrowed[i] == 1) {
				g.setStroke(new BasicStroke(1));
				drawLabelVector(new LocVektor(xPos, yPosTop-2, strW, 0), g);
				g.setStroke(stroke);
			}
			xPos += strW;
		}
		
	}
	
	public static Color randomColor() {
		return new Color((int)(Math.random()*255), (int)(Math.random()*255), (int)(Math.random()*255));
	}
	
	public static Color randomColorDark() {
		return new Color((int)(Math.random()*255)/2, (int)(Math.random()*255/2), (int)(Math.random()*255/2));
	}
	
	public static double random(double a, double b) {
		return Math.random()*(b-a)+a;
	}
	
	
}
