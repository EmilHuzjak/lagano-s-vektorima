package vectUtilities;

import mainpackage.LocVektor;

public class Mapper {
	
	// point in coordinate system represented by the top left corner of screen
	double x0 = 0, y0 = 0;
	
	double unitScreenX = 1, unitScreenY = 1;

	public Mapper(double x0, double y0, double xunit, double yunit) {
		this.x0 = x0;
		this.y0 = y0;
		unitScreenX = xunit;
		unitScreenY = yunit;
	}
	
	public void reset(double x0, double y0, double xunit, double yunit) {
		this.x0 = x0;
		this.y0 = y0;
		unitScreenX = xunit;
		unitScreenY = yunit;
	}

	public Mapper() {
		
	}
	
	
	public double[] mapToScreen(double x, double y) {
		//return new double[] { (x - x0) * unitScreenX, (y - y0) * unitScreenY };
		return new double[] { (x - x0) * unitScreenX, (y0 - y) * unitScreenY };
	}
	
	public double[] mapFromScreen(double x, double y) {
		return new double[] { x0 + x / unitScreenX, y0 - y / unitScreenY };
	}
	
	public LocVektor mapToScreen(LocVektor planeVec) {
		double[] start = mapToScreen(planeVec.x0, planeVec.y0);
		double[] end = mapToScreen(planeVec.x1, planeVec.y1);
		return new LocVektor(start[0], start[1], end[0], end[1], true);
	}

	public LocVektor mapFromScreen(LocVektor screenVec) {
		double[] start = mapFromScreen(screenVec.x0, screenVec.y0);
		double[] end = mapFromScreen(screenVec.x1, screenVec.y1);
		return new LocVektor(start[0], start[1], end[0], end[1], true);
	}

}
