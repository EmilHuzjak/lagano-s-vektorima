package vectUtilities;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class VecTextPanel extends JPanel {

	
	private static final long serialVersionUID = -1098221745470946619L;

	public VecTextPanel() {
		super();
		setLayout(null);
	}
	
	public VecTextPanel(String[] s, int[] arrowed, int fontSize) {
		this();
		text = s;
		this.arrowed = arrowed;
		this.fontSize = fontSize;
		string = true;
		font = new Font("Arial", 1, fontSize);
	}
	
	public void putText(String t) {
		text = new String[] {t};
		arrowed = new int[] {0};
		repaint();
	}
	
	public void putExpression(String[] t, int[] arr) {
		text = t;
		arrowed = arr;
		repaint();
	}
	
	String[] text = new String[] {};
	public int[] arrowed = new int[] {};
	public int fontSize = 1;
	public Font font;
	boolean string = false;
	boolean firstRender = true;
	
	public void resizeFont(int size) {
		fontSize = size;
		font = new Font("Arial", 1, fontSize);
	}
	
	int strw = 0;
	
	Graphics2D g2;
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g2 = (Graphics2D)g;
		g2.setFont(font);
		
		if (firstRender) {
			firstRender = false;
			for (int i = 0; i<text.length; i++) {
				strw += g2.getFontMetrics().stringWidth(text[i]);
			}
			setPreferredSize(new Dimension(strw+10,fontSize+fontSize/2));
			setSize(new Dimension(strw+10,fontSize+fontSize/2));
		}
		
		Utility.drawFancyString(text, arrowed, 5, fontSize+fontSize/5, g2);
		
		
	}
	
	
	
	
}
