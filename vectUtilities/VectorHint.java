package vectUtilities;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

public class VectorHint extends JPanel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int h = 50;
	int hPrompt = 50;
	int wPrompt = 60;
	int w = 200;
	String hint = "klik";
	String text = "";
	
	public VectorHint(String t) {
		super();
		addMouseListener(new Clicker());
		text = t;
		lines = t.split("\n");
		h = (lines.length+1) * sizeFont;
		h = Math.max(hPrompt, h);
		setPreferredSize(new Dimension(w,h));
		
	}
	
	public VectorHint(String t, int wi, int hi) {
		this(t);
		w = wi;
		h = hi;
		//setPreferredSize(new Dimension(w,h));
	}
	
	String[] lines;
	
	boolean expanded = false;
	
	int sizeFont = 18;
	
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		g.setFont(new Font("Arial", 1, hPrompt/2));
		g.drawString(hint, 10, (int)(0.7*hPrompt));
		g.setFont(new Font("Arial", 0, sizeFont));
		g.setColor(new Color(100,100,50));
		if (expanded) {
			for (int i = 0; i<lines.length; i++) {
				String s = lines[i];
				g.drawString(s, wPrompt, (i+1)*sizeFont);
			}
		}
		
	}
	
	class Clicker implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			int x = e.getX();
			int y = e.getY();
			if (x>=0 && x<=wPrompt && y>=0 && y<=hPrompt) {
				
				expanded = !expanded;
				repaint();
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
