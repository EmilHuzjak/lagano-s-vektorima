package vectUtilities;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

import mainpackage.LocVektor;

public class MouseHandler implements MouseListener, MouseMotionListener {

	int numVect = 0;

	public List<LocVektor> vecList = new ArrayList<LocVektor>();

	// 1 at those indexes at which the vectors in vecList can have end points
	// altered
	public List<Integer> endsAlterable = new ArrayList<Integer>();

	// 1 at those indexes at which the vectors in vecList can have start points
	// altered
	public List<Integer> startsAlterable = new ArrayList<Integer>();

	public int startHeld = -1, endHeld = -1;

	double r = 5;

	public Mapper mapper;
	
	public boolean map = false;
	
	public ActionListener action;
	public boolean performaction = false;
	
	
	
	public void putVectors(List<LocVektor> listv, List<Integer> liststarts, List<Integer> listends) {
		vecList = listv;
		startsAlterable = liststarts;
		endsAlterable = listends;
		numVect = vecList.size();
		numVect = Math.min(numVect, Math.min(liststarts.size(), listends.size()));
	}
	
	public void putVector(LocVektor lv, int startalt, int endalt) {
		vecList.add(lv);
		numVect++;
		startsAlterable.add(startalt);
		endsAlterable.add(endalt);
	}
	
	
	@Override
	public void mousePressed(MouseEvent e) {
		// selects first moveable vector tip which has been clicked on, if none then
		// checks the starts
		startHeld = -1;
		endHeld = -1;
		int x = e.getX(), y = e.getY();
		double dx, dy;

		for (int i = 0; i < numVect; i++) {
			if (endsAlterable.get(i) != 1) {
				continue;
			}
			LocVektor fromScreen;
			if (map) {
				fromScreen = mapper.mapToScreen(vecList.get(i));
				
			}
			else {
				fromScreen = vecList.get(i);
			}
			dx = x - fromScreen.x1;
			dy = y - fromScreen.y1;
			if (dx * dx + dy * dy <= r * r) {
				endHeld = i;
				return;
			}
		}
		
		for (int i = 0; i < numVect; i++) {
			if (startsAlterable.get(i) != 1) {
				continue;
			}
			LocVektor fromScreen;
			if (map) {
				fromScreen = mapper.mapToScreen(vecList.get(i));
			}
			else {
				fromScreen = vecList.get(i);
			}
			dx = x - fromScreen.x0;
			dy = y - fromScreen.y0;
			if (dx * dx + dy * dy <= r * r) {
				startHeld = i;
				return;
			}
		}
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		if (endHeld >= 0) {
			if (map) {
				LocVektor heldEnd = mapper.mapToScreen(vecList.get(endHeld));
				heldEnd.x1 = e.getX();
				heldEnd.y1 = e.getY();
				heldEnd.fixLength();
				heldEnd = mapper.mapFromScreen(heldEnd);
				vecList.get(endHeld).matchStart(heldEnd);
				vecList.get(endHeld).matchComponents(heldEnd);
			} else {
				LocVektor heldEnd = vecList.get(endHeld);
				heldEnd.x1 = e.getX();
				heldEnd.y1 = e.getY();
				heldEnd.fixLength();
			}
			performAction();
		} else if (startHeld >= 0) {
			if (map) {
				LocVektor heldEnd = mapper.mapToScreen(vecList.get(startHeld));
				heldEnd.x0 = e.getX();
				heldEnd.y0 = e.getY();
				heldEnd.fixLength();
				heldEnd = mapper.mapFromScreen(heldEnd);
				vecList.get(endHeld).matchStart(heldEnd);
				vecList.get(endHeld).matchComponents(heldEnd);
			} else {
				LocVektor heldStart = vecList.get(startHeld);
				heldStart.x0 = e.getX();
				heldStart.y0 = e.getY();
				heldStart.fix();
			}
			performAction();
		}
	}
	
	public void performAction() {
		if (action != null && performaction) {
			action.actionPerformed(new ActionEvent(action, endHeld, null));
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
