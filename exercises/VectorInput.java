package exercises;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import mainpackage.LocVektor;
import vectUtilities.Utility;
import vectUtilities.VecTextPanel;

public class VectorInput extends JPanel {


	private static final long serialVersionUID = -5632326379858264588L;

	public VectorInput() {
		super();
		//System.out.println("\nconstr input");
		setLayout(null);
		vtp1.addFocusListener(foc);
		vtp2.addFocusListener(foc);
		vtp1.requestFocus();
		checkB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String s1 = vtp1.getText(), s2 = vtp2.getText();
				if (s1.equals("")) {
					xg = 0;
				}
				else {
					try {
						xg = Double.parseDouble(s1);
					} catch (Exception ex) {
						message = "Ne razumijem napisano :(";
						repaint();
						return;
					}
				}
				if (s2.equals("")) {
					yg = 0;
				}
				else {
					try {
						yg = Double.parseDouble(s2);
					} catch (Exception ex) {
						message = "Ne razumijem napisano :(";
						repaint();
						return;
					}
				}
				checkAnswer();
			}
		});

		resetB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				vtp1.setText("0");
				vtp2.setText("0");

			}
		});
		
		skipB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				vtp1.setText("0");
				vtp2.setText("0");
				message = "";
				if (rightListener != null) {
					rightListener.actionPerformed(new ActionEvent(this,1,"Action"));
				}
			}
		});
		
		revB.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				vtp1.setText(""+(int)(100000*res.x)/100000.0);
				vtp2.setText(""+(int)(100000*res.y)/100000.0);
				message = "";
				
			}
		});
		
		initGUI();
		
	}
	
	public boolean needMatchingDirection = false;
	
	String q = "Izračunaj jedinični vektor prikazanog vektora.";
	
	public void checkAnswer() {
		if (needMatchingDirection && Math.abs(LocVektor.angle(xg, yg)-LocVektor.angle(res.x, res.y)) < 0.5) {
			message = "Točno!";
			if (rightListener != null) {
				rightListener.actionPerformed(new ActionEvent(this,1,"Action"));
			}
		}
		else if (!needMatchingDirection && Math.abs(xg-res.x)<=0.1 && Math.abs(yg-res.y)<=0.1 ) {
			message = "Točno!";
			if (rightListener != null) {
				rightListener.actionPerformed(new ActionEvent(this,1,"Action"));
			}
		}
		else {
			message = "Netočno";
		}
		repaint();
	}
	
	String message = "";
	
	ActionListener rightListener;
	
	int whichFocus = -1;
	
	FocusListener foc = new FocusListener() {
		
		@Override
		public void focusLost(FocusEvent e) {
			// TODO Auto-generated method stub
			if (e.getSource().equals(vtp1)) {
				if (vtp1.getText().contentEquals(""))
					vtp1.setText("0");
			}
			else if (e.getSource().equals(vtp2)) {
				if (vtp2.getText().contentEquals(""))
					vtp2.setText("0");
			}
		}
		
		@Override
		public void focusGained(FocusEvent e) {
			// TODO Auto-generated method stub
			message = "";
			
			if (e.getSource().equals(vtp1)) {
				whichFocus = 1;
				if (vtp1.getText().equals("0"))
					vtp1.setText("");
			}
			else if (e.getSource().equals(vtp2)) {
				whichFocus = 2;
				if (vtp2.getText().equals("0"))
					vtp2.setText("");
			}
		}
	};
	
	int space1, space2;
	int vtpw = 100;
	
	int sizeFont = 20;
	Font font = new Font("Arial", 1, sizeFont);
	
	int horPos1 = 10;
	int verPos1 = 10;
	
	int showPW = 1000;
	
	public VecTextPanel qPanel = new VecTextPanel(new String[] {q},
			new int[] {0}, 15);
	
	public void initGUI() {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setPreferredSize(new Dimension(500, sizeFont*5));
				//vtp1.setBounds(horPos1,verPos1,vtpw,sizeFont*3/2);
				//vtp2.setBounds(horPos1+vtpw,verPos1,vtpw,sizeFont*3/2);
				
				//checkB.setBounds(horPos1+vtpw*2,verPos1,vtpw,sizeFont*3/2);
				vtp1.setFont(font);vtp2.setFont(font);
//				add(vtp1);add(vtp2);
//				add(checkB);
				
				showP.setPreferredSize(new Dimension(showPW,100));
				showP.setBounds(horPos1,verPos1,showPW,100);
				showP.add(qPanel);
				
				
				showP.add(vtp1);
				showP.add(new VecTextPanel(new String[] {"i ", " + "}, new int[] {1,0}, sizeFont));
				showP.add(vtp2);
				showP.add(new VecTextPanel(new String[] {"j "}, new int[] {1}, sizeFont));
				showP.add(checkB);
				showP.add(resetB);
				showP.add(skipB);
				showP.add(revB);
				showP.revalidate();
				showP.repaint();
				add(showP);
				
				
				vtp1.setText("0");
				vtp2.setText("0");
				revalidate();
				repaint();
//				repaint();
//				revalidate();
//				repaint();
			}
		});
	}
	
	public void putPrompt(String ques) {
		q = ques;
		qPanel.putText(q);
	}
	
	VectorTextPane vtp1 = new VectorTextPane(), vtp2 = new VectorTextPane();
	JButton checkB = new JButton("Provjeri");
	JButton resetB = new JButton("Odgovore na 0");
	JButton skipB = new JButton("Preskoči");
	JButton revB = new JButton("Otkrij odgovor");
	
	JPanel butPanel = new JPanel();
	
	JPanel showP = new JPanel();
	
	double xg, yg;
	
	LocVektor res = new LocVektor();
	
	Graphics2D g2;
	
	boolean firstRender = true;
	
	@Override
	public void paintComponent(Graphics g) {
		//System.out.println("repaint, firstRender: " + firstRender);
		super.paintComponent(g);
		g2 = (Graphics2D)g;
		g2.setFont(font);
		showPW = showP.getWidth();
		g2.drawString(message, horPos1 + showPW+5, verPos1+sizeFont*2);
		//Utility.drawFancyString(new String[] {"i ", " + "}, new int[]{1, 0}, horPos1+vtpw+2, sizeFont+verPos1,  g2);
		if (firstRender) {
			space1 = 4 + g2.getFontMetrics().stringWidth("i + ");
			Rectangle bounds2 = vtp2.getBounds();
			bounds2.setLocation((int)bounds2.getLocation().getX()+space1, (int)bounds2.getLocation().getY());
			vtp2.setBounds(bounds2);
			bounds2 = checkB.getBounds();
			space2 = space1;
			bounds2.setLocation((int)bounds2.getLocation().getX()+space2+space1, (int)bounds2.getLocation().getY());
			checkB.setBounds(bounds2);
			firstRender = false;
			//revalidate();
		}
		//System.out.println(space1);

		//Utility.drawFancyString(new String[] {"j "}, new int[]{1}, horPos1+vtpw*2+2+space1, sizeFont+verPos1,  g2);
		
		
	}
	
	
	
	
}
