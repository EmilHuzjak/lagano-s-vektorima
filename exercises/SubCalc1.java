package exercises;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import exercises.LinearComb.ControlPanel;
import mainpackage.LocVektor;
import mainpackage.VectorsCart;
import vectUtilities.Utility;

public class SubCalc1 extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2123693423983549824L;





	public SubCalc1() {
		super();
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setLayout(new BoxLayout(SubCalc1.this, BoxLayout.PAGE_AXIS));
				initGUI();
				revalidate();
				repaint();
			}
		});
		
	}
	
	int sizeFont = 20;
	Font font = new Font("Arial", 1, sizeFont);
	int yposTF = sizeFont*2;
	int xpos0TF = 200;
	int heightTF = sizeFont*3/2;
	int widthTF = 80;
	
	int minscal = -5, maxscal = 5;
	int sc1 = 1, sc2 = 1;
	
	JTextField xtf = new JTextField(), ytf = new JTextField();
	
	VectorsCart csv = new VectorsCart();
	
	ControlPanel cp = new ControlPanel();
	
	JButton checkB = new JButton("Provjeri");
	
	public void initGUI() {
		
		//csv.setPreferredSize(new Dimension(1000,1000));
		csv.draggy = false;
		xtf.setBounds(xpos0TF, yposTF, widthTF, heightTF);
		ytf.setBounds(xpos0TF+2*widthTF, yposTF, widthTF, heightTF);
		checkB.setBounds(xpos0TF+4*widthTF,yposTF, widthTF, heightTF);
		cp.add(xtf); cp.add(ytf);
		cp.add(checkB);
		
		cp.revalidate();
		cp.repaint();
		//add(cp);
		//generateExpression();
		add(input);
		input.rightListener = listener;
		add(csv);
		csv.drawLabels = true;
		csv.labels = Arrays.asList(new String[] {"u","w"});
	}
	
	ActionListener listener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			generateExpression();
		}
	};
	
	public void generateExpression() {
		csv.generateRandomVectors(2);
		csv.repaint();
		sc1 = (int)Utility.random(minscal,  maxscal);
		sc2 = (int)Utility.random(minscal,  maxscal);
		LocVektor scaled1 = csv.vectors[0].copy(), scaled2 = csv.vectors[1].copy();
//		scaled1.scale(sc1);
//		scaled2.scale(sc2);
		scaled1.scale(-1);
		res = LocVektor.add(scaled1, scaled2);
		input.res = res;
		input.qPanel.putExpression(new String[] {"Odredite ","v", " tako da ", "u", " + ",
				"v", " = ", "w"},
				new int[] {0,1,0,1,0,1,0,1});
		input.qPanel.resizeFont(15);
	}
	
	
	boolean firstRender = true;
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (firstRender) {
			firstRender = false;
			SwingUtilities.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					generateExpression();
				}
			});
		}
	}
	
	
	LocVektor res = new LocVektor();
	
	Graphics2D g2;
	
	VectorInput input = new VectorInput();
	
	class ControlPanel extends JPanel {

		private static final long serialVersionUID = -1952962741527491779L;
		
		public ControlPanel() {
			super();
			//setLayout(new BoxLayout(ControlPanel.this, BoxLayout.PAGE_AXIS));
			setPreferredSize(new Dimension(500, 4*sizeFont));
		}
		
		public void paintComponent(Graphics g) {
			g2 = (Graphics2D)g;
			Utility.drawFancyString(new String[] {"Prikazani su vektori ","u ", " i ", "v ", ". Izračunajte:"},
					new int[] {0,1,0,1,0}, 50,yposTF+5+sizeFont,g2);
			Utility.drawFancyString(new String[] {"5", "u ", " + ", "3", "v "},
					new int[] {0,1,0,0,1}, 50,yposTF+5+2*sizeFont,g2);
			Utility.drawFancyString(new String[] {"i "}, new int[] {1}, xpos0TF+widthTF, yposTF+heightTF, g2);
			Utility.drawFancyString(new String[] {"j "}, new int[] {1}, xpos0TF+3*widthTF, yposTF+heightTF, g2);

		}
		
		
	}
	
	
	

	
	
	
}
