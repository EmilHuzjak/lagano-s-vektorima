package exercises;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import mainpackage.VectorsCart;

public class UnitVectorCalc extends JPanel {

	
	private static final long serialVersionUID = 7682037545759619694L;
	
	public UnitVectorCalc() {
		super();
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				setLayout(new BoxLayout(UnitVectorCalc.this, BoxLayout.PAGE_AXIS));
				// TODO Auto-generated method stub
				input.rightListener = rightListener;
				
				input.revalidate();
				input.repaint();
				add(input);
				
				//csv.setPreferredSize(new Dimension(1000,1000));
				//csv.setBounds(100,500,1000,1000);
				//csv.repaint();
				add(csv);
				revalidate();
				repaint();
			}
		});
		
		
	}
	
	
	VectorsCart csv = new VectorsCart();
	
	VectorInput input = new VectorInput();
	
	ActionListener rightListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			csv.generateRandomVectors(1);
			csv.repaint();
			input.repaint();
			input.res = csv.vectors[0].unit();
		}
		
	};
	
	int firstRender = 0;
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		System.out.println(csv.vectors[0]);
		System.out.println(csv.yCoordSpan);
		if (firstRender == 0) {
			firstRender = 1;
			SwingUtilities.invokeLater(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					csv.generateRandomVectors(1);
					input.res = csv.vectors[0].unit();
					csv.repaint();
				}
			});
			
			//repaint();
		}
//		if (firstRender == 1) {
//			firstRender = 2;
//			csv.generateRandomVectors(1);
//			input.res = csv.vectors[0].unit();
//			//revalidate();
//			csv.repaint();
//		}
		
	}
	
	
	
	
}
