package exercises;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import mainpackage.LocVektor;
import mainpackage.VectorsCart;
import vectUtilities.Utility;

public class AngleGiven extends JPanel {

	private static final long serialVersionUID = 1537160268626515475L;

	public AngleGiven() {
		super();
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				setLayout(new BoxLayout(AngleGiven.this, BoxLayout.PAGE_AXIS));
				// TODO Auto-generated method stub
				input.rightListener = rightListener;
				//generateQuestion();
				input.qPanel.putExpression(new String[] {"Pronađi vektor ", "u", " tako da kut između ",
						"u", " i ", " v ", "bude " + angle + "°"}, new int[] {0,1,0,1,0,1,0});
				
				input.needMatchingDirection = true;
				
				csv.generateRandomVectors(1);
				
				input.revalidate();
				input.repaint();
				add(input);

				// csv.setPreferredSize(new Dimension(1000,1000));
				// csv.setBounds(100,500,1000,1000);
				// csv.repaint();
				csv.drawLabels = true;
				csv.labels = Arrays.asList(new String[] {"v"});
				add(csv);
				revalidate();
				repaint();
			}
		});

	}

	double angle = Utility.random(0, 180);

	VectorsCart csv = new VectorsCart();

	VectorInput input = new VectorInput();

	ActionListener rightListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			generateQuestion();
		}

	};

	public void generateQuestion() {
		angle = (int)(Utility.random(0, 180) * 100) / 100.0;
		
		csv.generateRandomVectors(1);
		csv.repaint();
		input.qPanel.putExpression(new String[] {"Pronađi vektor ", "u", " tako da kut između ",
				"u", " i ", " v ", "bude " + angle + "°"}, new int[] {0,1,0,1,0,1,0});
		LocVektor res = csv.vectors[0].copy();
		res.rotate(Math.random() < 0.5 ?  angle*Math.PI/180 : -angle*Math.PI/180);
		res.scale(Utility.random(0.5, 1.5));
		input.res = res;
		input.repaint();

	}

	int firstRender = 0;

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		System.out.println(csv.vectors[0]);
		System.out.println(csv.yCoordSpan);
		if (firstRender == 0) {
			firstRender = 1;
			SwingUtilities.invokeLater(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub

					generateQuestion();
					//input.res = csv.vectors[0].unit();
					csv.repaint();
				}
			});

		}


	}

}
