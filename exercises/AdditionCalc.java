package exercises;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import vectUtilities.Utility;

public class AdditionCalc extends JPanel {

	private static final long serialVersionUID = 215207808465095369L;

	int sizeFont = 20;
	Font f = new Font("Arial", 1, sizeFont);

	public AdditionCalc() {
		super();
		setLayout(null);
		setPreferredSize(new Dimension(500, 200));
		generateQuestion();
		checkB.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					xg = Double.parseDouble(xtf.getText());
					yg = Double.parseDouble(ytf.getText());
					if (Math.abs(y - yg) < 0.1 && Math.abs(x - xg) < 0.1) {
						message = "Točno";
						generateQuestion();

					} else {
						message = "Netočno";
					}
				} catch(Exception exc) {
					message = "Krivo";
				}
				
				
				repaint();
			}
		});
		checkB.setBounds( 300, 20, 100, 30);
		add(checkB);
		xtf.setFont(f);
		ytf.setFont(f);
		xtf.setBounds( 200, 20, 50, 30);
		ytf.setBounds( 250, 20, 50, 30);
		ytf.setPreferredSize(new Dimension(100, sizeFont*2));
		xtf.setPreferredSize(new Dimension(100, sizeFont*2));
		add(xtf);
		add(ytf);
		xtf.getDocument().putProperty("owner", xtf);
		ytf.getDocument().putProperty("owner", ytf);
		DocumentListener docl = new DocumentListener() {
			
			public void changedUpdate(DocumentEvent e) {
				event(e);
			}

			public void removeUpdate(DocumentEvent e) {
				event(e);

			}

			public void insertUpdate(DocumentEvent e) {
				event(e);
			}
			
			public void event(DocumentEvent e) {
				message = "";
				try {
					JTextField jtf = (JTextField)(e.getDocument().getProperty("owner"));
					
					String s = jtf.getText();
					if (jtf.equals(xtf)) {
						xg = Double.parseDouble(s);
					} else if (jtf.equals(ytf)) {
						yg = Double.parseDouble(s);
					}
					
				} catch(Exception ex) {
					
				}
				repaint();
			}
			

			
		};
		
		ytf.getDocument().addDocumentListener(docl);
		xtf.getDocument().addDocumentListener(docl);
		
	}

	public void generateQuestion() {
		x1 = (int) Utility.random(xmin, xmax);
		y1 = (int) Utility.random(ymin, ymax);
		x2 = (int) Utility.random(xmin, xmax);
		y2 = (int) Utility.random(ymin, ymax);
		x = x1 + x2;
		y = y1 + y2;
		exp = new String[] { "(" + x1, "i ", ", " + y1, "j ", ") + (" + x2, "i ", ", " + y2, "j ", ")" };
		arrowed = new int[] { 0, 1, 0, 1, 0, 1, 0, 1, 0 };
		xgu = false;
		ygu = false;
		// message = "";
	}

	String[] exp;
	int[] arrowed;

	JTextField xtf = new JTextField(), ytf = new JTextField();

	double xmin = -10, ymin = -10, xmax = 10, ymax = 10;
	double x1, y1, x2, y2;
	double x, y;
	double xg, yg;
	boolean xgu = false, ygu = false;

	JButton checkB = new JButton("Provjeri");

	String message = "";

	Graphics2D g2;

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g2 = (Graphics2D) g;
		g2.setFont(f);
		g2.drawString("Izračunaj zbroj vektora: ", 80, 100 + sizeFont);
		Utility.drawFancyString(exp, arrowed, 80, 100 + 3 * sizeFont, g2);

		String[] guess = new String[] { " = " + xg, "i ", ", " + yg, "j " };
		int[] arrowedGuess = new int[] { 0, 1, 0, 1 };
		Utility.drawFancyString(guess, arrowedGuess, 80, 100 + 5 * sizeFont, g2);

		g2.drawString(message, 330, 100);

	}

}
