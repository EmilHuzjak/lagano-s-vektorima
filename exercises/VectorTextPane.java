package exercises;

import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

public class VectorTextPane extends JTextPane {

	
	
	StyledDocument doc;
	Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
	Style regular;
	Style s;
	int i = 0;
	
	public VectorTextPane() {
		super();
		doc = getStyledDocument();
		regular = doc.addStyle("regular", def);

		//s = doc.addStyle("icon", regular);
	}
	
	public void setMyIcon(ImageIcon ic, String name) {
		s = doc.addStyle(name, regular);
		StyleConstants.setIcon(s, ic);
	}
	
	public void putIcon(ImageIcon ic) {
		setMyIcon(ic, i+"");
		try {
			doc.insertString(doc.getLength(), " ", doc.getStyle(i+""));
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		i++;

	}
	
	public void write(String s) {
		try {
			doc.insertString(doc.getLength(), s, doc.getStyle("regular"));
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
}
