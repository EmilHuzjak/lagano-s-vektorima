package exercises;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import mainpackage.Calculator;
import mainpackage.CoordSystemVectors;

public class AngleCalc extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1511148134525926253L;

	CoordSystemVectors csv = new CoordSystemVectors();
	Calculator calc = new Calculator();
	JButton checkButton = new JButton("Provjeri");
	
	
	double dot = csv.giveDot();
	JLabel message = new JLabel(" ");
	public void init() {
		csv.draggy = false;
		csv.setup();
		csv.generateRandomVectors(2);
		dot = csv.giveDot();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		//setLayout(new GridLayout(1,0));
		///setLayout(new CardLayout());
		add(csv);
		JPanel bottom = new JPanel();
		bottom.add(new JLabel("Izra�?unajte kut između vektora u stupnjevima"));
		bottom.add(calc);
		
		JTextField answer = new JTextField();
		add(answer);
		checkButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String ans = answer.getText();
				double angle = csv.giveAngle();
				if (angle - Double.parseDouble(ans) < 0.1) {
					csv.generateRandomVectors(2);
					message.setText("Tocno!");
					calc.display.setText("");
					csv.repaint();
					dot = csv.giveDot();
					//System.out.println(dot);
				}
				else {
					message.setText("Krivo");
				}
				//System.out.println(dot);
				
				
			}
		});
		bottom.add(checkButton);
		bottom.add(message);
		add(bottom);
		revalidate();
		repaint();
	}
	
	
	public AngleCalc() {
		super();
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				init();
			}
		});
		
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		SwingUtilities.invokeLater(new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			csv.generateRandomVectors(2);
			csv.repaint();
			dot = csv.giveDot();
		}
	});
	}
	
	
}
