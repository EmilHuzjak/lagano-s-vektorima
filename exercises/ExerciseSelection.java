package exercises;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import mainpackage.StartPanel;
//import mainpackage.TaskPanel;

public class ExerciseSelection extends JPanel {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 603071453360242650L;
	
	JMenuBar bar = new JMenuBar();
	JMenu menu1, menu2, menu3, menu4;
	
	JMenuItem i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12;
	
	JPanel jp;
	
	public void initializeMenu() {
		JPanel p = new JPanel();
		menu1 = new JMenu("Zbrajanje");
		menu2 = new JMenu("Oduzimanje");
		menu3 = new JMenu("Skalarni produkt");
		menu4 = new JMenu("Uvod");
		
		
		i1 = new JMenuItem("Izračunaj zbroj");
		i2 = new JMenuItem("Zbrajanje 2");
		i3 = new JMenuItem("Izračunaj nepoznanicu");
		i4 = new JMenuItem("Izračunaj razliku");
		i5 = new JMenuItem("Oduzimanje 3");
		i6 = new JMenuItem("Izračunaj umnožak");
		i7 = new JMenuItem("Ortogonalnost");
		i8 = new JMenuItem("Pronađi vektor");
		i9 = new JMenuItem("Izračunaj kut");
		i10 = new JMenuItem("Linearna kombinacija");
		i11 = new JMenuItem("Jedinični vektori");
		i12 = new JMenuItem("Vektor zadanog kuta");
		
		
		i1.addActionListener(l);
		i3.addActionListener(l);
		i4.addActionListener(l);
		i6.addActionListener(l);
		i7.addActionListener(l);
		i8.addActionListener(l);
		i9.addActionListener(l);
		i10.addActionListener(l);
		i11.addActionListener(l);
		i12.addActionListener(l);
		
		menu1.add(i1);
		menu1.add(i2);
		menu2.add(i3);
		menu2.add(i4);
		//menu2.add(i5);
		menu3.add(i6);
		menu3.add(i7);
		menu4.add(i8);
		menu3.add(i9);
		menu4.add(i10);
		menu4.add(i11);
		menu4.add(i12);
		
		bar.add(menu1);
		bar.add(menu2);
		bar.add(menu3);
		bar.add(menu4);

		p.add(backButton);
		p.add(bar);
		add(p);

	}
	
	JButton backButton = new JButton("Na glavnu stranicu");
	
	DotProductCalc dpc = new DotProductCalc();
	
	public ExerciseSelection() {
		super();
		
		backButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Container par = getParent();
				par.removeAll();
				par.add(new StartPanel());
				par.revalidate();
				par.repaint();
			}
		});
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setLayout(new BoxLayout(ExerciseSelection.this, BoxLayout.PAGE_AXIS));
				add(backButton);
				initializeMenu();
				add(dpc);
				jp = dpc;
				revalidate();
				repaint();
			}
		});
	}
	
	
	public void replaceExercise(JPanel newp) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				
				if (jp == newp)
					return;
				remove(jp);
				jp = newp;
				add(jp);
				
				revalidate();
				repaint();
				
			}
		});
	}
	
	
	
	ActionListener l = new ActionListener() {


		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (e.getSource().equals(i1)) {
				if (jp instanceof AdditionCalc)
					return;
				replaceExercise(new AdditionCalc());
				return;
			}
			if (e.getSource().equals(i3)) {
				if (jp instanceof SubCalc1)
					return;
				replaceExercise(new SubCalc1());
				return;
			}
			if (e.getSource().equals(i4)) {
				if (jp instanceof SubCalc)
					return;
				replaceExercise(new SubCalc());
				return;
			}
			if (e.getSource().equals(i6)) {
				if (jp instanceof DotProductCalc)
					return;
				replaceExercise(new DotProductCalc());
				return;
			}
			if (e.getSource().equals(i7)) {
				if (jp instanceof DotProd0)
					return;
				replaceExercise(new DotProd0());
				return;
			}
			if (e.getSource().equals(i8)) {
				if (jp instanceof TaskPanel)
					return;
				TaskPanel tp = new TaskPanel();
				tp.setPreferredSize(new Dimension(2000,1000));
				replaceExercise(tp);
				return;
			}
			if (e.getSource().equals(i9)) {
				if (jp instanceof AngleCalc)
					return;
				replaceExercise(new AngleCalc());
				return;
			}
			if (e.getSource().equals(i10)) {
				if (jp instanceof LinearComb)
					return;
				replaceExercise(new LinearComb());
				return;
			}
			if (e.getSource().equals(i11)) {
				if (jp instanceof UnitVectorCalc)
					return;
				replaceExercise(new UnitVectorCalc());
				return;
			}
			if (e.getSource().equals(i12)) {
				if (jp instanceof AngleGiven)
					return;
				replaceExercise(new AngleGiven());
				return;
			}
		}
		
	};
	
	
	
}
