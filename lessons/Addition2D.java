package lessons;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import exercises.VectorTextPane;
import mainpackage.LocVektor;
import vectUtilities.MouseHandler;
import vectUtilities.Utility;
import vectUtilities.VectorHint;

public class Addition2D extends SpecificLessonPanel {

	
	private static final long serialVersionUID = 6980550185412728797L;
	
	
	public Addition2D() {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				initContent();
				initGUI();
			}
			
		});
	}
	
	public VectorTextPane vtp1 = new VectorTextPane(), vtp2 = new VectorTextPane(), vtp3 = new VectorTextPane();

	public void initGUI() {
		setLayout(new BoxLayout(Addition2D.this, BoxLayout.PAGE_AXIS));
		add(vtp1);
		add(new VectorHint("Dva vektora pribrojnika i rezultantu stavljamo u oblik trokuta.\n"
				+ "Treba naravno paziti na smjer rezultante da ne bi stavili suprotan!"));
		add(vtp2);
		add(new Addition2DDemo());

		add(vtp3);
		
		revalidate();
		repaint();
		
	}

	int sizeFont = 20;

	Font f = new Font("Arial", 0, sizeFont);
	//Font fBold = new Font("Arial", 1, sizeFont);
	
	
	public void initContent() {
		//vtp1.setFont(fBold);
		vtp1.setFont(f);
		ImageIcon vIcon = Utility.imageText("v", 100, sizeFont+5);
		ImageIcon uIcon = Utility.imageText("u", 100, sizeFont+5);
		ImageIcon modV = Utility.drawModulus(vIcon);
		ImageIcon modU = Utility.drawModulus(uIcon);

		vtp1.write("Općenito, zbrajanje bilo kojih vektora možemo obaviti po već opisanom postupku.\n");
		vtp1.write("Nacrtavši vektor ");
		vtp1.putIcon(vIcon);
		vtp1.write(" i ");
		vtp1.putIcon(uIcon);
		vtp1.write(", zbrajamo ih grafički tako da stavimo početak jednog vektora na kraj drugog.\n");
		vtp1.putIcon(vIcon);
		vtp1.write(" + ");
		vtp1.putIcon(uIcon);
		vtp1.write(" je tada vektor od početka prvog vektora do kraja drugog.\n"
				+ "Uočite zašto se ovo od milja zove pravilo trokuta.");
		
		vtp2.setFont(f);
		vtp2.write("Promatrajte kako pravilo trokuta funkcionira na primjeru");
		
		vtp3.setFont(f);
		
		vtp3.write("Vrijedi ");
		vtp3.putIcon(modV);
		vtp3.write(" + ");
		vtp3.putIcon(modU);
		
		vtp3.write(" ≤ ");
		vtp3.putIcon(modV);
		vtp3.write(" + ");
		vtp3.putIcon(modU);
		vtp3.write(".");

		
	}
	
	class Addition2DDemo extends JPanel {

		
		private static final long serialVersionUID = 1L;
		
		int numVect;
		
		List<LocVektor> vecList = new ArrayList<>();
		
		List<LocVektor> transList = new ArrayList<>();
		List<Integer> movables = new ArrayList<>();
		
		List<Color> colors = new ArrayList<>();
		
		
		public Addition2DDemo() {
			super();
			

			
			setPreferredSize(new Dimension(1000, 500));
			numVect = 2;
			v1 = new LocVektor(100,50,10,100);
			v2 = new LocVektor(200,50,100,-10);
			vecList.add(v1);
			vecList.add(v2);
			LocVektor vt1 = v1.copy();
			vt1.setStartAt(150, 150);
			transList.add(vt1);
			LocVektor vt2 = v2.copy();
			vt2.setStartAt(vt1.x1, vt1.y1);
			transList.add(vt2);

			resv = LocVektor.add(v1, v2);
			resv.matchStart(vt1);
			
			
			MouseHandler mh = new MouseHandler();
			mh.performaction = true;
			mh.action = new VecListener();
			movables.add(1); movables.add(1);
			
			mh.putVectors(vecList, movables, movables);
			addMouseListener(mh);
			addMouseMotionListener(mh);
			
			colors.add(Utility.randomColorDark());
			colors.add(Utility.randomColorDark());
			
		}
		
		LocVektor v1, v2;
		
		LocVektor resv;
		
		Graphics2D g2;
		
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g2 = (Graphics2D) g;
			g2.setColor(Color.blue);
			g2.setStroke(new BasicStroke(3));
			for (int i = 0; i<numVect; i++) {
				g2.setColor(colors.get(i));
				Utility.drawLocVector(vecList.get(i), g2);
				Utility.drawLocVector(transList.get(i), g2);
			}
			g2.setFont(new Font("Arial", 1, 15));
			g2.setStroke(new BasicStroke(2));
			g2.setColor(colors.get(0));
			Utility.drawFancyString(new String[]{"v"}, new int[]{1}, vecList.get(0).x1+5, vecList.get(0).y1+5, g2);
			g2.setColor(colors.get(1));
			Utility.drawFancyString(new String[]{"u"}, new int[]{1}, vecList.get(1).x1+5, vecList.get(1).y1+5, g2);
			
			g2.setColor(Color.red);
			Utility.drawLocVector(resv, g2);
			g2.setColor(new Color(150,0,0));

			Utility.drawFancyString(new String[]{"u", " + ", "v"}, new int[]{1, 0, 1},
					resv.midX(), resv.midY(), g2);
			
			
			
		}
		
		
		
		class VecListener implements ActionListener {

			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				transList.get(0).matchComponents(vecList.get(0));
				resv = transList.get(0).copy();
				for (int i = 1; i<numVect; i++) {
					transList.get(i).matchComponents(vecList.get(i));
					transList.get(i).startEnd(transList.get(i-1));
					resv.add(transList.get(i));
				}
				
				repaint();
			}
			
		}
		
		
	}
	
	
	
	
	public SpecificLessonPanel nextPanel() {
		return new UnitVectors();
	}
	
	
	
}
