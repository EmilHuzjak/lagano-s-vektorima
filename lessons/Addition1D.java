package lessons;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

import exercises.VectorTextPane;
import mainpackage.LocVektor;
import mainpackage.Question;
import vectUtilities.Utility;

public class Addition1D extends SpecificLessonPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2768481784633054834L;

	
	@Override
	public SpecificLessonPanel nextPanel() {
		// TODO Auto-generated method stub
		return new Addition2D();
	}
	
	
	public JTextPane jtp1 = new JTextPane();
	
	public VectorTextPane vtp1 = new VectorTextPane(), vtp2 = new VectorTextPane();
	
	public Addition1D() {
		super();
		initContent();
		initGUI();
	}
	
	public void initGUI() {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setLayout(new BoxLayout(Addition1D.this, BoxLayout.PAGE_AXIS));
				add(vtp1);
				add(new AdditionDemo());
				add(vtp2);
				add (new AdditionDemo(true));
				add(new Question("Ako zbrojimo dva vektora suprotnih orijentacija, rezultat može biti nul-vektor.",
						new String[] {"DA", "NE"}, 0));

				revalidate();
				repaint();
			}
		});
	}
	
	
	
	int sizeFont = 15;
	Font f = new Font("Arial", 1, sizeFont);
	
	
	public void initContent() {
		
		
		vtp1.setFont(f);
		vtp1.write("Jedna od pojava koju opisujemo vektorima je sila - mjerilo jačine vučenja ili guranja.\n ");
		vtp1.write("Zamislimo da dvoje ljudi gura kutiju. Ako guraju u istom smjeru,\n kutija će se micati brže nego da ju gura samo jedan.\n");
		vtp1.write("Intuicija nam govori da zbrajanjem dvaju vektora iste orijentacije dobivamo vektor čija je duljina\n");
		vtp1.write("jednaka zbroju njihovih duljina. S druge strane, ako imaju suprotne orijentacije,\n");
		vtp1.write("djelomično će se poništavati i duljina će biti razlika duljine većeg i manjeg vektora.\n");
		vtp1.write("Upravo tako obavljamo zbrajanje kolinearnih vektora.");

		vtp2.setFont(f);
		vtp2.write("U ovoj demonstraciji smo crveni vektor nazvali ");
		vtp2.putIcon(Utility.imageText("v", 100, sizeFont+5));
		vtp2.write(", žuti ");
		vtp2.putIcon(Utility.imageText("u", 100, sizeFont+5));
		vtp2.write(",\na njihov zbroj ");
		vtp2.putIcon(Utility.imageText("v", 100, sizeFont+5));
		vtp2.write(" + ");
		vtp2.putIcon(Utility.imageText("u", 100, sizeFont+5));
		vtp2.write("je označen crnom bojom.\n");
		
		vtp2.write("Primijetite da jednostavno nižemo vektore jedan na drugi - početak prvog na kraj drugog.\n");
		vtp2.write("Ovo je postupak zvan \"pravilo trokuta\" koji ćemo kasnije opisati u potpunosti");
		vtp2.write("\n\n\nZa zbrajanje vektora vrijedi da je ");
		vtp2.putIcon(Utility.imageText("v", 100, sizeFont+5));
		vtp2.write(" + ");
		vtp2.putIcon(Utility.imageText("u", 100, sizeFont+5));
		vtp2.write(" = ");
		vtp2.putIcon(Utility.imageText("u", 100, sizeFont+5));
		vtp2.write(" + ");
		vtp2.putIcon(Utility.imageText("v", 100, sizeFont+5));
		
		vtp2.write(".\nDakle, zbrajanje je komutativno. Nema veze koji je od pribrojnika prvi.\n"
				+ "Ovo ćemo najbolje vizualizirati ako zamislimo vektore kao pomake pri putovanju.\n"
				+ "Ako se pomaknemo istočno prvo 1 km pa onda 2, to je isto kao da smo se\n"
				+ "pomaknuli istočno prvo 2 km pa onda 1.\n");
		vtp2.write("Demonstrirajmo to pomoću strelica. U prvom slučaju dodajemo žuti vektor na kraj crvenog\n"
				+ ", a u drugom crveni na kraj žutog. Naravno, u oba slučaja rezultat je jednak.");
	}
	
	public ImageIcon giveImageText(String text, int w, int h) {
		BufferedImage im = new BufferedImage(w,h,2);
		Graphics g = im.createGraphics();
		g.setColor(Color.black);
		g.setFont(new Font("Arial", 1, h-3));
		int width = g.getFontMetrics().stringWidth(text);
		
		
		im = new BufferedImage(width+5,h,2);
		g = im.createGraphics();
		g.setColor(Color.black);
		g.setFont(new Font("Arial", 1, h-3));
		
		
		
		g.drawString(text, 1, h-1);
		((Graphics2D) g).setStroke(new BasicStroke(2));
		g.drawLine(1, 3, width+1, 3);
		g.fillOval(width-2,1,4,4);
		g.drawLine(width, 3, width-4, 1);
		g.drawLine(width, 3, width-4, 5);

		return new ImageIcon(im);
	}
	
	
	
	
	public class AdditionDemo extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6990333148424479826L;
		
		
		
		public AdditionDemo() {
			super();
			addMouseMotionListener( new Dragger());
			addMouseListener(new Clicker());
			setBackground(new Color(180, 180, 250));
			setPreferredSize(new Dimension(500, 300));
			v1c = v1.copy(); v2c = v2.copy();
			
			v1c.y0 = 200; v2c.y0 = 205;
			v1c.fix();
			v2c.x0 = v1c.x1;
			v2c.fix();
			
			vres = LocVektor.add(v1c, v2c);
			vres.translate(new LocVektor(0,0,0,40));
			
			
		}
		
		public AdditionDemo(boolean commut) {
			this();
			commutDemo = commut;
			if (commut) {
				setPreferredSize(new Dimension(500,350));
				v1cc = v1.copy(); v2cc = v2.copy();
				v1cc.y0 = 280; v2cc.y0 = 285;
				v2cc.fix();
				v1cc.x0 = v2cc.x1;
				v1cc.fix();
				
				vresc = LocVektor.add(v2cc, v1cc);
				vresc.translate(new LocVektor(0,0,0,40));
			}
			
			
		}
		
		int w, h;
		
		LocVektor v1 = new LocVektor(300, 50, 300, 0), v2 = new LocVektor(300, 100, 100, 0);
		LocVektor v1c, v2c, vres;
		LocVektor v1cc, v2cc, vresc;
		String n1 = "v", n2 = "u";
		
		Graphics2D g2;
		boolean commutDemo = false;
		
		
		int labelWidth = 0;
		
		//boolean forLabel = false;
		
		public void labelVector(String l, double x, double y) {
			int fsize = g2.getFont().getSize();
			labelWidth = g2.getFontMetrics().stringWidth(l);
			//System.out.println(labelWidth);
			g2.drawString(l, (int)x, (int)y);
			g2.setStroke(new BasicStroke(1));
			Utility.drawLabelVector(new LocVektor(x-3, y-fsize-3, labelWidth+4, 0), g2);
		}
		
		
		public void paintComponent(Graphics g) {
			v1.fix();
			v2.fix();
			super.paintComponent(g);
			w = getWidth(); h = getHeight();
			g2 = (Graphics2D)g;
			g2.setStroke(new BasicStroke(3));
			g2.setColor(Color.red);
			Utility.drawLocVector(v1, g2);
			Utility.drawLocVector(v1c, g2);
			
			g2.setColor(Color.yellow);
			Utility.drawLocVector(v2, g2);
			Utility.drawLocVector(v2c, g2);
			
			g2.setColor(Color.black);
			Utility.drawLocVector(vres, g2);
			
			labelVector("v", v1.x1+5, v1.y1-5);
			labelVector("u", v2.x1+5, v2.y1-5);
			labelVector("v", v1c.x1+5, v1c.y1-5);
			labelVector("u", v2c.x1+5, v2c.y1-5);
			labelVector("v", vres.x1+5, vres.y1-5);
			g.drawString(" + ", (int)(vres.x1+5+labelWidth), (int)(vres.y1-5));
			labelWidth += g.getFontMetrics().stringWidth(" + ");
			labelVector("u", vres.x1+5+labelWidth, vres.y1-5);
			
			if (commutDemo) {
				g2.setStroke(new BasicStroke(3));
				g2.setColor(Color.red);
				Utility.drawLocVector(v1cc, g2);
				
				g2.setColor(Color.yellow);
				Utility.drawLocVector(v2cc, g2);
				
				g2.setColor(Color.black);
				Utility.drawLocVector(vresc, g2);
				
				labelVector("v", v1cc.x1+5, v1cc.y1-5);
				labelVector("u", v2cc.x1+5, v2cc.y1-5);
				labelVector("v", v1c.x1+5, v1c.y1-5);
				labelVector("u", v2c.x1+5, v2c.y1-5);
				labelVector("u", vresc.x1+5, vresc.y1-5);
				g.drawString(" + ", (int)(vresc.x1+5+labelWidth), (int)(vresc.y1-5));
				labelWidth += g.getFontMetrics().stringWidth(" + ");
				labelVector("v", vresc.x1+5+labelWidth, vresc.y1-5);
			}
			
		}
		
		
		class Dragger implements MouseMotionListener {

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				int x = e.getX(), y = e.getY();
				
				if (held == 1) {
					//System.out.println(1);
					v1.x1 = x;
					//v1.y1 = y;
					v1.fixLength();
				} else if (held == 2) {
					v2.x1 = x;
					//v2.y1 = y;
					v2.fixLength();
				} else {
					return;
				}

				
				
				v1c = v1.copy(); v2c = v2.copy();
				
				v1c.y0 = 200; v2c.y0 = 205;
				v1c.fix();
				v2c.x0 = v1c.x1;
				v2c.fix();
				
				vres = LocVektor.add(v1c, v2c);
				vres.translate(new LocVektor(0,0,0,40));
				
				if (commutDemo) {
					v1cc = v1.copy(); v2cc = v2.copy();
					v1cc.y0 = 280; v2cc.y0 = 285;
					v2cc.fix();
					v1cc.x0 = v2cc.x1;
					v1cc.fix();
					
					vresc = LocVektor.add(v2cc, v1cc);
					vresc.translate(new LocVektor(0,0,0,40));
				}
				
				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		int held = -1;
		class Clicker implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub

				// TODO Auto-generated method stub
				int x = e.getX(), y = e.getY();
				//System.out.println(x + " " + y);
				//System.out.println(v1.x1 + " " + v1.y1);
				if (Math.abs(x-v1.x1)+ Math.abs(y-v1.y1) < 10  ) {
					held = 1;
				} else if (Math.abs(x-v2.x1)+ Math.abs(y-v2.y1) < 10  ) {
					held = 2;
				} else {
					held = -1;
				}
				
			
			
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
	}
	
	

	
	
	
	
}
