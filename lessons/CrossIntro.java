package lessons;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import mainpackage.LocVektor;
import mainpackage.Vektor;

public class CrossIntro extends SpecificLessonPanel {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CrossIntro() {
		super();
		setBackground(new Color(200, 250, 200));
		setPreferredSize(new Dimension(200, 300));
		repaint();
		addMouseMotionListener(new Clicker());
		w = getWidth();
		h = getHeight();
		v1=new LocVektor();v2=new LocVektor();
		v1.x0=x0; v2.x0=x0;
		v1.y0=y0; v2.y0=y0;
		v1.x=100;v1.y=-100;
		v2.x=-100;v2.y=-100;
		an=Math.sin(v2.angleFrom(v1));
		
		
	}
	
	double an = 0;
	
	
	int held = -1;
	
	int w, h;
	
	int x0=200, y0=200;
	LocVektor v1, v2;
	
	
	public void paintLocLine(LocVektor lv) {
		g2.drawLine((int)lv.x0, (int)lv.y0, (int)(lv.x0+lv.x), (int)(lv.y0+lv.y));
	}
	double arrowLength = 10;
	LocVektor locArrow;
	double arrowHalfAngle = Math.PI/6;
	public void paintLocVektor(LocVektor lv) {
		//System.out.println(lv);
		paintLocLine(lv);
		lv.fix();
		if (lv.r < 2)
			return;
		
		double dxa = -lv.x/arrowLength, dya = -lv.y/arrowLength;
		double r = Math.sqrt(dxa*dxa + dya*dya);
		double an = Vektor.angle(dxa, dya);
		if (r<8) {
			r = 8;
			dxa = r * Math.cos(an);
			dya = r * Math.sin(an);
		}
		an += arrowHalfAngle;
		locArrow = new LocVektor(lv.x1, lv.y1, r*Math.cos(an), r*Math.sin(an));
		paintLocLine(locArrow);
		an -= 2*arrowHalfAngle;
		locArrow.x = r*Math.cos(an);
		locArrow.y = r*Math.sin(an);
		paintLocLine(locArrow);
		
		/*
		locArrow = lv;
		locArrow.fix();
		//g2.setColor(Color.BLACK);
		//System.out.println(locArrow);
		locArrow.x0 += lv.x;
		locArrow.y0 += lv.y;
		locArrow.fix();
		//System.out.println(locArrow);
		//lv.rotate(Math.PI/2);
		//paintLocLine(locArrow);
		//g2.setColor(newCol);

		
		locArrow.scaleToR(-arrowLength);
		//System.out.println(locArrow.r);
		//System.out.println(lv);
		locArrow.fix();
		//System.out.println(lv);
		locArrow.rotate(arrowHalfAngle);
		//System.out.println(locArrow.r);
		paintLocLine(locArrow);
		locArrow.rotate(-2 * arrowHalfAngle);
		//g2.setColor(Color.ORANGE);
		paintLocLine(locArrow);
		locArrow = null;
		*/
	}
	
	Graphics2D g2;
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g2=(Graphics2D)g;
		g2.setStroke(new BasicStroke(3));
		paintLocVektor(v1);paintLocVektor(v2);
		double r = Math.abs(an*20);
		g2.fillOval((int)(v1.x0-r),(int)(v1.y0-r), (int)(2*r),(int)(2*r));
	}
	
	
	class Clicker implements MouseMotionListener {

		
		@Override
		public void mouseDragged(MouseEvent e) {
			int x = e.getX(), y = e.getY();
			
			if (held == -1) {
				if (Math.abs(v1.x1-x)+Math.abs(v1.y1-y)<10) {
					held = 1;
				}
				else if (Math.abs(v2.x1-x)+Math.abs(v2.y1-y)<10) {
					held = 2;
				}
				
			}
			
			if (Math.abs(v1.x1-x)+Math.abs(v1.y1-y)<10) {
				held = 1;
				v1.x1=x;v1.y1=y;
				v1.fixLength();
			}
			else if (Math.abs(v2.x1-x)+Math.abs(v2.y1-y)<10) {
				held = 2;

				v2.x1=x;v2.y1=y;
				v2.fixLength();
			}
			else {
				held = -1;
				return;
			}
			an = v2.angleFrom(v1);
			an = Math.sin(an);
			repaint();
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	}
	
	class Mouse implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			held = -1;
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	

}
