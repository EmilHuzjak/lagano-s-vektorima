package lessons;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

import exercises.VectorTextPane;
import lessons.Addition1D.AdditionDemo;
import lessons.Addition1D.AdditionDemo.Clicker;
import lessons.Addition1D.AdditionDemo.Dragger;
import mainpackage.LocVektor;
import vectUtilities.Utility;

public class UnitVectors extends SpecificLessonPanel {

	
	
	


	/**
	 * 
	 */
	private static final long serialVersionUID = 2768481784633054834L;

	
	@Override
	public SpecificLessonPanel nextPanel() {
		// TODO Auto-generated method stub
		return new MultByScalar();
	}
	
	
	public JTextPane jtp1 = new JTextPane();
	
	public VectorTextPane vtp1 = new VectorTextPane(), vtp2 = new VectorTextPane();
	
	public UnitVectors() {
		super();
		initContent();
		initGUI();
	}
	
	public void initGUI() {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				setLayout(new BoxLayout(UnitVectors.this, BoxLayout.PAGE_AXIS));
				add(vtp1);
				add(new UnitDemo());
				//add(vtp2);
				revalidate();
				repaint();
			}
		});
	}
	
	
	
	int sizeFont = 15;
	Font f = new Font("Arial", 1, sizeFont);
	
	
	public void initContent() {
		
		
		vtp1.setFont(f);
		vtp1.write("Vektor koji ima duljinu 1 zovemo jedinični vektor.\n");
		vtp1.write("Svaki vektor, osim nul-vektora, ima svoj jedinični vektor. To je vektor duljine 1\n"
				+ "Koji ima isti smjer i orijentaciju kao početni vektor.");
		vtp1.write("\n\nMijenjajte vektor ");
		vtp1.putIcon(Utility.imageText("v", 100, sizeFont+2));
		vtp1.write(" i proučite kako se mijenja njegov jedinični vektor, označen ");
		vtp1.putIcon(Utility.imageText("vjed", 100, sizeFont+2));
		vtp1.write("\ni nacrtan plavom bojom.\n");
		
		
//		vtp2.setFont(f);
//		vtp2.write("U ovoj demonstraciji smo crveni vektor nazvali ");
//		vtp2.putIcon(Utility.imageText("v", 100, sizeFont+5));
//		vtp2.write(", žuti ");
//		vtp2.putIcon(Utility.imageText("u", 100, sizeFont+5));
//		vtp2.write(",\na njihov zbroj ");
//		vtp2.putIcon(Utility.imageText("v", 100, sizeFont+5));
//		vtp2.write(" + ");
//		vtp2.putIcon(Utility.imageText("u", 100, sizeFont+5));
//		vtp2.write("je označen crnom bojom.\n");
//		
//		vtp2.write("Primijetite da jednostavno nižemo vektore jedan na drugi - početak prvog na kraj drugog.\n");
//		vtp2.write("Ovo je postupak zvan \"pravilo trokuta\" koji ćemo kasnije opisati u potpunosti");
//		vtp2.write("\n\n\nZa zbrajanje vektora vrijedi da je ");
//		vtp2.putIcon(Utility.imageText("v", 100, sizeFont+5));
//		vtp2.write(" + ");
//		vtp2.putIcon(Utility.imageText("u", 100, sizeFont+5));
//		vtp2.write(" = ");
//		vtp2.putIcon(Utility.imageText("u", 100, sizeFont+5));
//		vtp2.write(" + ");
//		vtp2.putIcon(Utility.imageText("v", 100, sizeFont+5));
//		
//		vtp2.write(".\nDakle, zbrajanje je komutativno. Nema veze koji je od pribrojnika prvi.\n"
//				+ "Ovo ćemo najbolje vizualizirati ako zamislimo vektore kao pomake pri putovanju.\n"
//				+ "Ako se pomaknemo istočno prvo 1 km pa onda 2, to je isto kao da smo se\n"
//				+ "pomaknuli istočno prvo 2 km pa onda 1.\n");
//		vtp2.write("Demonstrirajmo to pomoću strelica. U prvom slučaju dodajemo žuti vektor na kraj crvenog\n"
//				+ ", a u drugom crveni na kraj žutog. Naravno, u oba slučaja rezultat je jednak.");
	}
	
	public ImageIcon giveImageText(String text, int w, int h) {
		BufferedImage im = new BufferedImage(w,h,2);
		Graphics g = im.createGraphics();
		g.setColor(Color.black);
		g.setFont(new Font("Arial", 1, h-3));
		int width = g.getFontMetrics().stringWidth(text);
		
		
		im = new BufferedImage(width+5,h,2);
		g = im.createGraphics();
		g.setColor(Color.black);
		g.setFont(new Font("Arial", 1, h-3));
		
		
		
		g.drawString(text, 1, h-1);
		((Graphics2D) g).setStroke(new BasicStroke(2));
		g.drawLine(1, 3, width+1, 3);
		g.fillOval(width-2,1,4,4);
		g.drawLine(width, 3, width-4, 1);
		g.drawLine(width, 3, width-4, 5);

		return new ImageIcon(im);
	}
	
	
	
	
	public class UnitDemo extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6990333148424479826L;
		

		
		
		public UnitDemo() {
			super();
			addMouseMotionListener( new Dragger());
			addMouseListener(new Clicker());
//			setBackground(new Color(180, 180, 250));
//			setPreferredSize(new Dimension(500, 300));
//			v1c = v1.copy(); v2c = v2.copy();
//			
//			v1c.y0 = 200; v2c.y0 = 205;
//			v1c.fix();
//			v2c.x0 = v1c.x1;
//			v2c.fix();
//			
//			vres = LocVektor.add(v1c, v2c);
//			vres.translate(new LocVektor(0,0,0,40));
			
			setPreferredSize(new Dimension(500, 300));
			
			v = new LocVektor(200, 100, -100,-50);
			vunit = v.unit();
			vunit.scaleToR(50);
			vunit.translate(new LocVektor(0,100));
		}
		
		
		
		int w, h;
		
		LocVektor v1 = new LocVektor(300, 50, 300, 0), v2 = new LocVektor(300, 100, 100, 0);
		LocVektor v1c, v2c, vres;
		LocVektor v1cc, v2cc, vresc;
		String n1 = "v", n2 = "u";
		
		Graphics2D g2;
		boolean commutDemo = false;
		
		LocVektor v, vunit;
		
		int labelWidth = 0;
		
		//boolean forLabel = false;
		
		public void labelVector(String l, double x, double y) {
			int fsize = g2.getFont().getSize();
			labelWidth = g2.getFontMetrics().stringWidth(l);
			//System.out.println(labelWidth);
			g2.drawString(l, (int)x, (int)y);
			g2.setStroke(new BasicStroke(1));
			Utility.drawLabelVector(new LocVektor(x-3, y-fsize-3, labelWidth+4, 0), g2);
		}
		
		
		public void paintComponent(Graphics g) {
			super.paintComponent(g);

			v.fix();
			vunit.fix();
			//w = getWidth(); h = getHeight();
			g2 = (Graphics2D)g;
			g2.setStroke(new BasicStroke(3));
			g2.setColor(Color.red);
			Utility.drawLocVector(v, g2);
			g2.setColor(Color.blue);
			Utility.drawLocVector(vunit, g2);
			
			
			
			labelVector("v", v.x1+5, v.y1-5);
			labelVector("vjed", vunit.x1+5, vunit.y1-5);
			
			
			
		}
		
		
		class Dragger implements MouseMotionListener {

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				int x = e.getX(), y = e.getY();
				
				if (held == 1) {
					//System.out.println(1);
					v.x1=x; v.y1 = y;
					//v1.y1 = y;
					v.fixLength();
					vunit = v.unit();
					vunit.scale(50);
					vunit.translate(new LocVektor(0,100));
				}
				
				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		int held = -1;
		class Clicker implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

				// TODO Auto-generated method stub

				// TODO Auto-generated method stub
				int x = e.getX(), y = e.getY();
				//System.out.println(x + " " + y);
				//System.out.println(v1.x1 + " " + v1.y1);
				if (Math.abs(x-v.x1)+ Math.abs(y-v.y1) < 10  ) {
					System.out.println(1);
					held = 1;
				} else {
					held = -1;
				}
				
			
			
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
	}
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
}
