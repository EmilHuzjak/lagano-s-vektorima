package lessons;

import java.awt.BasicStroke;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import exercises.VectorTextPane;
import mainpackage.Question;
import vectUtilities.Utility;
import vectUtilities.VectorHint;


public class Intro extends SpecificLessonPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8546798545389109392L;

	
	@Override
	public SpecificLessonPanel nextPanel() {
		return new Addition1D();
	}
	
	public Intro() {
		super();
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				initContent();
				revalidate(); repaint();
			}
		});
	}
	
	
	
	int sizeFont;
	
	public void initContent() {


		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		// setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		JTextArea jta1 = new JTextArea();
		jta1.setEditable(false);
		jta1.append("Vektori su usmjerene dužine. To znači da imaju duljinu, orijentaciju i smjer.\n");
		jta1.append("Usredotočimo se na jednu dimenziju. Imamo samo dvije orijentacije: lijevo i desno.\n");
		jta1.append("Stoga, vektor u jednoj dimenziji daje položaj točke na brojevnom pravcu.\n");
		jta1.append("Često prikazujemo vektor strelicom koja ima orijentaciju i duljinu iste kao taj vektor.\n\n\n");
		jta1.append("Pomičite kraj vektora (vrh strelice) da bi vidjeli kako se mijenjaju duljina i orijentacija vektora");

		add(jta1);
		/*
		 * JPanel inner = new JPanel(); inner.setPreferredSize(new Dimension(200, 100));
		 * inner.setBackground(Color.green); add(inner);
		 */

		VectorPanel1D p1 = new VectorPanel1D();
		add(p1);
		
		JTextArea jta2 = new JTextArea();
		jta2.setEditable(false);
		jta2.append("Iskoristimo cijeli potencijal brojevnog pravca i opišimo vektor samo jednim brojem.\n");
		jta2.append("Umjesto riječi lijevo ili desno ćemo koristiti znak (pozitivno ili negativno)\n");
		jta2.append("Kako smo brojevni pravac odlučili orijentirati s pozitivnim vrijednostima prema desno,\n");
		jta2.append("onda će svi vektori koji imaju orijentaciju prema desno biti \"pozitivni\", a oni prema lijevo \"negativni\".\n");
		
		
		
		add(jta2);
		//System.out.println("y: " + jta2.getY());

		VectorPanel1D p2 = new VectorPanel1D();
		p2.envelopInfo = true;
		add(p2);
		
		JTextArea jta3 = new JTextArea();
		jta3.setEditable(false);
		jta3.append("Rekli smo da vektor opisuju duljina, smjer i orijentacija, no nismo ništa rekli o tome gdje počinje.\n");
		jta3.append("I zaista (iako smo ga dosad radi jasnoće crtali s početkom u 0)\n,\n");
		jta3.append("vektor smatramo neovisnim o mjestu gdje se nalazi. Možemo ga pomicati bez da ga promijenimo.\n");

		add(jta3);
		
		add(new Question("Raketa i kamen se gibaju vertikalno - raketa gore, a kamen dolje.\n"
				+ "Vektori njihovih brzina imaju jednake:", new String[] {"Orijentacije", "Smjerove", "Module"}, 1));
		
		add(new Question("Vektori istih orijentacija imaju iste smjerove", new String[] {"DA", "NE"}, 0));
		
		SeveralVectorPanel1D p3 = new SeveralVectorPanel1D();
		p3.envelopInfo = true;
		//add(p3);

		
		
		
		JTextPane jtp1 = new JTextPane();
		Font f = jtp1.getFont();
		sizeFont = f.getSize();
		f = new Font("Arial", 1, 20);
		jtp1.setFont(f);
		sizeFont = f.getSize();
		StyledDocument doc = jtp1.getStyledDocument();
		Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);

		Style regular = doc.addStyle("regular", def);
		Style s = doc.addStyle("icon", regular);
		
		//StyleConstants.setFontFamily(def, "SansSerif");
		//doc.append(giveImageText("v", 10, 20), "icon");
		
		ImageIcon vecIcon = Utility.imageText("v", 30, sizeFont);
		StyleConstants.setIcon(s, vecIcon);
		
		s = doc.addStyle("icon2", regular);
		StyleConstants.setIcon(s, Utility.imageText("AB", 30,  sizeFont));

		s = doc.addStyle("icon3", regular);
		StyleConstants.setIcon(s, Utility.drawModulus(Utility.imageText("v", 30,  sizeFont)));
		
		
		try {
			doc.insertString(doc.getLength(), "Jedan od standardnih načina zapisivanja vektora je pomoću strelice na vrhu: ", doc.getStyle("regular"));
			doc.insertString(doc.getLength(), " ", doc.getStyle("icon"));
			doc.insertString(doc.getLength(), "\nNa primjer, ", doc.getStyle("regular"));
			doc.insertString(doc.getLength(), " ", doc.getStyle("icon2"));
			doc.insertString(doc.getLength(), " je izraz za vektor od točke A do točke B.\n", doc.getStyle("regular"));
			doc.insertString(doc.getLength(), "Ovo nije samo stilistička odluka - važno je prepoznati\n odnosi li se neki izraz na vektor ili jednostavno na broj, jer to utječe na način\n na koji obavljamo matematičke operacije nad njima.\n\n", doc.getStyle("regular"));
			doc.insertString(doc.getLength(), "Duljinu vektora nazivamo njegovim modulom i zapisujemo sa okomitim crtama sa strane. \nOna je broj veći ili jednak nuli.\n\n", doc.getStyle("regular"));
			doc.insertString(doc.getLength(), " ", doc.getStyle("icon3"));
			doc.insertString(doc.getLength(), "  - duljina (modul) vektora ", doc.getStyle("regular"));
			doc.insertString(doc.getLength(), " ", doc.getStyle("icon"));

		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		add(jtp1);
		
		ImageIcon vicon = Utility.imageText("v", 50, sizeFont);
		
		VectorTextPane vtp1 = new VectorTextPane();
		vtp1.setFont(new Font("Arial", 1, sizeFont));
		vtp1.write("Vektor duljine 0 nazivamo nul-vektor. Pišemo ga ");
		vtp1.putIcon(Utility.imageText("0", 50, sizeFont));
		vtp1.write(".\nSvi nul-vektori su međusobno jednaki.\n"
				+ "Kažemo da nul-vektor nema orijentaciju ni smjer. Razmislite zašto.");
		add(vtp1);
		add(new VectorHint("Po definiciji, vektor koji nema duljinu ne može pokazivati\n"
				+ "nigdje. Mogli bismo također reći da on ima sve orijentacije odjednom,\n"
				+ "no to se ne pokazuje korisno i ostat ćemo na definiciji da nema nijednu.", 500, 100));
		
		Button nextBut = new Button("Dalje");
		nextBut.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					/*
					vf.remove(jsp);
					vf.add(new JScrollPane(vf.lessonP));
					*/
					removeAll();
					/*
					try {
						putAddition();
					} catch (InvocationTargetException | InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					*/
					revalidate();
					//LessonPanel.this.revalidate();

				}
			});
		add(nextBut);
		add(new JLabel("Vektori"));
		//add(bufferP);
		revalidate();
		repaint();

		//revalidate();
		
		
	
	}
	
	public void putAddition() {
		
	}
	
	class VectorPanel1D extends JPanel {


		private static final long serialVersionUID = 1L;

		public VectorPanel1D() {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 200));
			repaint();
			addMouseMotionListener(new Clicker());
		}
		
		int w, h;

		int lineUnits = 10;
		int unitWidth;
		double xV = 2.5;
		int x0;
		
		double x01 = 1;
		double x02 = -4;
		
		
		boolean showInfo = true;
		
		boolean envelopInfo = false;
		
		@Override
		public void paintComponent(Graphics g) {
			Font f = g.getFont();
			super.paintComponent(g);
			w = getWidth();
			h = getHeight();
			x0 = w / 2;
			unitWidth = w/lineUnits;
			Graphics2D g2d = (Graphics2D) g;
			int y = h / 2;
			g2d.setColor(Color.black);
			
			g2d.drawLine(0, y, w, y);
			int arrowLen = 30;
			if (envelopInfo) {
				g2d.drawLine(w, y, w-arrowLen, y-arrowLen/2);
				g2d.drawLine(w, y, w-arrowLen, y+arrowLen/2);
			}
			
			g2d.drawLine(x0, y-5, x0, y+5);
			g2d.drawString("0", x0, y+15);
			
			for (int i = 1; i<=lineUnits/2; i++) {
				int xcoor = i*unitWidth + x0;
				g2d.drawLine(xcoor, y-5, xcoor, y+5);
				g2d.drawString(""+i, xcoor+5, y+15);
				g2d.drawLine(2*x0 - xcoor, y-5, 2*x0 - xcoor, y+5);
				g2d.drawString("-"+i, 2*x0 - xcoor+5, y+15);
				
			}
			g2d.setColor(Color.blue);
			g2d.setStroke(new BasicStroke(5));
			int xVs = x0 + (int)(xV*unitWidth);
			int x01s = x0 + (int)(xV*unitWidth);
			int xV1s = x01s + (int)(xV*unitWidth);
			g2d.drawLine(x0, y, xVs, y);
			arrowLen = (int)(xV*unitWidth * 0.1);
			int arrowLen1 = Math.min(Math.abs(arrowLen), w/50);
			arrowLen1 = Math.max(Math.abs(arrowLen),  8);
			arrowLen = arrowLen1 * (int)Math.signum(arrowLen);
			//g2d.setStroke(new BasicStroke(3));
			g2d.drawLine(xVs, y, xVs-arrowLen, y-arrowLen/3);
			g2d.drawLine(xVs, y, xVs-arrowLen, y+arrowLen/3);
			if (showInfo) {
				//f.deriveFont(10);
				//System.out.println(f);
				//g.setFont(new Font("Dialog", Font.BOLD, 20));
				g.setFont(f.deriveFont(Font.BOLD, 15f));
				if (envelopInfo) {
					g.drawString("Iznos vektora: " + (int)(100*xV)/100.0, w/3, y+40);
				}
				else {
					g.drawString("Duljina: " + (int)(100*Math.abs(xV))/100.0 + ", smjer: " + (xV<0 ? "lijevo" : "desno"), w/3, y+40);
				}
			}
		}
		
		class Clicker implements MouseMotionListener {

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				xV = e.getX();
				xV = (xV-x0)/unitWidth;
				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}}
		
	
	}
	
	class SeveralVectorPanel1D extends JPanel {


		private static final long serialVersionUID = 1L;

		public SeveralVectorPanel1D() {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 300));
			repaint();
			addMouseMotionListener(new Clicker());
			addMouseListener(new ML());
			w = getWidth();
			h = getHeight();
			//System.out.println(w);
			//introduce();
		}

		int w, h;

		int lineUnits = 10;
		int unitWidth;
		double xV = 2.5;
		int x0;
		int vecAmt = 2;
		boolean showInfo = true;
		double[] vecStarts, vecEnds;
		double ys[];
		public void introduce() {
			vecStarts = new double[vecAmt];
			vecEnds = new double[vecAmt];
			ys = new double[vecAmt];
			for (int i = 0; i < vecAmt; i++ ) {
				ys[i] = 0.2*h + 0.6*h/vecAmt*i;
				//System.out.println(ys[i]);
				vecStarts[i] = 0;
				vecEnds[i] = vecStarts[i] + xV;
			}
		}
		
		boolean drawAxes = true;
		boolean envelopInfo = false;
		
		boolean firstRender = true;
		
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			w = getWidth();
			h = getHeight();
			//System.out.println(h);
			if (firstRender) {
				introduce();
				//System.out.println(h);
				//System.out.println(ys[0]);
				//System.out.println(ys[1]);


				firstRender = false;
			}
			Font f = g.getFont();
			
			x0 = w / 2;
			unitWidth = w/lineUnits;
			Graphics2D g2d = (Graphics2D) g;
			int y = h / 2;
			g2d.setColor(Color.black);
			int arrowLen = 30;
			//System.out.println(ys[0]);
			if (drawAxes) {
				for (int i = 0; i<vecAmt; i++) {
					//int yVs = 
					g2d.drawLine(0, (int)ys[i], w, (int)ys[i]);
					if (envelopInfo) {
						g2d.drawLine(w, (int)ys[i], w-arrowLen, (int)ys[i]-arrowLen/2);
						g2d.drawLine(w, (int)ys[i], w-arrowLen, (int)ys[i]+arrowLen/2);
					}
					g2d.drawLine(x0, (int)ys[i]-5, x0, (int)ys[i]+5);
					g2d.drawString("0", x0, (int)ys[i]+15);
					
					
					for (int j = 1; j<=lineUnits/2; j++) {
						int xcoor = j*unitWidth + x0;
						g2d.drawLine(xcoor, (int)ys[i]-5, xcoor, (int)ys[i]+5);
						g2d.drawString(""+j, xcoor+5, (int)ys[i]+15);
						g2d.drawLine(2*x0 - xcoor, (int)ys[i]-5, 2*x0 - xcoor, (int)ys[i]+5);
						g2d.drawString("-"+j, 2*x0 - xcoor+5, (int)ys[i]+15);
						
					}
				}
			}
			g.setFont(f.deriveFont(Font.BOLD, 15f));
			g2d.setColor(Color.blue);
			g2d.setStroke(new BasicStroke(5));
			for (int i = 0; i<vecAmt; i++) {
				int xVs = x0 + (int)((vecStarts[i] + xV)*unitWidth);
				g2d.drawLine(x0+(int)(vecStarts[i]*unitWidth), (int)ys[i], xVs, (int)ys[i]);
				arrowLen = (int)(xV*unitWidth * 0.1);
				int arrowLen1 = Math.min(Math.abs(arrowLen), w/50);
				arrowLen1 = Math.max(Math.abs(arrowLen),  8);
				arrowLen = arrowLen1 * (int)Math.signum(arrowLen);
				//g2d.setStroke(new BasicStroke(3));
				g2d.drawLine(xVs, (int)ys[i], xVs-arrowLen, (int)ys[i]-arrowLen/3);
				g2d.drawLine(xVs, (int)ys[i], xVs-arrowLen, (int)ys[i]+arrowLen/3);
				if (showInfo) {
					//f.deriveFont(10);
					//System.out.println(f);
					//g.setFont(new Font("Dialog", Font.BOLD, 20));
					g.setFont(f.deriveFont(Font.BOLD, 15f));
					if (envelopInfo) {
						g.drawString("Iznos vektora: " + (int)(100*xV)/100.0, w/3, (int)ys[i]+40);
					}
					else {
						g.drawString("Duljina: " + (int)(100*Math.abs(xV))/100.0 + ", smjer: " + (xV<0 ? "lijevo" : "desno"), w/3, (int)ys[i]+40);
					}
				}
			}
			
		}
		
		int held = -1;

		class ML implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				held = -1;	
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
		class Clicker implements MouseMotionListener {

			
			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				
				
				int xe = e.getX(), ye = e.getY();
				
				//System.out.println("dragged " + xe + " " + ye);
				
				if (held >= 0) {
					vecStarts[held] = (xe-x0)*1.0/unitWidth;
				}
				else 
				for (int i = 0; i<vecAmt; i++) {
					int x0i = (int)(vecStarts[i]*unitWidth + x0);
					int dx = xe - x0i;
					int dy = ye - (int)ys[i];
					if (dx*dx + dy*dy < 9) {
						
					}
				}
				
				xV = xe;
				xV = (xV-x0)/unitWidth;
				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}}
		
	}
	
	
	
	
	

	
	
	
}
