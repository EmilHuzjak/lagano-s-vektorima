package lessons;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import exercises.VectorTextPane;
import mainpackage.LocVektor;
import vectUtilities.DoubleSlider;
import vectUtilities.Utility;

public class MultByScalar extends SpecificLessonPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8705395416065571852L;

	@Override
	public SpecificLessonPanel nextPanel() {
		return new Intro2D();
	}
	
	
	public MultByScalar() {
		super();

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {

				initContent();
				initGUI();
			}
		});
	}

	public VectorTextPane vtp1 = new VectorTextPane(), vtp2 = new VectorTextPane();

	public void initGUI() {
		setLayout(new BoxLayout(MultByScalar.this, BoxLayout.PAGE_AXIS));
		add(vtp1);
		add(new MultDemo());
		add(vtp2);
		revalidate();
		repaint();
		
	}

	int sizeFont = 20;

	Font f = new Font("Arial", 0, sizeFont);
	//Font fBold = new Font("Arial", 1, sizeFont);
	public void initContent() {
		//vtp1.setFont(fBold);
		vtp1.setFont(f);
		ImageIcon vIcon = Utility.imageText("v", 100, sizeFont+5);
		vtp1.write("Vektor možemo množiti brojem (skalarom). Rezultat toga je drugi vektor.\n");
		vtp1.write("Množenje nekog iznosa brojem često shvaćamo kao povećanje\n"
				+ "ili smanjivanje tog iznosa za faktor kojim smo množili (2n je dvostruko veće od n).");
		vtp1.write("\nIstu intuiciju možemo koristiti i pri množenju vektora brojem. Vektor ");
		vtp1.putIcon(vIcon);
		vtp1.write("\npomnožen brojem x je novi vektor, zapisan x·");
		vtp1.putIcon(vIcon);
		vtp1.write(" ili jednostavno x");
		vtp1.putIcon(vIcon);
		vtp1.write(" koji ima isti smjer i\norijentaciju kao vektor ");
		vtp1.putIcon(vIcon);
		vtp1.write("no duljinu koja je povećana (ili smanjena, ako x<1)\nupravo za faktor x.");
		
		vtp2.setFont(f);
		vtp2.write("Primijetimo da množenje vektora negativnim brojem daje vektor suprotne orijentacije.\n");
		vtp2.write("-1");
		vtp2.putIcon(vIcon);
		vtp2.write(" pišemo skraćeno i kao -");
		vtp2.putIcon(vIcon);
		vtp2.write(". Vektori -");
		vtp2.putIcon(vIcon);
		vtp2.write(" i ");
		vtp2.putIcon(vIcon);
		vtp2.write("su iste duljine i suprotne orijentacije.");
		vtp2.write("\nNaravno, u izrazu 1");
		vtp2.putIcon(vIcon);
		vtp2.write(" nije potrebno pisati 1 i skraćujemo ga na običan ");
		vtp2.putIcon(vIcon);
	}

	
	
	
	class MultDemo extends JPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 8192379484166820966L;
		
		JSlider scalar1 = new JSlider(JSlider.HORIZONTAL,
                -10, 10, 1);
		
		DoubleSlider s1 = new DoubleSlider(JSlider.HORIZONTAL,
                -3, 3, 1, 10);
		
		int sizeFontDemo = 20;
		
		double scale = 1;
		
		public MultDemo() {
			addMouseMotionListener(new Dragger());
			addMouseListener(new Clicker());
			vsc.scale(50);
			vsc.translate(new LocVektor(100,0));
			setPreferredSize(new Dimension(500, 500));
			scalar1.setMajorTickSpacing(2);
			scalar1.setMinorTickSpacing(1);
			scalar1.setPaintTicks(true);
			scalar1.setPaintLabels(true);
			s1.addChangeListener(new ChangeListener() {
				
				@Override
				public void stateChanged(ChangeEvent e) {
					// TODO Auto-generated method stub
					scale = s1.getDoubleValue();
					repaint();
				}
			});
			
			add(s1);
		}
		
		
		LocVektor v = new LocVektor(100,200, 50, 20), vsc = v.copy();
		
		Graphics2D g2;
		
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g2 = (Graphics2D)g;
			double x0sc = vsc.x0, y0sc = vsc.y0;
			vsc = v.copy();
			vsc.x0 = x0sc; vsc.y0 = y0sc;
			//vsc.translate(new LocVektor(100,0));
			vsc.scaleToR(scale*v.mag());
			v.fix(); vsc.fix();
			g2.setStroke(new BasicStroke(3));
			g2.setColor(Color.blue);
			Utility.drawLocVector(v, g2);
			g2.setColor(Color.red);
			Utility.drawLocVector(vsc, g2);
			g2.setFont(new Font("Arial", 1, sizeFontDemo));
			g2.setColor(Color.black);
			g.drawString("v", (int)v.x1+5, (int)v.y1-5);
			g2.setStroke(new BasicStroke(1));
			int vW = g2.getFontMetrics().stringWidth("v");
			int numwidth = g2.getFontMetrics().stringWidth(""+scale);
			g2.drawString(scale+"v", (int)vsc.x1+5, (int)vsc.y1-5);
			Utility.drawLabelVector(new LocVektor((int)vsc.x1+5+numwidth, (int)vsc.y1-5-sizeFontDemo-2, vW, 0), g2);
			g2.setColor(Color.blue);
			Utility.drawLabelVector(new LocVektor((int)v.x1+5, (int)v.y1-5-sizeFontDemo-2, vW, 0), g2);
			
			
			
		}
		
		int held = -1;
		
		int heldStart = -1;
		
		class Clicker implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				double r = 5;
				held = -1; heldStart = -1;
				int x = e.getX(), y = e.getY();
				double dx = v.x1-x, dy = v.y1-y;
				if (dx*dx + dy*dy < r*r) {
					held = 1;
					return;
				}
								
				dx = v.x0-x; dy = v.y0-y;
				if (dx*dx + dy*dy < r*r) {
					heldStart = 1;
					return;
				}
				dx = vsc.x0-x; dy = vsc.y0-y;
				if (dx*dx + dy*dy < r*r) {
					heldStart = 2;
					return;
				}
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
		class Dragger implements MouseMotionListener {

			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				if (held == 1) {
					v.x1 = e.getX();
					v.y1 = e.getY();
					v.fixLength();
					repaint();
					return;
				}
				if (heldStart == 1) {
					v.x0 = e.getX();
					v.y0 = e.getY();
					v.fix();
					repaint();
					return;
				}
				if (heldStart == 2) {
					vsc.x0 = e.getX();
					vsc.y0 = e.getY();
					vsc.fix();
					repaint();
					return;
				}
				
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}}
			
	}
	
	
	
	
	
	
	

}
