package lessons;

import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import mainpackage.StartPanel;

public class LessonPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2849373784575299796L;

	public LessonPanel() {
		super();
		
		backButton.addActionListener( new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Container parent = getParent();
				parent.removeAll();
				parent.add(new StartPanel());
				parent.revalidate();
				parent.repaint();
				
			}
		});

		forwButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				replaceExercise(jp.nextPanel());
				
			}
		});
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {

				SpringLayout layout = new SpringLayout();
				setLayout(layout);
				initializeMenu();
				jp = new Intro();
				scroll = new JScrollPane(jp);
				
				
				
				
				scroll.scrollRectToVisible(new Rectangle(0,0,1,1));
			    
				layout.putConstraint(SpringLayout.WEST, scroll,
                        10,
                        SpringLayout.WEST, LessonPanel.this);
				layout.putConstraint(SpringLayout.NORTH, scroll,
                        10,
                        SpringLayout.SOUTH, bar);
				layout.putConstraint(SpringLayout.EAST, scroll,
                        0,
                        SpringLayout.EAST, LessonPanel.this);
				layout.putConstraint(SpringLayout.SOUTH, scroll,
                        0,
                        SpringLayout.SOUTH, LessonPanel.this);

				scroll.getVerticalScrollBar().setUnitIncrement(16);

				SwingUtilities.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						//scroll.setScrollPosition(0,0);
						add(scroll);
						//scroll.scrollRectToVisible(new Rectangle(0,0,1,1));
						revalidate();
						repaint();
					}
				});
				
			}
		});
	}
	
	ActionListener scrollTop = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			timer.stop();
			JViewport jv = scroll.getViewport();
			jv.setViewPosition(new Point(0,0));
		}
	};
	
	JMenuBar bar = new JMenuBar();
	JMenu menu1, menu2, menu3, menu4;
	
	JMenuItem i1, i2, i3, i4, i5, i6, i7, i8;
	
	SpecificLessonPanel jp;
	
	JButton backButton = new JButton("Na glavnu stranicu");
	JButton forwButton = new JButton("Dalje");
	
	public void initializeMenu() {

		JPanel p = new JPanel();
		menu1 = new JMenu("Zbrajanje");
		menu2 = new JMenu("Oduzimanje");
		menu3 = new JMenu("Skalarni produkt");
		menu4 = new JMenu("Općenito o vektorima");
		
		
		i1 = new JMenuItem("Zbrajanje 1");
		i2 = new JMenuItem("Zbrajanje 2");
		i3 = new JMenuItem("Oduzimanje 1");
		i4 = new JMenuItem("Oduzimanje 2");
		i5 = new JMenuItem("Oduzimanje 3");
		i6 = new JMenuItem("Skalarni 1");
		i7 = new JMenuItem("Skalarni 2");
		i8 = new JMenuItem("Općenito 1");


		i6.addActionListener(l);
		i7.addActionListener(l);
		i8.addActionListener(l);

		menu1.add(i1);
		menu1.add(i2);
		menu2.add(i3);
		menu2.add(i4);
		menu2.add(i5);
		menu3.add(i6);
		menu3.add(i7);
		menu4.add(i8);

		bar.add(menu1);
		bar.add(menu2);
		bar.add(menu3);
		bar.add(menu4);
		
		
		p.add(backButton);
		p.add(bar);
		p.add(forwButton);
		//p.setPreferredSize(bar.getPreferredSize());
		add(p);
		
	
	}
	
	
	JScrollPane scroll = new JScrollPane();
	
	ActionListener l = new ActionListener() {


		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	};
	
	
	
	Timer timer = new Timer(50, scrollTop);
	
	public void replaceExercise(SpecificLessonPanel newp) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				if (jp == newp)
					return;
				jp = newp;
				scroll.setViewportView(jp);
				
				timer.restart();
				
				
			}
		});
	}


	
	
}
