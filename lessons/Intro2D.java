package lessons;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import exercises.VectorTextPane;
import mainpackage.LocVektor;
import mainpackage.VectorsCart;
import vectUtilities.Utility;
import vectUtilities.VecTextPanel;

public class Intro2D extends SpecificLessonPanel {

	
	@Override
	public SpecificLessonPanel nextPanel() {
		return new DotIntro2();
	}
	
	private static final long serialVersionUID = -2081477338867575444L;
	
	public Intro2D() {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				initContent();
				initGUI();
			}
		});
	}
	
	int sizeFont = 20;
	
	public VectorTextPane vtp1 = new VectorTextPane(), vtp2 = new VectorTextPane(),
			vtp3 = new VectorTextPane(), vtp4 = new VectorTextPane(),
			vtp5 = new VectorTextPane(), vtp6 = new VectorTextPane(),
			vtp7 = new VectorTextPane();
	
	
	LocVektor[] cartVectors0, cartVectors, cartVectors1, cartVectors2, cartVectors3, cartVectors4;
	
	VectorsCart vc0 = new VectorsCart(), vc = new VectorsCart(),
			vc1 = new VectorsCart(), vc2 = new VectorsCart(),
			vc3 = new VectorsCart();
	
	VecTextPanel tp = new VecTextPanel(new String[] {"v ", "= 6 \"desno\" + 3\"gore\""}, new int[] {1, 0}, sizeFont),
			tp1 = new VecTextPanel(new String[] {"v ", "= 6", "i ", " + 3", "j "}, new int[] {1, 0, 1, 0, 1}, sizeFont);
	VecTextPanel tp2 = new VecTextPanel(new String[] {"a ", "= 6", "i "}, new int[] {1, 0, 1}, sizeFont);
	VecTextPanel tp3 = new VecTextPanel(new String[] {"b ", "= 3", "j "}, new int[] {1, 0, 1}, sizeFont);
	VecTextPanel tp4 = new VecTextPanel(new String[] {"a = 6       b = 3"}, new int[] {0}, sizeFont);
	
	
	LocVektor startingV = new LocVektor(0,0,6,3);
	LocVektor startV3 = startingV.copy();
	
	public void initGUI() {
		setLayout(new BoxLayout(Intro2D.this, BoxLayout.PAGE_AXIS));
		add(vtp1);
		add(new Madness(10, 1000, 500));
		add(vtp2);
		
		
		cartVectors0 = new LocVektor[] {new LocVektor(0,0,6,3)};
		vc0.vectors = cartVectors0;
		vc0.numVectors = 1;
		add(vc0);
		
		add(vtp3);
		
		//vc.setPreferredSize(new Dimension(500,200));
		vc.draggy = true;
		cartVectors = new LocVektor[] {new LocVektor(0,0,6,3),new LocVektor(0,0,6,0), new LocVektor(0,0,0,3)};
		vc.vectors = cartVectors;
		vc.numVectors = 3;
		vc.mh.putVectors(Arrays.asList(cartVectors), Arrays.asList(new Integer[] {0,0,0}),
				Arrays.asList(new Integer[] {1,0,0}));;
		vc.mh.performaction = true;
		vc.xCoordSpan = 20;
		vc.mh.action = listener;
		vc.customColors = true;
		vc.colors = Arrays.asList(new Color[] {Color.blue, Color.YELLOW, Color.YELLOW});
		vc.drawLabels = true;
		vc.labels = Arrays.asList(new String[] {"v", "a","b"});
		vc.setup();
		add(vc);
		
		add(vtp4);
		
		cartVectors1 = new LocVektor[] {new LocVektor(0,0,6,3),new LocVektor(0,0,6,0), new LocVektor(6,0,0,3)};
		vc1.vectors = cartVectors1;
		vc1.draggy = true;
		
		vc1.numVectors = 3;
		vc1.mh.putVectors(Arrays.asList(cartVectors1), Arrays.asList(new Integer[] {0,0,0}),
				Arrays.asList(new Integer[] {1,0,0}));;
		vc1.mh.performaction = true;
		vc1.xCoordSpan = 20;
		vc1.mh.action = listener1;
		vc1.customColors = true;
		vc1.colors = Arrays.asList(new Color[] {Color.blue, Color.YELLOW, Color.YELLOW});
		vc1.drawLabels = true;
		vc1.labels = Arrays.asList(new String[] {"v", "a","b"});
		vc1.setup();
		add(vc1);
		add(tp);
		
		add(vtp5);
		
		cartVectors2 = new LocVektor[] {new LocVektor(0,0,1,0), new LocVektor(0,0,0,1)};
		vc2.vectors = cartVectors2;
		vc2.draggy = false;
		
		vc2.numVectors = 2;
		
		vc2.xCoordSpan = 20;
		vc2.customColors = true;
		vc2.colors = Arrays.asList(new Color[] {new Color(255,0,0), new Color(255,0,0)});
		vc2.drawLabels = true;
		vc2.labels = Arrays.asList(new String[] {"i ", "j "});
		//vc2.setup();
		add(vc2);
		
		
		add(vtp6);
		
		cartVectors3 = new LocVektor[] {startV3, startV3.xComp(), startV3.yComp(), LocVektor.unitX(), LocVektor.unitY()};
		vc3.numVectors = 5;
		vc3.vectors = cartVectors3;
		vc3.draggy = true;
		vc3.mh.putVectors(Arrays.asList(cartVectors3), Arrays.asList(new Integer[] {0,0,0,0,0}),
				Arrays.asList(new Integer[] {1,0,0,0,0}));;
		vc3.mh.performaction = true;
		vc3.xCoordSpan = 20;
		vc3.mh.action = listener2;
		vc3.customColors = true;
		vc3.colors = Arrays.asList(new Color[] {Color.blue, Color.YELLOW, Color.YELLOW, Color.red, Color.red});
		vc3.drawLabels = true;
		vc3.labels = Arrays.asList(new String[] {"v", "a", "b", "i ", "j "});
		vc3.setup();
		add(vc3);
		add(tp1);
		add(tp2);
		add(tp3);
		add(tp4);
		add(vtp7);
		revalidate();
		repaint();
		
	}
	
	Font f = new Font("Arial", 1, sizeFont);
	
	ImageIcon vIcon = Utility.imageText("v", 100, sizeFont),
			 uIcon = Utility.imageText("u", 100, sizeFont),
			 aIcon = Utility.imageText("a", 100, sizeFont),
			 bIcon = Utility.imageText("b", 100, sizeFont),
			 iIcon = Utility.imageText("i ", 100, sizeFont),
			 jIcon = Utility.imageText("j ", 100, sizeFont);
	
	
	
	public void initContent() {
		vtp1.setFont(f);
		vtp1.write("Uvedimo algbarski pristup u razmatranje vektora.");
		vtp1.write("Vektoru smo definirali pravac (orijentaciju), no kako\nopisati općenit "
				+ "vektor bilo kojeg smjera? Točnije, ako imamo proizvoljan broj nekolinearnih\n"
				+ "vektora, kako baratati njima bez da nacrtamo pravac za svaki?");
		
		vtp2.setFont(f);
		vtp2.write("Nacrtajmo Kartezijev (x-y) koordinatni sustav i stavimo\n"
				+ "početak željenog vektora u ishodište. ");
		vtp3.setFont(f);
		vtp3.write("Nacrtajmo jedan vektor paralelan s x-osi i jedan paralelan s y-osi.\n"
				+ "Namjestimo ih tako da zbrojeni daju naš glavni vektor.\n"
				+ "Pomicanjem vrha vektora ");
		vtp3.putIcon(vIcon);
		vtp3.write(" možete provjeriti funkcionira li zbroj.\n");
		
		vtp4.setFont(f);
		vtp4.write("Ispod je prikazan zbroj po pravilu trokuta.\n"
				+ "Za vektor s vrhom u prvom kvadrantu vidimo da je ");
		vtp4.putIcon(Utility.drawModulus(aIcon));
		vtp4.write(" x koordinata vrha,\ndok je ");
		vtp4.putIcon(Utility.drawModulus(bIcon));
		vtp4.write(" njegova y koordinata. ");

		vtp5.setFont(f);
		
		vtp5.write("Iskoristimo jedinične vektore da bi formalno definirali pojmove\n"
				+ "lijevo i desno.\n"
				+ "Uvedimo vektor ");
		vtp5.putIcon(iIcon);
		vtp5.write(", duljine 1, orijentiran prema pozitivnom dijelu x osi\n"
				+ "te vektor");
		vtp5.putIcon(jIcon);
		vtp5.write(", duljine 1, orijentiran prema pozitivnom dijelu y osi.\n");
		
		vtp6.setFont(f);
		vtp6.write("Sada svaki vektor ");
		vtp6.putIcon(aIcon);
		vtp6.write(" u smjeru x osi možemo napisati kao vektor");
		vtp6.putIcon(iIcon);
		vtp6.write("\n uvećan ili umanjen za neki faktor a (to jest, skalarno ga množimo s\n"
				+ "brojem a). \n");
		vtp6.putIcon(aIcon);
		vtp6.write(" = a");
		vtp6.putIcon(iIcon);
		
		vtp6.write("\nIsto tako za neki vektor ");
		vtp6.putIcon(bIcon);
		vtp6.write(" u smjeru y osi,\n");
		vtp6.putIcon(bIcon);
		vtp6.write(" = b");
		vtp6.putIcon(jIcon);
		
		
		vtp7.setFont(f);
		vtp7.write("Vektor ");
		vtp7.putIcon(aIcon);
		vtp7.write(" = a");
		vtp7.putIcon(iIcon);
		vtp7.write(" zovemo komponenta vektora ");
		vtp7.putIcon(vIcon);
		vtp7.write(" u smjeru osi x.\n"
				+ "Vektor");
		vtp7.putIcon(bIcon);
		vtp7.write(" = b");
		vtp7.putIcon(jIcon);
		vtp7.write(" zovemo komponenta vektora ");
		vtp7.putIcon(vIcon);
		vtp7.write(" u smjeru osi y.\n"
				+ "Brojevi a i b su skalarne komponente vektora ");
		vtp7.putIcon(vIcon);
		vtp7.write(" u smjeru osi x i y redom.\n"
				+ "Oni su brojevi (skalari) kojima množimo jedinične vektore ");
		vtp7.putIcon(iIcon);
		vtp7.write(" i ");
		vtp7.putIcon(jIcon);
		vtp7.write("\n");
		vtp7.putIcon(jIcon);
		vtp7.write(" se izgovara \"jot\".");
	}
	
	
	ActionListener listener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			cartVectors[1].x = cartVectors[0].x;
			cartVectors[1].fix();
			cartVectors[2].y = cartVectors[0].y;
			cartVectors[2].fix();

			vc.repaint();
		}
	};
	
	
	ActionListener listener1 = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			cartVectors1[1].x = cartVectors1[0].x;
			cartVectors1[1].fix();
			cartVectors1[2].y = cartVectors1[0].y;
			cartVectors1[2].x0 = cartVectors1[0].x1;
			cartVectors1[2].fix();
			LocVektor a = cartVectors1[1], b = cartVectors1[2];
			tp.putExpression(new String[] {"v ", " = "+(int)(a.mag()*1000)/1000.0 + (a.x>0 ? " desno + " : " lijevo + ")
				+ (int)(b.mag()*1000)/1000.0 + (b.y>0 ? " gore" : " dolje")},
					new int[] {1,0});
			
			vc1.repaint();
		}
	};
	
	ActionListener listener2 = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			cartVectors3[1].x = cartVectors3[0].x;
			cartVectors3[1].fix();
			cartVectors3[2].y = cartVectors3[0].y;
			//cartVectors1[2].x0 = cartVectors1[0].x1;
			cartVectors1[2].fix();
			LocVektor a = cartVectors3[1], b = cartVectors3[2];
			tp1.putExpression(new String[] {"v ", " = "+(int)(a.x*1000)/1000.0, "i ",
					(b.y<0 ? " - " : " + ")+(int)(Math.abs(b.y)*1000)/1000.0, "j "},
					new int[] {1,0,1,0,1});
			
			tp2.putExpression(new String[] {"a ", "= " + (int)(1000*a.x)/1000.0, "i "}, new int[] {1, 0, 1});
			tp3.putExpression(new String[] {"b ", "= " + (int)(1000*b.y)/1000.0, "j "}, new int[] {1, 0, 1});
			tp4.putText("a = " + (int)(1000*a.x)/1000.0 + "       b = " + (int)(1000*b.y)/1000.0);
			tp4.arrowed = new int[] {0};
			vc3.repaint();
		}
	};
	
	class Demo2D extends JPanel {

		private static final long serialVersionUID = -2889856819584253505L;
		
		Graphics2D g2;
		
		@Override
		public void paintComponent(Graphics g) {
			
		}
		
		
		
		
	}
	
	class Madness extends JPanel {

		private static final long serialVersionUID = 1L;
		
		public Madness(int numv, int wi, int hi) {
			super();
			setBackground(new Color(250, 250,200));
			setPreferredSize(new Dimension(wi, hi));
			w = wi;
			h = hi;
			numvec = numv;
			vectors = new LocVektor[numv];
			for (int i = 0; i< numv; i++) {
				vectors[i] = new LocVektor(Math.random()*w, Math.random()*h,
						(Math.random()-0.5)*w/3, (Math.random()-0.5)*h/3);
			}
		}
		LocVektor[] vectors;
		int numvec = 0;
		int w, h;
		
		Graphics2D g2;
		
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g2 = (Graphics2D)g;
			g2.setColor(Color.LIGHT_GRAY);
			for (int i = 0; i<numvec; i++) {
				
				LocVektor line = vectors[i].copy();
				line.scaleToR(3000);
				Utility.drawLocLine(line, g2);
				line.scale(-1);
				Utility.drawLocLine(line, g2);
			}
			g2.setColor(Color.blue);
			g2.setStroke(new BasicStroke(3));
			for (int i = 0; i<numvec; i++) {
				Utility.drawLocVector(vectors[i], g2);
			}
		}
		
	}

	
	
}
