package lessons;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import mainpackage.LocVektor;
import mainpackage.Vektor;



public class DotIntro extends SpecificLessonPanel {

	public SpecificLessonPanel nextPanel() {
		return new CrossIntro();
	}
	
	
	
	private static final long serialVersionUID = 1L;

	public DotIntro(boolean horiz) {
		super();
		setBackground(new Color(200, 250, 200));
		setPreferredSize(new Dimension(200, 300));
		repaint();
		addMouseMotionListener(new Clicker());
		addMouseListener(new ML());
		w = getWidth();
		h = getHeight();
		fixed = true;
		this.horiz = horiz;
		//System.out.println(w);
		//introduce();
		
	}
	
	public DotIntro() {
		super();
		setBackground(new Color(200, 250, 200));
		setPreferredSize(new Dimension(200, 500));
		repaint();
		addMouseMotionListener(new Clicker());
		addMouseListener(new ML());
		w = getWidth();
		h = getHeight();
		//System.out.println(w);
		//introduce();
		
	}

	boolean fixed = false;
	boolean horiz;
	
	boolean formulas = false;

	
	int w, h;

	int lineUnits = 10;
	int unitWidth;
	double xV1 = 2.5, xV2 = 2.5;
	int x0;
	int vecAmt = 2;
	boolean showInfo = true;
	double[] vecStarts, vecEnds;
	double ys[] = new double[2];
	
	
	LocVektor vproj;
	LocVektor guideLine;
	
	LocVektor v1, v2;
	
	Graphics2D g2;
	
	public void paintLocLine(LocVektor lv) {
		g2.drawLine((int)lv.x0, (int)lv.y0, (int)(lv.x0+lv.x), (int)(lv.y0+lv.y));
	}
	double arrowLength = 10;
	LocVektor locArrow;
	double arrowHalfAngle = Math.PI/6;
	public void paintLocVektor(LocVektor lv) {
		//System.out.println(lv);
		paintLocLine(lv);
		lv.fix();
		if (lv.r < 2)
			return;
		
		double dxa = -lv.x/arrowLength, dya = -lv.y/arrowLength;
		double r = Math.sqrt(dxa*dxa + dya*dya);
		double an = Vektor.angle(dxa, dya);
		if (r<8) {
			r = 8;
			dxa = r * Math.cos(an);
			dya = r * Math.sin(an);
		}
		an += arrowHalfAngle;
		locArrow = new LocVektor(lv.x1, lv.y1, r*Math.cos(an), r*Math.sin(an));
		paintLocLine(locArrow);
		an -= 2*arrowHalfAngle;
		locArrow.x = r*Math.cos(an);
		locArrow.y = r*Math.sin(an);
		paintLocLine(locArrow);
		
		/*
		locArrow = lv;
		locArrow.fix();
		//g2.setColor(Color.BLACK);
		//System.out.println(locArrow);
		locArrow.x0 += lv.x;
		locArrow.y0 += lv.y;
		locArrow.fix();
		//System.out.println(locArrow);
		//lv.rotate(Math.PI/2);
		//paintLocLine(locArrow);
		//g2.setColor(newCol);

		
		locArrow.scaleToR(-arrowLength);
		//System.out.println(locArrow.r);
		//System.out.println(lv);
		locArrow.fix();
		//System.out.println(lv);
		locArrow.rotate(arrowHalfAngle);
		//System.out.println(locArrow.r);
		paintLocLine(locArrow);
		locArrow.rotate(-2 * arrowHalfAngle);
		//g2.setColor(Color.ORANGE);
		paintLocLine(locArrow);
		locArrow = null;
		*/
	}
	
	public void introduce() {
		ys[0] = 0.5;
		if (!fixed) {
			v1 = new LocVektor(w/2, h/2, w*0.2, h*0.4);
			v2 = new LocVektor(w/2, h/2, w*0.3, -h*0.2);
		}
		else {
			v1 = new LocVektor(w/2, h/2, w*0.2, h*0.4);
			v2 = new LocVektor(w/2, h/2, w*0.3, 0);
		}
		vproj = v1.projOn(v2);
		vproj.x0=v1.x0; vproj.y0=v1.y0;
		
		guideLine = new LocVektor(v1.x1, v1.y1, vproj.x1, vproj.y1, true);
	}
	
	boolean drawAxes = true;
	boolean envelopInfo = false;
	
	boolean firstRender = true;
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		w = getWidth();
		h = getHeight();
		g2 = (Graphics2D)g;
		//System.out.println(h);
		if (firstRender) {
			firstRender =false;
			repaint();
			introduce();
			/*
			System.out.println(h);
			System.out.println(ys[0]);
			System.out.println(ys[1]);
*/
			
			
		}
		double minLength = Math.sqrt(w*w+h*h);
		v1.fix(); v2.fix();
		LocVektor extendedv2 = v2.copy();
		extendedv2.scaleToR(minLength);
		paintLocLine(extendedv2);
		extendedv2.scale(-1);
		paintLocLine(extendedv2);
		Font f = g.getFont();
		g2.setColor(Color.blue);
		g2.setStroke(new BasicStroke(3));
		
		paintLocVektor(v1);
		g2.setColor(Color.green);
		paintLocVektor(v2);
		
		g2.setStroke(new BasicStroke(4));
		g2.setColor(Color.red);
		paintLocVektor(vproj);
		
		g2.setStroke(new BasicStroke(1));
		g2.setColor(Color.black);
		paintLocLine(guideLine);
		
		if (formulas) {
			
			g.drawString("v", (int)v1.x1, (int)v1.y1);
			g.drawString("u", (int)v2.x1, (int)v2.y1);

			int x1 = (int)(v1.x)/10, x2 = (int)(v2.x)/10, y1 = (int)(v1.y)/10, y2 = (int)(v2.y)/10;
			int prod = x1*x2 + y1*y2;
			String prodCalc = "v�u = (" + x1 + ", " + y1 + ") � (" + x2 + ", " + y2 + ") = " +
					x1 + "�" + x2 + " + " + y1 + "�" + y2 + " = " + prod;
			g.drawString(prodCalc, 200, 450);
			
			int r = 30;
			double a1 = LocVektor.angle(v1.x, -v1.y);
			double a2 = LocVektor.angle(v2.x, -v2.y);
			
			double delta = LocVektor.closestAngleFromTo(a1, a2);
			g.drawArc((int)(v1.x0-r), (int)(v1.y0-r), 2*r, 2*r, (int)(180/Math.PI*a1), (int)(180/Math.PI*delta));

			
			/*
			int first = LocVektor.whichFirstCCW(a1, a2);
			System.out.println(a1);
			double firstAngle;
			double da;
			if (first==1) {
				da = a2-a1;
				firstAngle = a1;
			}
			else {
				da = a1-a2;
				firstAngle = a2;
			}
			//System.out.println(a1);
			//System.out.println(a1 + " " + da);
			g.drawArc((int)(v1.x0-r), (int)(v1.y0-r), 2*r, 2*r, (int)(180/Math.PI*firstAngle), (int)(180/Math.PI*da));
			*/
		}
		
	}
	
	int held = -1;

	class ML implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			int x = e.getX(), y = e.getY();
			//System.out.println(x);
			double xv = v1.x1, yv = v1.y1;
			if (Math.abs(x-xv) + Math.abs(y-yv) < 10) {
				held = 1;
			}
			else {
				xv = v2.x1; yv = v2.y1;
				if (Math.abs(x-xv) + Math.abs(y-yv) < 5) {
					held = 2;
				}
				else {
					held = -1;
				}
			}
			//System.out.println(held);
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			held = -1;	
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	class Clicker implements MouseMotionListener {

		
		@Override
		public void mouseDragged(MouseEvent e) {
			//System.out.println(e.getX());
			//System.out.println("dragging: " + held);
			if (held == 1) {
				v1.x1 = e.getX();
				v1.y1 = e.getY();
				v1.fixLength();
				//System.out.println(v1);
			}
			else if (held == 2) {
				//System.out.println(v2);
				v2.x1 = e.getX();
				if (!fixed)
					v2.y1 = e.getY();
				v2.fixLength();
				//System.out.println(v2);
			}
			vproj = v1.projOn(v2);
			vproj.fixLength();
			vproj.x0=v1.x0; vproj.y0=v1.y0;
			guideLine = new LocVektor(v1.x1, v1.y1, vproj.x1, vproj.y1, true);

			repaint();
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}}
	
	
	
	class DotProductPanel extends JPanel {




		private static final long serialVersionUID = 1L;

		public DotProductPanel(boolean horiz) {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 300));
			repaint();
			addMouseMotionListener(new Clicker());
			addMouseListener(new ML());
			w = getWidth();
			h = getHeight();
			fixed = true;
			this.horiz = horiz;
			//System.out.println(w);
			//introduce();
			
		}
		
		public DotProductPanel() {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 500));
			repaint();
			addMouseMotionListener(new Clicker());
			addMouseListener(new ML());
			w = getWidth();
			h = getHeight();
			//System.out.println(w);
			//introduce();
			
		}

		boolean fixed = false;
		boolean horiz;
		
		boolean formulas = false;

		
		int w, h;

		int lineUnits = 10;
		int unitWidth;
		double xV1 = 2.5, xV2 = 2.5;
		int x0;
		int vecAmt = 2;
		boolean showInfo = true;
		double[] vecStarts, vecEnds;
		double ys[] = new double[2];
		
		
		LocVektor vproj;
		LocVektor guideLine;
		
		LocVektor v1, v2;
		
		Graphics2D g2;
		
		public void paintLocLine(LocVektor lv) {
			g2.drawLine((int)lv.x0, (int)lv.y0, (int)(lv.x0+lv.x), (int)(lv.y0+lv.y));
		}
		double arrowLength = 10;
		LocVektor locArrow;
		double arrowHalfAngle = Math.PI/6;
		public void paintLocVektor(LocVektor lv) {
			//System.out.println(lv);
			paintLocLine(lv);
			lv.fix();
			if (lv.r < 2)
				return;
			
			double dxa = -lv.x/arrowLength, dya = -lv.y/arrowLength;
			double r = Math.sqrt(dxa*dxa + dya*dya);
			double an = Vektor.angle(dxa, dya);
			if (r<8) {
				r = 8;
				dxa = r * Math.cos(an);
				dya = r * Math.sin(an);
			}
			an += arrowHalfAngle;
			locArrow = new LocVektor(lv.x1, lv.y1, r*Math.cos(an), r*Math.sin(an));
			paintLocLine(locArrow);
			an -= 2*arrowHalfAngle;
			locArrow.x = r*Math.cos(an);
			locArrow.y = r*Math.sin(an);
			paintLocLine(locArrow);
			
			/*
			locArrow = lv;
			locArrow.fix();
			//g2.setColor(Color.BLACK);
			//System.out.println(locArrow);
			locArrow.x0 += lv.x;
			locArrow.y0 += lv.y;
			locArrow.fix();
			//System.out.println(locArrow);
			//lv.rotate(Math.PI/2);
			//paintLocLine(locArrow);
			//g2.setColor(newCol);

			
			locArrow.scaleToR(-arrowLength);
			//System.out.println(locArrow.r);
			//System.out.println(lv);
			locArrow.fix();
			//System.out.println(lv);
			locArrow.rotate(arrowHalfAngle);
			//System.out.println(locArrow.r);
			paintLocLine(locArrow);
			locArrow.rotate(-2 * arrowHalfAngle);
			//g2.setColor(Color.ORANGE);
			paintLocLine(locArrow);
			locArrow = null;
			*/
		}
		
		public void introduce() {
			ys[0] = 0.5;
			if (!fixed) {
				v1 = new LocVektor(w/2, h/2, w*0.2, h*0.4);
				v2 = new LocVektor(w/2, h/2, w*0.3, -h*0.2);
			}
			else {
				v1 = new LocVektor(w/2, h/2, w*0.2, h*0.4);
				v2 = new LocVektor(w/2, h/2, w*0.3, 0);
			}
			vproj = v1.projOn(v2);
			vproj.x0=v1.x0; vproj.y0=v1.y0;
			
			guideLine = new LocVektor(v1.x1, v1.y1, vproj.x1, vproj.y1, true);
		}
		
		boolean drawAxes = true;
		boolean envelopInfo = false;
		
		boolean firstRender = true;
		
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			w = getWidth();
			h = getHeight();
			g2 = (Graphics2D)g;
			//System.out.println(h);
			if (firstRender) {
				firstRender =false;
				repaint();
				introduce();
				/*
				System.out.println(h);
				System.out.println(ys[0]);
				System.out.println(ys[1]);
*/
				
				
			}
			double minLength = Math.sqrt(w*w+h*h);
			v1.fix(); v2.fix();
			LocVektor extendedv2 = v2.copy();
			extendedv2.scaleToR(minLength);
			paintLocLine(extendedv2);
			extendedv2.scale(-1);
			paintLocLine(extendedv2);
			Font f = g.getFont();
			g2.setColor(Color.blue);
			g2.setStroke(new BasicStroke(3));
			
			paintLocVektor(v1);
			g2.setColor(Color.green);
			paintLocVektor(v2);
			
			g2.setStroke(new BasicStroke(4));
			g2.setColor(Color.red);
			paintLocVektor(vproj);
			
			g2.setStroke(new BasicStroke(1));
			g2.setColor(Color.black);
			paintLocLine(guideLine);
			
			if (formulas) {
				
				g.drawString("v", (int)v1.x1, (int)v1.y1);
				g.drawString("u", (int)v2.x1, (int)v2.y1);

				int x1 = (int)(v1.x)/10, x2 = (int)(v2.x)/10, y1 = (int)(v1.y)/10, y2 = (int)(v2.y)/10;
				int prod = x1*x2 + y1*y2;
				String prodCalc = "v�u = (" + x1 + ", " + y1 + ") � (" + x2 + ", " + y2 + ") = " +
						x1 + "�" + x2 + " + " + y1 + "�" + y2 + " = " + prod;
				g.drawString(prodCalc, 200, 450);
				
				int r = 30;
				double a1 = LocVektor.angle(v1.x, -v1.y);
				double a2 = LocVektor.angle(v2.x, -v2.y);
				
				double delta = LocVektor.closestAngleFromTo(a1, a2);
				g.drawArc((int)(v1.x0-r), (int)(v1.y0-r), 2*r, 2*r, (int)(180/Math.PI*a1), (int)(180/Math.PI*delta));

				
				/*
				int first = LocVektor.whichFirstCCW(a1, a2);
				System.out.println(a1);
				double firstAngle;
				double da;
				if (first==1) {
					da = a2-a1;
					firstAngle = a1;
				}
				else {
					da = a1-a2;
					firstAngle = a2;
				}
				//System.out.println(a1);
				//System.out.println(a1 + " " + da);
				g.drawArc((int)(v1.x0-r), (int)(v1.y0-r), 2*r, 2*r, (int)(180/Math.PI*firstAngle), (int)(180/Math.PI*da));
				*/
			}
			
		}
		
		int held = -1;

		class ML implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				int x = e.getX(), y = e.getY();
				//System.out.println(x);
				double xv = v1.x1, yv = v1.y1;
				if (Math.abs(x-xv) + Math.abs(y-yv) < 10) {
					held = 1;
				}
				else {
					xv = v2.x1; yv = v2.y1;
					if (Math.abs(x-xv) + Math.abs(y-yv) < 5) {
						held = 2;
					}
					else {
						held = -1;
					}
				}
				//System.out.println(held);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				held = -1;	
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
		class Clicker implements MouseMotionListener {

			
			@Override
			public void mouseDragged(MouseEvent e) {
				//System.out.println(e.getX());
				//System.out.println("dragging: " + held);
				if (held == 1) {
					v1.x1 = e.getX();
					v1.y1 = e.getY();
					v1.fixLength();
					//System.out.println(v1);
				}
				else if (held == 2) {
					//System.out.println(v2);
					v2.x1 = e.getX();
					if (!fixed)
						v2.y1 = e.getY();
					v2.fixLength();
					//System.out.println(v2);
				}
				vproj = v1.projOn(v2);
				vproj.fixLength();
				vproj.x0=v1.x0; vproj.y0=v1.y0;
				guideLine = new LocVektor(v1.x1, v1.y1, vproj.x1, vproj.y1, true);

				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}}
		
		
		
	}
	


}
