package lessons;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import exercises.VectorTextPane;
import mainpackage.LocVektor;
import mainpackage.Question;
import mainpackage.VectorsCart;
import mainpackage.Vektor;
import vectUtilities.Utility;
import vectUtilities.VecTextPanel;

public class DotIntro2 extends SpecificLessonPanel {

	
	private static final long serialVersionUID = 3424276961628407066L;

	@Override
	public SpecificLessonPanel nextPanel() {
		return new CrossIntro();
	}
	
	int sizeFont = 20;
	Font font = new Font("Arial", 0, sizeFont);
	
	VectorTextPane vtp1 = new VectorTextPane(),
			vtp2 = new VectorTextPane(),
			vtp3 = new VectorTextPane(),
			vtp4 = new VectorTextPane(),
			vtp5 = new VectorTextPane(),
			vtp6 = new VectorTextPane();
	
	public LocVektor v = new LocVektor(0,0,5,4), u = new LocVektor(0,0,-3,2);
	VecTextPanel tp1 = new VecTextPanel(new String[] {"v", " = 5", "i ", " + 4", "j"}, new int[] {1,0,1,0,1}, sizeFont);
	VecTextPanel tp2 = new VecTextPanel(new String[] {"u", " = -3", "i ", " + 2", "j"}, new int[] {1,0,1,0,1}, sizeFont);
	VecTextPanel tp3 = new VecTextPanel(new String[] {"v", "·", "u", " = 5·(-3) + 4·2 = -7"}, new int[] {1,0,1,0}, sizeFont);
	
	VectorsCart vc = new VectorsCart();
	
	ImageIcon vIcon = Utility.imageText("v", 100, sizeFont),
			 uIcon = Utility.imageText("u", 100, sizeFont),
			 aIcon = Utility.imageText("a", 100, sizeFont),
			 bIcon = Utility.imageText("b", 100, sizeFont),
			 iIcon = Utility.imageText("i ", 100, sizeFont),
			 jIcon = Utility.imageText("j ", 100, sizeFont),
			 pIcon = Utility.imageText("p", 100, sizeFont);
	
	public DotIntro2() {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				setLayout(new BoxLayout(DotIntro2.this, BoxLayout.PAGE_AXIS));
				initContent();
			}
		});
	}
	
	
	
	
	
	public void initContent() {

		removeAll();
		
		
		vtp1.setFont(font);
		vtp1.write("Vektor možemo projicirati na drugi vektor. pogledajmo što to znači.\n");
		vtp1.write("Kada vektor ");
		vtp1.putIcon(vIcon);
		vtp1.write(" projiciramo na vektor ");
		vtp1.putIcon(uIcon);
		vtp1.write(", dobivamo novi vektor (nazovimo ga ");
		vtp1.putIcon(pIcon);
		vtp1.write("),\nkoji ima smjer vektora ");
		vtp1.putIcon(uIcon);
		vtp1.write(" i duljinu manju ili jednaku duljini vektora ");
		vtp1.putIcon(vIcon);
		vtp1.write(".\nTu projekciju možemo zamisliti kao sjenu vektora ");
		vtp1.putIcon(vIcon);
		vtp1.write(" na neki zid definiran vektorom ");
		vtp1.putIcon(uIcon);
		vtp1.write(",\nkad bi svjetlo sjalo okomito na ");
		vtp1.putIcon(uIcon);
		
		add(vtp1);


		
		add(new DotProductPanel());
		
		vtp2.setFont(font);
		vtp2.write("Ako postavimo ");
		vtp2.putIcon(uIcon);
		vtp2.write(" u horizontalnom smjeru, ");
		vtp2.putIcon(pIcon);
		vtp2.write(" predstavlja horizontalnu komponentu vektora ");
		vtp2.putIcon(vIcon);
		vtp2.write(".\n");
		add(vtp2);
		add(new DotProductPanel(true));
		

		
		
		vtp3.setFont(font);
		vtp3.putIcon(pIcon);
		vtp3.write(" i ");
		vtp3.putIcon(vIcon);
		vtp3.write(" su jednake duljine kad je θ=0 ili θ=180°. \nOpćenito, |");
		vtp3.putIcon(pIcon);
		vtp3.write("| = |");
		vtp3.putIcon(vIcon);
		vtp3.write("||cosθ|.");
		
		add(vtp3);
		
		String q = "Za vektore neke duljine, kada je njihov skalarni umnožak najveći?";
		
		add(new Question(q, new String[] {
			"Kad je kut između njih 0",
			"Kad je kut između njih 180°",
			"Kad je kut između njih 90°"
		}, 0));
		
		q = "A kada je najmanji?";
		add(new Question(q, new String[] {
				"Kad je kut između njih 0",
				"Kad je kut između njih 180°",
				"Kad je kut između njih 90°"
			}, 1));
		
		q = "Kada projekcija u na v ima najmanju duljinu=";
		add(new Question(q, new String[] {
				"Kad je kut između u i v 0",
				"Kad je kut između u i v 180°",
				"Kad je kut između u i v 90°"
			}, 2));
		
		

		
		vtp4.setFont(font);
		vtp4.write("Projekcija nestaje (pretvara se u nul-vektor) za θ=90°.\n"
				+ "Umnožak |");
		vtp4.putIcon(vIcon);
		vtp4.write("||");
		vtp4.putIcon(vIcon);
		vtp4.write("|cosθ nazivamo skalarni umnožak vektora ");
		vtp4.putIcon(vIcon);
		vtp4.write(" i ");
		vtp4.putIcon(uIcon);
		vtp4.write(".\nOznačavamo ga obično ");
		vtp4.putIcon(vIcon);
		vtp4.write(" · ");
		vtp4.putIcon(uIcon);
		vtp4.write(" ili ");
		vtp4.putIcon(uIcon);
		vtp4.write(" · ");
		vtp4.putIcon(vIcon);
		vtp4.write(".\nKao i kod zbrajanja, skalarni umnožak vektora je komutativan.\n");
		vtp4.write("Trebamo paziti da ne pomiješamo pojmove množenje vektora skalarom\n"
				+ "i skalarni umnožak vektora.");

		add(vtp4);
		
		
		vtp5.setFont(font);
		vtp5.write("Skalarni umnožak se lagano računa pomoću komponenti vektora. Za bilo koje vektore\n");
		vtp5.putIcon(vIcon);
		vtp5.write(" = a");
		vtp5.putIcon(iIcon);
		vtp5.write(" + b");
		vtp5.putIcon(jIcon);
		vtp5.write("\n");
		vtp5.putIcon(uIcon);
		vtp5.write(" = c");
		vtp5.putIcon(iIcon);
		vtp5.write(" + d");
		vtp5.putIcon(jIcon);
		vtp5.write("\n");
		vtp5.write("Vrijedi formula\n");
		vtp5.putIcon(vIcon);
		vtp5.write(" · ");
		vtp5.putIcon(uIcon);
		vtp5.write(" = a·b + c·d\n");
		vtp5.write("Pomoću ove formule jednostavno određujemo kut između dvaju vektora.");
		add(vtp5);
		vc.vectors = new LocVektor[] {v, u};
		vc.mh.putVectors(Arrays.asList(vc.vectors), Arrays.asList(new Integer[] {0,0}), Arrays.asList(new Integer[] {1,1}));
		vc.mh.action = lis;
		vc.mh.performaction = true;
		vc.numVectors = 2;
		vc.drawLabels = true;
		vc.labels = Arrays.asList(new String[] {"v", "u"});
		vc.draggy = true;
		vc.setup();
		add(vc);
		
		add(tp1);
		add(tp2);
		add(tp3);
		
		DotProductPanel dpp = new DotProductPanel();
		
		dpp.formulas = true;
		
		//add(dpp);
		
		
		
		
		
		add(new JLabel("Vektori"));
		revalidate();
		repaint();
	
	}
	
	ActionListener lis = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			double xv1 = vc.vectors[0].x, yv1 = vc.vectors[0].y;
			double xv2 = vc.vectors[1].x, yv2 = vc.vectors[1].y;
			xv1 = (int)(10*xv1)/10.0;
			yv1 = (int)(10*yv1)/10.0;
			xv2 = (int)(10*xv2)/10.0;
			yv2 = (int)(10*yv2)/10.0;
			double dot = xv1*xv2 + yv1*yv2;
			dot = (int)(10*dot)/10.0;
			tp1.putExpression(new String[] {"v", " = "+xv1, "i ", " + "+yv1, "j"}, new int[] {1,0,1,0,1});
			tp2.putExpression(new String[] {"u", " = "+xv2, "i ", " + "+yv2, "j"}, new int[] {1,0,1,0,1});
			tp3.putExpression(new String[] {"v", "·", "u", " = 5·(-3) + 4·2 = -7"}, new int[] {1,0,1,0});
			tp3.putExpression(new String[] {"v", "·", "u", " = "+xv1+"·"+xv2 + " + " + xv1+"·"+xv2 +" = "+dot},
					new int[] {1,0,1,0});
			vc.repaint();
			
		}
	};
	
	class DotProductPanel extends JPanel {





		private static final long serialVersionUID = 1L;

		public boolean drawLabels = true;
		
		String[] labels = new String[] {"v","u","p"};
		
		public DotProductPanel(boolean horiz) {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 300));
			repaint();
			addMouseMotionListener(new Clicker());
			addMouseListener(new ML());
			w = getWidth();
			h = getHeight();
			fixed = true;
			this.horiz = horiz;
			//System.out.println(w);
			//introduce();
			
		}
		
		public DotProductPanel() {
			super();
			setBackground(new Color(200, 250, 200));
			setPreferredSize(new Dimension(200, 500));
			repaint();
			addMouseMotionListener(new Clicker());
			addMouseListener(new ML());
			w = getWidth();
			h = getHeight();
			//System.out.println(w);
			//introduce();
			
		}

		boolean fixed = false;
		boolean horiz;
		
		boolean formulas = false;

		
		int w, h;

		int lineUnits = 10;
		int unitWidth;
		double xV1 = 2.5, xV2 = 2.5;
		int x0;
		int vecAmt = 2;
		boolean showInfo = true;
		double[] vecStarts, vecEnds;
		double ys[] = new double[2];
		
		
		LocVektor vproj;
		LocVektor guideLine;
		
		LocVektor v1, v2;
		
		Graphics2D g2;
		
		public void paintLocLine(LocVektor lv) {
			g2.drawLine((int)lv.x0, (int)lv.y0, (int)(lv.x0+lv.x), (int)(lv.y0+lv.y));
		}
		double arrowLength = 10;
		LocVektor locArrow;
		double arrowHalfAngle = Math.PI/6;
		public void paintLocVektor(LocVektor lv) {
			//System.out.println(lv);
			paintLocLine(lv);
			lv.fix();
			if (lv.r < 2)
				return;
			
			double dxa = -lv.x/arrowLength, dya = -lv.y/arrowLength;
			double r = Math.sqrt(dxa*dxa + dya*dya);
			double an = Vektor.angle(dxa, dya);
			if (r<8) {
				r = 8;
				dxa = r * Math.cos(an);
				dya = r * Math.sin(an);
			}
			an += arrowHalfAngle;
			locArrow = new LocVektor(lv.x1, lv.y1, r*Math.cos(an), r*Math.sin(an));
			paintLocLine(locArrow);
			an -= 2*arrowHalfAngle;
			locArrow.x = r*Math.cos(an);
			locArrow.y = r*Math.sin(an);
			paintLocLine(locArrow);
			
			/*
			locArrow = lv;
			locArrow.fix();
			//g2.setColor(Color.BLACK);
			//System.out.println(locArrow);
			locArrow.x0 += lv.x;
			locArrow.y0 += lv.y;
			locArrow.fix();
			//System.out.println(locArrow);
			//lv.rotate(Math.PI/2);
			//paintLocLine(locArrow);
			//g2.setColor(newCol);

			
			locArrow.scaleToR(-arrowLength);
			//System.out.println(locArrow.r);
			//System.out.println(lv);
			locArrow.fix();
			//System.out.println(lv);
			locArrow.rotate(arrowHalfAngle);
			//System.out.println(locArrow.r);
			paintLocLine(locArrow);
			locArrow.rotate(-2 * arrowHalfAngle);
			//g2.setColor(Color.ORANGE);
			paintLocLine(locArrow);
			locArrow = null;
			*/
		}
		
		public void introduce() {
			ys[0] = 0.5;
			if (!fixed) {
				v1 = new LocVektor(w/2, h/2, w*0.2, h*0.4);
				v2 = new LocVektor(w/2, h/2, w*0.3, -h*0.2);
			}
			else {
				v1 = new LocVektor(w/2, h/2, w*0.2, h*0.4);
				v2 = new LocVektor(w/2, h/2, w*0.3, 0);
			}
			vproj = v1.projOn(v2);
			vproj.x0=v1.x0; vproj.y0=v1.y0;
			
			guideLine = new LocVektor(v1.x1, v1.y1, vproj.x1, vproj.y1, true);
		}
		
		boolean drawAxes = true;
		boolean envelopInfo = false;
		
		boolean firstRender = true;
		
		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			w = getWidth();
			h = getHeight();
			g2 = (Graphics2D)g;
			g2.setFont(font);
			//System.out.println(h);
			if (firstRender) {
				firstRender =false;
				repaint();
				introduce();
				/*
				System.out.println(h);
				System.out.println(ys[0]);
				System.out.println(ys[1]);
*/
				
				
			}
			double minLength = Math.sqrt(w*w+h*h);
			v1.fix(); v2.fix();
			LocVektor extendedv2 = v2.copy();
			extendedv2.scaleToR(minLength);
			paintLocLine(extendedv2);
			extendedv2.scale(-1);
			paintLocLine(extendedv2);
			//Font f = g.getFont();
			g2.setColor(Color.blue);
			g2.setStroke(new BasicStroke(3));
			
			paintLocVektor(v1);
			Utility.drawFancyString(new String[] {"v"}, new int[] {1}, v1.x1, v1.y1, g2);
			
			g2.setColor(Color.green);
			paintLocVektor(v2);
			Utility.drawFancyString(new String[] {"u"}, new int[] {1}, v2.x1, v2.y1, g2);
			
			g2.setStroke(new BasicStroke(4));
			g2.setColor(Color.red);
			paintLocVektor(vproj);
			Utility.drawFancyString(new String[] {"p"}, new int[] {1}, vproj.x1, vproj.y1, g2);

			
			g2.setStroke(new BasicStroke(1));
			g2.setColor(Color.black);
			paintLocLine(guideLine);
			
			if (formulas) {
				
				g.drawString("v", (int)v1.x1, (int)v1.y1);
				g.drawString("u", (int)v2.x1, (int)v2.y1);

				int x1 = (int)(v1.x)/10, x2 = (int)(v2.x)/10, y1 = (int)(v1.y)/10, y2 = (int)(v2.y)/10;
				int prod = x1*x2 + y1*y2;
				String prodCalc = "v·u = (" + x1 + ", " + y1 + ") · (" + x2 + ", " + y2 + ") = " +
						x1 + "·" + x2 + " + " + y1 + "·" + y2 + " = " + prod;
				g.drawString(prodCalc, 200, 450);
				
				int r = 30;
				double a1 = LocVektor.angle(v1.x, -v1.y);
				double a2 = LocVektor.angle(v2.x, -v2.y);
				
				double delta = LocVektor.closestAngleFromTo(a1, a2);
				g.drawArc((int)(v1.x0-r), (int)(v1.y0-r), 2*r, 2*r, (int)(180/Math.PI*a1), (int)(180/Math.PI*delta));

				
				/*
				int first = LocVektor.whichFirstCCW(a1, a2);
				System.out.println(a1);
				double firstAngle;
				double da;
				if (first==1) {
					da = a2-a1;
					firstAngle = a1;
				}
				else {
					da = a1-a2;
					firstAngle = a2;
				}
				//System.out.println(a1);
				//System.out.println(a1 + " " + da);
				g.drawArc((int)(v1.x0-r), (int)(v1.y0-r), 2*r, 2*r, (int)(180/Math.PI*firstAngle), (int)(180/Math.PI*da));
				*/
			}
			
		}
		
		int held = -1;

		class ML implements MouseListener {

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				int x = e.getX(), y = e.getY();
				//System.out.println(x);
				double xv = v1.x1, yv = v1.y1;
				if (Math.abs(x-xv) + Math.abs(y-yv) < 10) {
					held = 1;
				}
				else {
					xv = v2.x1; yv = v2.y1;
					if (Math.abs(x-xv) + Math.abs(y-yv) < 5) {
						held = 2;
					}
					else {
						held = -1;
					}
				}
				//System.out.println(held);
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				held = -1;	
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		}
		
		class Clicker implements MouseMotionListener {

			
			@Override
			public void mouseDragged(MouseEvent e) {
				//System.out.println(e.getX());
				//System.out.println("dragging: " + held);
				if (held == 1) {
					v1.x1 = e.getX();
					v1.y1 = e.getY();
					v1.fixLength();
					//System.out.println(v1);
				}
				else if (held == 2) {
					//System.out.println(v2);
					v2.x1 = e.getX();
					if (!fixed)
						v2.y1 = e.getY();
					v2.fixLength();
					//System.out.println(v2);
				}
				vproj = v1.projOn(v2);
				vproj.fixLength();
				vproj.x0=v1.x0; vproj.y0=v1.y0;
				guideLine = new LocVektor(v1.x1, v1.y1, vproj.x1, vproj.y1, true);

				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}}
		
		
		
	
	}
	
	
}
